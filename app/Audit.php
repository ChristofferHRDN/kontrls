<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

/**
 * @property mixed interesting_filecount
 * @property bool running
 * @property bool finished
 * @property static stopped_audit
 * @property Domain domain
 * @property mixed initiated_by
 * @property bool stopped
 */
class Audit extends Model
{
	protected $fillable = ['domain_id', 'user_id', 'running', 'initiated_by'];
	protected $with = ['domain'];
	protected $hidden = ['domain_id', 'user_id', 'id'];

    public function domain()
	{
		return $this->belongsTo('App\Domain');
	}

	public function user()
	{
		return $this->belongsTo('App\User');
	}

	public function files()
	{
		return $this->hasMany('App\AuditedFile');
	}

	public function structure()
	{
		return $this->hasMany('App\AuditStructure');
	}

	public function interestingStructure()
	{
		return $this->hasMany('App\AuditInterestingStructure');
	}

	public function statuses()
	{
		return $this->hasMany('App\AuditStatus');
	}

	public function getRouteKeyName()
	{
		return 'hashid';
	}

	public function isDone()
	{
		return ($this->stopped || $this->finished || $this->stopped_audit !== "0000-00-00 00:00:00");
	}

	public function getStatus()
	{
		if ($this->stopped) {
			$status = 'Stopped';
		} elseif ($this->finished) {
			$status = 'Done';
		} else {
			$status = $this->statuses()->orderBy('id', 'desc')->first();
			if($status !== null){
				$status = $status->status;
			} else {
				$status = "Waiting";
			}

		}
		return $status;
	}
}
