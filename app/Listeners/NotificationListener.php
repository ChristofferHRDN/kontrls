<?php

namespace App\Listeners;

use Laravel\Spark\Events\NotificationCreated;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Contracts\Queue\ShouldQueue;

class NotificationListener
{
    /**
     * Create the event listener.
     *
     */
    public function __construct()
    {
        //
    }

    /**
     * Handle the event.
     *
     * @param LaravelSparkEventsNotificationCreated|NotificationCreated $event
     */
    public function handle(NotificationCreated $event)
    {
        //
    }
}
