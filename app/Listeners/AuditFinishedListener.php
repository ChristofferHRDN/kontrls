<?php

namespace App\Listeners;

use App\Events\AuditFinished;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Contracts\Queue\ShouldQueue;
use Laravel\Spark\Contracts\Repositories\NotificationRepository;

class AuditFinishedListener
{
    protected $notificationRepository;

	/**
	 * Create the event listener.
	 *
	 * @param NotificationRepository $notificationRepository
	 */
    public function __construct(NotificationRepository $notificationRepository)
    {
        $this->notificationRepository = $notificationRepository;
    }

    /**
     * Handle the event.
     *
     * @param  AuditFinished  $event
     * @return void
     */
    public function handle(AuditFinished $event)
    {
        $this->notificationRepository->create($event->audit->domain->user, [
			'icon' => 'fa-users',
			'body' => 'An audit has finished!',
			'action_text' => 'View completed audit',
			'action_url' => route('domain.audit.show', [$event->audit->domain, $event->audit], false)
		]);
    }
}
