<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class AuditStatus extends Model
{
	protected $fillable = ['status'];
    public function audit()
	{
		return $this->belongsTo('App\Audit');
	}
}
