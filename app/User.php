<?php

namespace App;

use App\Traits\HasSettings;
use Laravel\Spark\User as SparkUser;

class User extends SparkUser
{
    use HasSettings;
    
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'name',
        'email',
    ];

    /**
     * The attributes excluded from the model's JSON form.
     *
     * @var array
     */
    protected $hidden = [
        'password',
        'remember_token',
        'authy_id',
        'country_code',
        'phone',
        'card_brand',
        'card_last_four',
        'card_country',
        'billing_address',
        'billing_address_line_2',
        'billing_city',
        'billing_zip',
        'billing_country',
        'extra_billing_information',
    ];

    /**
     * The attributes that should be cast to native types.
     *
     * @var array
     */
    protected $casts = [
        'trial_ends_at' => 'date',
        'uses_two_factor_auth' => 'boolean',
    ];

    public function domains()
    {
        return $this->hasMany('App\Domain');
    }

    public function groups()
    {
        return $this->hasMany('App\Group');
    }

    public function alert_methods()
    {
        return $this->hasMany('App\AlertMethod');
    }

    public function logs()
    {
        return $this->morphMany('App\ModelLog', 'loggable');
    }

    public function alerts()
    {
        return $this->hasManyThrough('App\Alert', 'App\Domain');
    }

	/**
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     */
    public function allowed_hashes()
    {
        return $this->hasMany('App\UserAllowedHashes');
    }

	/**
	 * @return array
	 */
	public function allowedHashes()
    {
        return collect($this->allowed_hashes)->map(function ($hash) {
            return $hash->md5;
        })->toArray();
    }
}
