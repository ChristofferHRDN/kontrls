<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Settings extends Model
{
    protected $fillable = ['values'];
    protected $hidden = ['id', 'settingsable_id', 'settingable_type', 'created_at', 'updated_at'];
    
    public function domain()
    {
        return $this->morphTo();
    }

    public function group()
    {
        return $this->morphTo();
    }

    public function getValuesAttribute()
    {
        return json_decode($this->attributes["values"], true);
    }

    public function getValue($key, $default = null)
    {
        return array_get($this->values, $key, $default);
    }

    public function setValue($key, $value)
    {
        $values = $this->getValuesAttribute();
        $values[$key] = $value;
        $this->attributes["values"] = json_encode($values);
        return $this->save();
    }

	public function setValues($values)
	{
		$this->attributes["values"] = json_encode($values);
		return $this->save();
	}
    public static function getDomainDefaults($jsonify = true, $key = null)
    {
        $values = ['polltime' => 5, 'time_between_alerts' => 10];

        if($key){
            return $values[$key];
        }
        
        if($jsonify){
            $values = json_encode($values);
        }
        return $values;
    }

    public static function getGroupDefaults($jsonify = true, $key = null)
    {
        $values = ['polltime' => 5, 'time_between_alerts' => 10];
        
        if($key){
            return $values[$key];
        }

        if($jsonify){
            $values = json_encode($values);
        }
        return $values;
    }
}
