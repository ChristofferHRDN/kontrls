<?php

namespace App;

use App\Backuper\BackupSender;
use Illuminate\Database\Eloquent\Model;

class Backup extends Model
{
	protected $guarded = [];
    public function domain()
	{
		return $this->belongsTo('App\Domain');
	}

	public function statuses()
	{
		return $this->hasMany('App\BackupStatus');
	}

	public function createStatusFromAkeeba($body)
	{
		return $this->statuses()->create([
			'domain' => $body->Domain,
			'step' => $body->Step,
			'errors' => $body->Error,
			'warnings' => \GuzzleHttp\json_encode($body->Warnings),
			'progress' => $body->Progress,
			'stepnumber' => $body->stepNumber,
			'stepstate' => $body->stepState,
			'bid' => $body->backupid
		]);
	}
	public static function getVersion($url, $secret)
	{
		$salt = 'foo';
		$challenge = $salt . ':' . md5($salt . $secret);
		$json = [
			'encapsulation' => 1,
			'body' => json_encode([
				'challenge' => $challenge,
				'method' => 'getVersion',
				'data' => [],
			])];
		return BackupSender::callAkeeba($url, $json);
	}
}
