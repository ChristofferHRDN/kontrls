<?php
/**
 * Created by PhpStorm.
 * User: christoffer
 * Date: 12/05/16
 * Time: 08:04
 */

namespace App\Auditer;

use App\Domain;
use App\Http\Controllers\Audit\AuditOnData;
use Illuminate\Support\Facades\Log;

class AuditSender
{
	public static function sendAuditRequest(\App\Audit $audit, $type = 'dircount', $file = '.')
	{
		Log::info("Before post $type");
		$currentType = $type;
		$secret = env('AUDITER_SECRET');
		$checksum = md5($type . $file . $secret);
		$client = new \GuzzleHttp\Client();
		$file = Crypter::encryptNoKey($file, $secret);
		$type = Crypter::encryptNoKey($type, $secret);

		$params = [
			'form_params' => [
				'file' => $file,
				'type' => $type,
				'checksum' => $checksum,
			]
		];

		$res = $client->post($audit->domain->audit_url, $params);
		Log::info("After post $currentType");

		return $res->getBody()->__toString();
	}

	public static function sendFetchFilesRequest(\App\Audit $audit, $type = 'dircount', $file = '.', array $files)
	{
		Log::info("Before post $type");
		$currentType = $type;
		$secret = env('AUDITER_SECRET');
		$checksum = md5($type . $file . $secret);
		$client = new \GuzzleHttp\Client();
		$file = Crypter::encryptNoKey($file, $secret);
		$type = Crypter::encryptNoKey($type, $secret);
		$files = Crypter::encryptNoKey(json_encode($files), $secret);

		$params = [
			'form_params' => [
				'file' => $file,
				'type' => $type,
				'checksum' => $checksum,
				'files' => $files
			]
		];

		$res = $client->post($audit->domain->audit_url, $params);
		Log::info("After post $currentType");

		return $res->getBody()->__toString();
	}

	public static function sendVerifyRequest(\App\Domain $domain)
	{
		$type = 'verify';
		$file = '.';
		Log::info('Before verify in the sendVerifyRequest method');
		$currentType = $type;
		$secret = env('AUDITER_SECRET');
		$checksum = md5($type . $file . $secret);
		$client = new \GuzzleHttp\Client();
		$file = Crypter::encryptNoKey($file, $secret);
		$type = Crypter::encryptNoKey($type, $secret);

		$res = $client->post($domain->audit_url, [
			'form_params' => [
				'file' => $file,
				'type' => $type,
				'checksum' => $checksum
			]
		]);

		try {
			list($data, $result, $type) = AuditOnData::parseData($res->getBody()->__toString(), $domain->identifier);
		} catch (\ErrorException $e) {
			throw new AuditParseException('Parsing the response from the server failed. Download a new verison of the syncfile and try again.' .
				' If it continues to fail, send a support message.', 0, $e);
		}

		Log::info('Verify ' . json_encode($result));

		$domain = Domain::where('uniq', array_get($result, 'verify'))->first();

		if ($domain === null) {
			return 'Have you configured your domain correctly? You seem to have an incorrectly configured $uniq field in your worker file.';
		}
		if ($domain->isVerified()) {
			return 'That domain is already verified with another uniq.';
		}

		$domain->verified = true;
		$domain->failed_verification = false;
		$domain->save();
		return 'Successfully verified the domain.';
	}
}