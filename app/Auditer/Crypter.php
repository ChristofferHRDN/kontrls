<?php
/**
 * Created by PhpStorm.
 * User: christoffer
 * Date: 11/05/16
 * Time: 11:06
 */

namespace App\Auditer;

class Crypter
{
	protected static $cipher = 'aes-128-ctr';
	protected static $iv = 7634050080419892;
	
	public static function decrypt($data, $key)
	{
		$output = openssl_decrypt($data, self::$cipher, $key, 0, self::$iv);

		// Uncompress the unencrypted data.
		$output = gzuncompress($output);

		return $output;
	}
	public static function encryptNoKey($data, $secret)
	{
		return openssl_encrypt($data, self::$cipher, $secret, 0, self::$iv);
	}
	public static function decryptNoKey($data, $secret)
	{
		return openssl_decrypt($data, self::$cipher, $secret, 0, self::$iv);
	}
}