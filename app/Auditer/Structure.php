<?php

namespace App\Auditer;

use App\Jobs\Audit\AuditGetFilesFromList;
use Log;
use App\AuditInterestingStructure;
use App\AuditStructure;

class Structure
{
	public static function parseStructure($audit, $data)
	{
		$audit->statuses()->create([
			'status' => 'Begin adding structure. ' . (count($data) - 1) . ' files'
		]);
		foreach ($data as $index => $file) {
			if($index === 'info'){
				continue;
			}
			AuditStructure::create([
				'audit_id' => $audit->id,
				'filename' => $file
			]);
		}
		$audit->statuses()->create([
			'status' => 'Added structure.'
		]);
	}

	public static function parseInterestingStructure($audit, $data)
	{
		$audit->statuses()->create([
			'status' => 'Begin adding interesting structure. ' . (count($data) - 1) . ' files'
		]);
		if(!$audit->total_chunks){
			$audit->total_chunks = array_get($data, 'info.chunks');
		}
		$audit->parsed_chunks += 1;
		$audit->save();
		
		foreach ($data as $index => $file) {
			if($index === 'info'){
				continue;
			}
			AuditInterestingStructure::create([
				'audit_id' => $audit->id,
				'filename' => $file
			]);
		}
		$audit->statuses()->create([
			'status' => 'Added structure.'
		]);
		
		if($audit->parsed_chunks === $audit->total_chunks && $audit->total_chunks !== 0){
			$getFilesJob = new AuditGetFilesFromList($audit);
			dispatch($getFilesJob->onQueue('audit'));
		}
	}
	
}
