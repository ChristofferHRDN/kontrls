<?php
/**
 * Created by PhpStorm.
 * User: christoffer
 * Date: 11/05/16
 * Time: 11:51
 */

namespace App\Auditer;

class FileAndDirCount
{
	public static function saveCount($audit, $data)
	{
		$audit->statuses()->create([
			'status' => 'Being adding file and dir count.'
		]);
		if (array_key_exists('interestingfilecount', $data)) {
			$audit->interesting_filecount = array_get($data, 'interestingfilecount', 0);
		} elseif (array_key_exists('dircount', $data)) {
			$audit->dir_count = array_get($data, 'dircount', 0);
		} elseif (array_key_exists('filecount', $data)) {
			$audit->file_count = array_get($data, 'filecount', 0);
		}
		$audit->save();
		$audit->statuses()->create([
			'status' => 'Added file and dir count.'
		]);
	}

	public static function saveAllCount($audit, $data)
	{
		$audit->statuses()->create([
			'status' => 'Being adding file and dir count.'
		]);

		$audit->interesting_filecount = array_get($data, 'interestingfilecount', 0);
		$audit->dir_count = array_get($data, 'dircount', 0);
		$audit->file_count = array_get($data, 'filecount', 0);
		$audit->save();
		
		$audit->statuses()->create([
			'status' => 'Added file and dir count.'
		]);
	}
}