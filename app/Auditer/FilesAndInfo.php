<?php
/**
 * Created by PhpStorm.
 * User: christoffer
 * Date: 11/05/16
 * Time: 11:58
 */

namespace App\Auditer;

use App\Jobs\Audit\AuditScanFiles;
use App\VulnerabilityScanners\AnonymousScannerFileObject;

class FilesAndInfo
{
	public static function parseFiles($audit, $result)
	{
		$audit->statuses()->create([
			'status' => 'Creating bigger jobs for scanning.'
		]);
		$files = [];

		foreach ($result as $name => $fileData) {
			if ($name === 'info') {
				continue;
			}

			$md5 = array_get($fileData, 'md5');

			$file = new AnonymousScannerFileObject();
			$file->setExtraInfo(['filename' => $name, 'fstat' => array_get($fileData, 'info'), 'md5' => $md5]);
			$file->fwrite(array_get($fileData, 'content'));
			$file->rewind();
			$files[] = $file;
		}
		while (count($files)) {
			$tempFiles = array_splice($files, 0, env('FILESCANBULKCOUNT', 10));

			$job = new AuditScanFiles($audit, $tempFiles);
			$job->handle();
		}
		$audit->statuses()->create([
			'status' => 'Added jobs for scanning files.'
		]);
	}
}