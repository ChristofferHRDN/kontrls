<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class BackupStatus extends Model
{
	protected $guarded = [];
    public function backup()
	{
		return $this->belongsTo('App\Backup');
	}
}
