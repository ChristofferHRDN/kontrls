<?php

namespace App;

use App\Alerter\AlertSender;
use App\Http\FortySixElks;
use Hashids\Hashids;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\App;
use Illuminate\Support\Facades\Mail;
use Laravel\Spark\Repositories\NotificationRepository;

class AlertMethod extends Model
{
	protected $fillable = ['method', 'types', 'alert_domains', 'alert_way'];
	private $methodMapping;

	public static function getValidMethods()
	{
		return [
			'E-Mail' => 'E-Mail',
			//'SMS' => 'SMS',
		];
	}

	public static function getValidTypes()
	{
		return [
			'Uptime' => 'Alert on downtime via the uptime check',
			'Audit weight' => 'Automatic alert based on audited file weight',
		];
	}

	public static function getUptimeAlertMessage()
	{
		return "The page seems to be down!";
	}

	public static function getAuditWeightAlertMessage()
	{
		return '';
	}

	public function alerts()
	{
		return $this->hasMany('App\Alert');
	}

	public function user()
	{
		return $this->belongsTo('App\User');
	}

	public function getTypesAttribute()
	{
		$types = $this->attributes["types"];
		if ($types == null) {
			$this->types = json_encode("[]");
			$this->save();
		}
		return json_decode($this->attributes["types"]);
	}

	public function getAlertDomainsAttribute()
	{
		$ad = json_decode($this->attributes["alert_domains"]);
		if ($ad == "all") {
			return $ad;
		}
		$ad_list = [];
		foreach ($ad as $a) {
			$domain = Domain::find($a);
			if ($domain !== null) {
				$ad_list[] = $domain;
			}
		}
		return $ad_list;
	}

	public function doAlert($alert, $type)
	{
		$this->user->logs()->create([
			'log' => $alert,
			'log_type' => 'Alert'
		]);

		$method = AlertSender::getMethodName($this->method);

		if (method_exists('App\Alerter\AlertSender', $method)) {
			call_user_func('App\Alerter\AlertSender::'.$method, $alert, mb_strtolower(str_replace(' ', '', $type)), $this->alert_way);
		} else {
			echo "That alert method is not supported yet!\n";
		}

		(new NotificationRepository())->create($this->user, [
			'icon' => 'fa-users',
			'body' => 'An alert was created. Go check your ' . $this->method . ' and look at it.',
			'action_text' => 'Or look at it here.',
			'action_url' => route('alert.show', $alert, false)
		]);
	}

	public function createAlert(array $values)
	{
		$alert = new Alert($values);
		$alert->alert_method_id = $this->id;
		$alert->save();

		$hashids = new Hashids('alert_key_id', 8);

		$alert->hashid = $hashids->encode($alert->id);
		$alert->read_id = (new Hashids('alert_key', 40))->encode($alert->id);
		$alert->save();

		return $alert;
	}
}
