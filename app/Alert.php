<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Alert extends Model
{
	protected $fillable = ['message', 'domain_id'];
	protected $hidden = ['domain_id', 'id', 'alert_method_id', 'domain'];
	protected $appends = ['domain_name'];

	public function alert_method()
	{
		return $this->belongsTo('App\AlertMethod');
	}

	public function domain()
	{
		return $this->belongsTo('App\Domain');
	}

	public function getDomainNameAttribute()
	{
		return $this->domain->name;
	}

	public function getRouteKeyName()
	{
		return 'hashid';
	}
}
