<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class AuditInterestingStructure extends Model
{
	protected $fillable = ['audit_id', 'filename'];

	public function audit()
	{
		return $this->belongsTo('App\Audit');
	}
}
