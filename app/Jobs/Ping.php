<?php

namespace App\Jobs;

use App\Domain;
use GuzzleHttp\Client;
use GuzzleHttp\Exception\TransferException;
use GuzzleHttp\TransferStats;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Queue\SerializesModels;

/**
 * Class Ping
 * This job is for checking a domain given a range of different check_types
 *
 * There is the Uptime check, which just checks that the url is up, plain and simple
 * Fails if no response is returned
 * @package App\Jobs
 */
class Ping extends Job implements ShouldQueue
{
	use InteractsWithQueue, SerializesModels;

	protected $domain;
	protected $response;
	protected $___________marker___________;

	/**
	 * Create a new job instance.
	 *
	 * @param Domain $domain
	 */
	public function __construct(Domain $domain)
	{
		$this->___________marker___________ = $domain->id;
		$this->domain = $domain;
	}

	/**
	 * Execute the job.
	 *
	 * @return void
	 */
	public function handle()
	{
		$domain = $this->domain;

		if (!count($domain->monitors)) {
			\Log::warn('Skipping ' . $domain->name . ' because it has no monitors configured.');
			return;
		}

		echo "Beginning " . $domain->url . "\n";

		foreach ($this->domain->monitors as $monitor) {
			if (!$monitor->enabled) {
				continue;
			}

			$shouldAlert = false;
			$alertType = mb_strtolower($monitor->check_type->name);

			echo "Doing " . $monitor->url["url"] . "\n";
			$client = new Client();

			$log = null;

			$userAgent = $domain->settings->getValue('useragent') ? : 'Kntrl.io Ping Job';

			try {
				echo "Inside the try\n";
				$res = $client->request('GET', $monitor->url["url"], [
					'headers' => [
						'User-Agent' => $userAgent
					],
                    'connect_timeout' => 100,
					'timeout' => 1000,
					'on_stats' => function (TransferStats $stats) use ($domain, $monitor, &$shouldAlert) {
						echo "Got stats\n";

						$responseValue = $this->responseValue($stats);

						if (!$responseValue) {
							$shouldAlert = true;
						}

						$check = json_decode($monitor->check);
						$logValues = [
							'monitor' => $monitor->url,
							'stats' => [
								'transfertime' => $stats->getTransferTime()
							],
							'succeeded' => $responseValue ? true : false,
							'check' => ['result' => null, 'check' => $check]
						];

						if ($responseValue) {
							$response = $responseValue;
							$logValues['stats']['response'] = [
								'code' => $response->getStatusCode()
							];
							$method = $monitor->check_type->name;

							$checkResult = $this->$method($stats, $check);
							$logValues['check']['result'] = $checkResult;
							if (!$checkResult) {
								$shouldAlert = true;
							}
						}

						$log = $domain->logs()->create([
							'log' => json_encode($logValues),
							'log_type' => $monitor->check_type->name,
							'model_state' => null,
							'status' => $responseValue ? 'succeeded' : 'failed'
						]);
						echo "Created a log entry\n";
					}
				]);
            } catch (TransferException $e) {
				$shouldAlert = true;

				$check = json_decode($monitor->check);
				$logValues = [
					'monitor' => $monitor->url,
					'stats' => [
						'transfertime' => null
					],
					'succeeded' => false,
					'check' => ['result' => null, 'check' => $check]
				];
                \Log::error($e->getMessage());
				$log = $domain->logs()->create([
					'log' => json_encode($logValues),
					'log_type' => $monitor->check_type->name,
					'status' => 'failed'
				]);
			}


            if ($shouldAlert) {
				dispatch((new Alert($monitor->url, $domain, $alertType))->onQueue('alert'));
			}
		}

		// Queue the job again
		echo "\nQueueing " . $domain->url . " again\n";

		$job = new Ping($domain);
		$polltime = $domain->getPollTimeInSeconds() * 60;

		echo "Waiting " . $polltime . "\n";

		$job->delay($polltime);
		dispatch($job->onQueue('ping'));
	}

	/**
	 * @param TransferStats $stats
	 *
	 * @return bool|null|\Psr\Http\Message\ResponseInterface
	 */
	protected function responseValue(TransferStats $stats)
	{
		if ($stats->hasResponse() && (($response = $stats->getResponse()) !== null)) {
			return $this->response = $response;
		}
		return false;
	}

	protected function uptime(TransferStats $stats, $check)
	{
		$response = $this->response;
		// If there isn't a response, the page did not respond, a fail
		if (!$response) {
			return false;
		} else {
			$statusCode = $response->getStatusCode();

			// Get all status codes, by splitting on "," and removing all empty strings
			$allowedStatusCodes = collect(explode(',', $check->expected))->filter(function ($val) {
				return $val !== "";
			})->map(function ($val) {
				return (int)$val;
			})->toArray();

			if ($check->should_fail) {
				return !in_array($statusCode, $allowedStatusCodes);
			}
			return in_array($statusCode, $allowedStatusCodes);
		}
	}

	protected function regex(TransferStats $stats, $check)
	{
		$body = $this->response->getBody();
		preg_match('/' . $check->check_value . '/', $body, $result);

		if ($check->should_fail) {
			return ['result' => $result != json_decode($check->expected), 'extra' => $result];
		}
		return ['result' => $result == json_decode($check->expected), 'extra' => $result];
	}

	protected function string(TransferStats $stats, $check)
	{
		$body = $this->response->getBody();
		$result = strpos($body, $check->check_value);

		if ($check->should_fail) {
			return $result === false;
		}
		return $result !== false && $result >= 0;
	}
}
