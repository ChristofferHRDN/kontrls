<?php

namespace App\Jobs\Audit;

use App\Jobs\Job;
use Carbon\Carbon;
use Illuminate\Queue\SerializesModels;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Contracts\Queue\ShouldQueue;

class ScheduledAudit extends Job implements ShouldQueue
{
    use InteractsWithQueue, SerializesModels;

    protected $audit;
    protected $scheduledAudit;
    protected $___________marker___________;

    /**
     * Create a new job instance.
     *
     * @param \App\Audit $audit
     * @param \App\ScheduledAudit $scheduledAudit
     */
    public function __construct(\App\Audit $audit, \App\ScheduledAudit $scheduledAudit)
    {
        $this->audit = $audit;
        $this->scheduledAudit = $scheduledAudit;
        $this->___________marker___________ = $this->audit->domain->id;
    }

    /**
     * Execute the job.
     *
     * @return void
     */
    public function handle()
    {
        if ($this->audit->domain->isVerified()) {
            if($this->audit->stopped && count($this->audit->statuses()->where('status', '=', 'Stopped')->get()) === 0){
                $this->audit->statuses()->create(['status'=>'Stopped']);
                return;
            }
            $this->audit->running = true;
            $this->audit->started_audit = Carbon::now();
            $this->audit->save();
            $getCountJob = new AuditGetFileAndDirCount($this->audit);
            dispatch($getCountJob->onQueue('audit'));

            $getStructureJob = new AuditGetStructure($this->audit);
            dispatch($getStructureJob->onQueue('audit'));

            $getInterestingStructureJob = new AuditGetInterestingStructure($this->audit);
            dispatch($getInterestingStructureJob->onQueue('audit'));
        } else {
            dispatch((new AuditJob($this->audit))->onQueue('audit'));
        }
    }
}
