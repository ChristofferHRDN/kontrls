<?php

namespace App\Jobs\Audit;

use App\Audit;
use App\Auditer\AuditSender;
use App\Jobs\Job;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Queue\SerializesModels;

class AuditGetFiles extends Job implements ShouldQueue
{
	use InteractsWithQueue, SerializesModels;

	protected $audit;

	/**
	 * Create a new job instance.
	 *
	 * @param Audit $audit
	 */
	public function __construct(Audit $audit)
	{
		$this->audit = $audit;
	}

	/**
	 * Execute the job.
	 *
	 * @return void
	 */
	public function handle()
	{
		if($this->audit->isDone())
		{
			return;
		}
		if ($this->audit->stopped && count($this->audit->statuses()->where('status', '=', 'Stopped')->get()) === 0) {
			$this->audit->statuses()->create(['status' => 'Stopped']);
			return;
		}
		if ($this->attempts() > 1) {
			$getFilesJob = new AuditGetFilesFromList($this->audit);
			dispatch($getFilesJob->onQueue('audit'));
			return;
		}
		echo "Get files before\n";
		AuditSender::sendAuditRequest($this->audit, 'filesandinfo');
		echo "Get files\n";
	}
}
