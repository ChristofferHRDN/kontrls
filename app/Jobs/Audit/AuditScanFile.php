<?php

namespace App\Jobs\Audit;

use App\Audit;
use App\AuditedFile;
use App\Jobs\Job;
use App\VulnerabilityScanners\AnonymousScannerFileObject;
use App\VulnerabilityScanners\Scanners\GeneralScanner;
use Carbon\Carbon;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Queue\SerializesModels;

class AuditScanFile extends Job implements ShouldQueue
{
	use InteractsWithQueue, SerializesModels;

	protected $audit;
	protected $tempFile;

	/**
	 * Create a new job instance.
	 *
	 * @param Audit $audit
	 * @param AnonymousScannerFileObject $tempFile
	 */
	public function __construct(Audit $audit, AnonymousScannerFileObject $tempFile)
	{
		$this->audit = $audit;
		$this->tempFile = $tempFile;
	}

	/**
	 * Execute the job.
	 *
	 * @return void
	 */
	public function handle()
	{
		if($this->audit->isDone())
		{
			return;
		}
		if ($this->audit->stopped && count($this->audit->statuses()->where('status', '=', 'Stopped')->get()) === 0) {
			$this->audit->statuses()->create(['status' => 'Stopped']);
			return;
		}
		if ($this->audit->files()->where('filename', '=', $this->tempFile->getFilename())->count()) {
			echo "Skipped " . $this->tempFile->getFilename();
			return;
		}
		$this->audit->statuses()->create([
			'status' => 'Begin scanning ' . $this->tempFile->getFilename()
		]);
		$scanner = new GeneralScanner();
		$result = $scanner->scanFile($this->tempFile);

		AuditedFile::create(['audit_id' => $this->audit->id,
			'filename' => $this->tempFile->getFilename(),
			'caught' => $result['caught'],
			'weight' => $result['weight'],
			'lines' => json_encode($result['lines']),
			'explanations' => json_encode($result['explanations']),
			'stats' => json_encode($this->tempFile->getStats())]);

		$this->audit->statuses()->create([
			'status' => 'Scanned ' . $this->tempFile->getFilename()
		]);
		$count = DB::select("select count(DISTINCT filename) as aggregate from `audited_files` where `audited_files`.`audit_id` = ? and `audited_files`.`audit_id` is not null group by `audit_id`
", [$this->audit->id])[0]->aggregate;
		if ($count === $this->audit->interesting_filecount) {
			$this->audit->running = false;
			$this->audit->finished = true;
			$this->audit->stopped_audit = Carbon::now();
			$this->audit->save();
			$this->audit->statuses()->create([
				'status' => 'Done'
			]);
		}

	}
}
