<?php

namespace App\Jobs\Audit;

use App\Alert;
use App\Audit;
use App\AuditedFile;
use App\Events\AuditFinished;
use App\Jobs\Job;
use App\VulnerabilityScanners\AnonymousScannerFileObject;
use App\VulnerabilityScanners\Scanners\GeneralScanner;
use Carbon\Carbon;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Queue\SerializesModels;
use Illuminate\Support\Facades\DB;

class AuditScanFiles extends Job implements ShouldQueue
{
	use InteractsWithQueue, SerializesModels;

	protected $audit;
	protected $files;

	/**
	 * Create a new job instance.
	 *
	 * @param Audit $audit
	 * @param array $files
	 */
	public function __construct(Audit $audit, array $files)
	{
		$this->audit = $audit;
		$this->files = $files;
	}

	/**
	 * Execute the job.
	 *
	 * @return void
	 */
	public function handle()
	{
		if($this->audit->isDone())
		{
			return;
		}
		$caughtFileCountOverMinimum = 0;
		$minimum = $this->audit->domain->getSetting('minimum_audit_alert_weight', 10000);

		$crazyHighCount = 0;
		$crazyHighLimit = 5000;

		/** @var AnonymousScannerFileObject $file */
		foreach ($this->files as $file) {
			if ($this->audit->stopped &&
				count($this->audit->statuses()->where('status', '=', 'Stopped')->get()) === 0
			) {
				$this->audit->statuses()->create(['status' => 'Stopped']);
				return;
			}
			if ($this->audit->files()->where('filename', '=', $file->getFilename())->count()) {
				echo "Skipped " . $file->getFilename() . "\n";
				continue;
			}

			$scanner = new GeneralScanner();
			$result = $scanner->scanFile($file);

			$data = ['audit_id' => $this->audit->id,
				'filename' => $file->getFilename(),
				'caught' => $result['caught'],
				'weight' => $result['weight'],
				'lines' => json_encode($result['lines']),
				'explanations' => json_encode($result['explanations']),
				'stats' => json_encode($file->getStats()),
				'md5' => $file->getMD5()];

			if (in_array($file->getMD5(), $this->audit->user->allowedHashes())) {
				$this->audit->statuses()->create([
					'status' => 'Skipping ' . $file->getFilename() . ' because it\'s MD5 is whitelisted by the user.'
				]);
				$data['caught'] = false;
				$data['explanations'] =
					json_encode(array_merge(['This file has been flagged allowed by you via the MD5. ' .
						'We did however still scan it.'], $result['explanations']));
			}

			AuditedFile::create($data);

			if ($result['weight'] >= $minimum) {
				$caughtFileCountOverMinimum += 1;
			}
			if ($result['weight'] >= $crazyHighLimit) {
				$crazyHighCount += 1;
			}

			$count = DB::select("SELECT count(DISTINCT filename) AS aggregate FROM `audited_files` WHERE" .
				" `audited_files`.`audit_id` = ? AND `audited_files`.`audit_id` IS NOT NULL GROUP BY `audit_id`",
				[$this->audit->id])[0]->aggregate;
			if ($count >= $this->audit->interesting_filecount) {
				$this->audit->forceFill([
					'running' => false,
					'finished' => true,
					'stopped_audit' => Carbon::now(),
				])->save();

				$this->audit->statuses()->create([
					'status' => 'Done'
				]);
				event(new AuditFinished($this->audit));

				echo "Finished audit\n";

				if ($this->audit->initiated_by === "scheduling") {
					$this->audit->domain->scheduledAudit->forceFill([
						'last_audit' => Carbon::now(),
						'next_audit' => Carbon::now()->addSeconds($this->audit->domain->scheduledAudit->wait_time),
					])->save();

					if($this->audit->domain->scheduledAudit->enabled){
						$finishScheduledAuditJob = new FinishScheduledAudit($this->audit->domain->scheduledAudit, $this->audit);
						dispatch($finishScheduledAuditJob->onQueue('audit'));

						$job = new ScheduledAudit($this->audit, $this->audit->domain->scheduledAudit);
						dispatch($job->onQueue('audit')->delay($this->audit->domain->scheduledAudit->wait_time));
					}
				}
			}
		}
		echo $caughtFileCountOverMinimum . " files caught with weight over " . $minimum . "\n";
		if ($caughtFileCountOverMinimum && $this->audit->initiated_by == 'scheduling') {
			echo "Adding an alert \n";
			\Log::info($caughtFileCountOverMinimum . " files caught with weight over " . $minimum . "\n");
			$url = $this->audit->domain->urls()->first();
			
			$alert = new \App\Jobs\Alert($url, $this->audit->domain, 'Audit weight', $caughtFileCountOverMinimum .
				' files were caught having over ' .
				$minimum .
				' weight in an audit. Click to see: ' .
				route('domain.audit.show', [$this->audit->domain, $this->audit], true));
			echo "Dispatching the job \n";
			dispatch($alert->onQueue('alert'));
		}
	}
}
