<?php

namespace App\Jobs\Audit;

use App\Jobs\Job;
use Carbon\Carbon;
use Illuminate\Queue\SerializesModels;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Contracts\Queue\ShouldQueue;

class FinishScheduledAudit extends Job implements ShouldQueue
{
    use InteractsWithQueue, SerializesModels;
    
    protected $audit;
    protected $scheduledAudit;

    /**
     * Create a new job instance.
     *
     * @param ScheduledAudit|\App\ScheduledAudit $scheduledAudit
     * @param \App\Audit|Audit $audit
     */
    public function __construct(\App\ScheduledAudit $scheduledAudit, \App\Audit $audit)
    {
        $this->scheduledAudit = $scheduledAudit;
        $this->audit = $audit;
    }

    /**
     * Execute the job.
     *
     * @return void
     */
    public function handle()
    {
        
    }
}
