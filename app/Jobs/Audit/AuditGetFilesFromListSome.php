<?php

namespace App\Jobs\Audit;

use App\Audit;
use App\Auditer\AuditSender;
use App\Jobs\Job;
use Illuminate\Queue\SerializesModels;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Contracts\Queue\ShouldQueue;

class AuditGetFilesFromListSome extends Job implements ShouldQueue
{
    use InteractsWithQueue, SerializesModels;

    protected $audit;
    protected $files;

    /**
     * Create a new job instance.
     *
     * @param Audit $audit
     * @param array $files
     */
    public function __construct(Audit $audit, array $files)
    {
        $this->audit = $audit;
        $this->files = $files;
    }

    /**
     * Execute the job.
     *
     * @return void
     */
    public function handle()
    {
        if($this->audit->isDone())
        {
            return;
        }
        echo "Before AuditGetFilesFromListSOme\n";
        AuditSender::sendFetchFilesRequest($this->audit, 'filesfromlist', '', ['files' => json_encode($this->files)]);
        echo "After AuditGetFilesFromListSOme\n";
        $job = new AuditGetFilesFromList($this->audit, 100);
        dispatch($job->onQueue('audit'));
    }
}
