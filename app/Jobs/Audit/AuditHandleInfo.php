<?php

namespace App\Jobs\Audit;

use App\Auditer\FileAndDirCount;
use App\Auditer\FilesAndInfo;
use App\Auditer\Structure;
use Log;
use App\Jobs\Job;
use Illuminate\Queue\SerializesModels;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Contracts\Queue\ShouldQueue;

class AuditHandleInfo extends Job implements ShouldQueue
{
    use InteractsWithQueue, SerializesModels;

    protected $type;
    protected $audit;
    protected $result;

    /**
     * Create a new job instance.
     *
     * @param $type
     * @param $audit
     * @param $result
     */
    public function __construct($type, $audit, $result)
    {
        $this->type = $type;
        $this->audit = $audit;
        $this->result = $result;
    }

    /**
     * Execute the job.
     *
     * @return void
     */
    public function handle()
    {
        if($this->audit->isDone())
        {
            return;
        }
        $type = $this->type;
        $audit = $this->audit;
        $result = $this->result;

        echo "Handle info: $type\n";
        
        switch ($type) {
            case 'interestingfilecount':
            case 'dircount':
            case 'filecount':
                Log::info("Save count");
                FileAndDirCount::saveCount($audit, $result);
                break;
            case 'structure':
                Log::info("Save structure");
                Structure::parseStructure($audit, $result);
                break;
            case 'interestingstructure':
                Log::info("Save interesting structure");
                Structure::parseInterestingStructure($audit, $result);
                break;
            case
            'filesandinfo':
                Log::info("Parse files");
                FilesAndInfo::parseFiles($audit, $result);
                break;
            case 'allcount':
                Log::info("Save all count");
                FileAndDirCount::saveAllCount($audit, $result);
                break;
        }
        echo "Handled $type\n";
    }
}
