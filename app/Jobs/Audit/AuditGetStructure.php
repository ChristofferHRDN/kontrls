<?php

namespace App\Jobs\Audit;

use App\Audit;
use App\Auditer\AuditSender;
use App\Auditer\Crypter;
use App\Jobs\Job;
use Illuminate\Queue\SerializesModels;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Contracts\Queue\ShouldQueue;

class AuditGetStructure extends Job implements ShouldQueue
{
    use InteractsWithQueue, SerializesModels;

    protected $audit;

    /**
     * Create a new job instance.
     *
     * @param Audit $audit
     */
    public function __construct(Audit $audit)
    {
        $this->audit = $audit;
    }

    /**
     * Execute the job.
     *
     * @return void
     */
    public function handle()
    {
        if($this->audit->isDone())
        {
            return;
        }
        if($this->audit->stopped && count($this->audit->statuses()->where('status', '=', 'Stopped')->get()) === 0){
            $this->audit->statuses()->create(['status'=>'Stopped']);
            return;
        }
        AuditSender::sendAuditRequest($this->audit, 'structure');
        echo "Get structure \n";
    }
}
