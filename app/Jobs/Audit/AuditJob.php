<?php

namespace App\Jobs\Audit;

use App\Jobs\Job;
use Carbon\Carbon;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Queue\SerializesModels;
use Laravel\Spark\Repositories\NotificationRepository;

class AuditJob extends Job implements ShouldQueue
{
	use InteractsWithQueue, SerializesModels;

	protected $audit;

	/**
	 * Create a new job instance.
	 *
	 * @param \App\Audit $audit
	 */
	public function __construct(\App\Audit $audit)
	{
		$this->audit = $audit;
	}

	/**
	 * Execute the job.
	 *
	 * @return void
	 */
	public function handle()
	{
		if($this->audit->isDone())
		{
			return;
		}
		if($this->audit->domain === null){
			$this->audit->statuses()->create([
				'status' => 'The domain was forcefully removed. Nothing to do an audit on.'
			]);
			return;
		}
		if ($this->audit->domain->isVerified()) {
			if ($this->audit->stopped &&
				count($this->audit->statuses()->where('status', '=', 'Stopped')->get()) === 0
			) {
				$this->audit->statuses()->create(['status' => 'Stopped']);
				return;
			}
			$this->audit->running = true;
			$this->audit->started_audit = Carbon::now();
			$this->audit->save();
			
			$getCountJob = new AuditGetFileAndDirCount($this->audit);
			dispatch($getCountJob->onQueue('audit'));

			$getStructureJob = new AuditGetStructure($this->audit);
			dispatch($getStructureJob->onQueue('audit'));

			$getInterestingStructureJob = new AuditGetInterestingStructure($this->audit);
			dispatch($getInterestingStructureJob->onQueue('audit'));
		} else {
			(new NotificationRepository())->create($this->audit->user, [
				'icon' => 'fa-cloud',
				'body' => 'You need to verify your domain before you can audit it.',
			]);
			//dispatch((new AuditJob($this->audit))->onQueue('audit'));
		}
	}
}
