<?php

namespace App\Jobs\Audit;

use App\Audit;
use App\Auditer\AuditSender;
use App\Jobs\Job;
use Illuminate\Queue\SerializesModels;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Contracts\Queue\ShouldQueue;

class AuditGetInterestingStructure extends Job implements ShouldQueue
{
    use InteractsWithQueue, SerializesModels;

    protected $audit;

    /**
     * Create a new job instance.
     *
     * @param Audit $audit
     */
    public function __construct(Audit $audit)
    {
        $this->audit = $audit;
    }

    /**
     * Execute the job.
     *
     * @return void
     */
    public function handle()
    {
        if($this->audit->isDone())
        {
            return;
        }
        if($this->audit->stopped && count($this->audit->statuses()->where('status', '=', 'Stopped')->get()) === 0){
            $this->audit->statuses()->create(['status'=>'Stopped']);
            return;
        }
        echo "Interesting structure fetching\n";
        AuditSender::sendAuditRequest($this->audit, 'interestingstructure');
        echo "Got interesting structure, now queueing up the file fetching. \n";
    }
}
