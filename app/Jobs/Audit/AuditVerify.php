<?php

namespace App\Jobs\Audit;

use App\Auditer\AuditSender;
use App\Jobs\Job;
use Illuminate\Queue\SerializesModels;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Contracts\Queue\ShouldQueue;

class AuditVerify extends Job implements ShouldQueue
{
    use InteractsWithQueue, SerializesModels;

    protected $audit;

    /**
     * Create a new job instance.
     *
     * @param \App\Audit $audit
     */
    public function __construct(\App\Audit $audit)
    {
        $this->audit = $audit;
    }

    /**
     * Execute the job.
     *
     * @return void
     */
    public function handle()
    {
        if($this->audit->stopped && count($this->audit->statuses()->where('status', '=', 'Stopped')->get()) === 0){
            $this->audit->statuses()->create(['status'=>'Stopped']);
            return;
        }
        AuditSender::sendAuditRequest($this->audit, 'verify');
    }
}
