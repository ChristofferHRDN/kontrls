<?php

namespace App\Jobs\Audit;

use App\Audit;
use App\Jobs\Job;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Queue\SerializesModels;
use Illuminate\Support\Facades\DB;

class AuditGetFilesFromList extends Job implements ShouldQueue
{
	use InteractsWithQueue, SerializesModels;

	protected $audit;
	protected $limit;

	/**
	 * Create a new job instance.
	 *
	 * @param Audit $audit
	 */
	public function __construct(Audit $audit, $limit = 100)
	{
		$this->audit = $audit;
		$this->limit = $limit;
	}

	/**
	 * Execute the job.
	 *
	 * @return void
	 */
	public function handle()
	{
		if($this->audit->isDone())
		{
			return;
		}
		if ($this->audit->stopped && count($this->audit->statuses()->where('status', '=', 'Stopped')->get()) === 0) {
			$this->audit->statuses()->create(['status' => 'Stopped']);
			return;
		}
		$result =
			DB::select("SELECT `audit_interesting_structures`.`filename` FROM `audit_interesting_structures` LEFT JOIN `audited_files` ON `audited_files`.`audit_id` = `audit_interesting_structures`.`audit_id` AND `audited_files`.`filename` = `audit_interesting_structures`.`filename` WHERE `audited_files`.`id` IS NULL AND `audit_interesting_structures`.`audit_id` = ? LIMIT ?;", [$this->audit->id,
				$this->limit]);

		$files = collect($result)->map(function ($entry) {
			return $entry->filename;
		});
		if ($result) {
			$job = new AuditGetFilesFromListSome($this->audit, $files->toArray());
			dispatch($job->onQueue('audit'));
		}
	}
}
