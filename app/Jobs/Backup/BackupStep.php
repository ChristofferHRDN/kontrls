<?php

namespace App\Jobs\Backup;

use App\Backup;
use App\Backuper\AkeebaException;
use App\Backuper\BackupException;
use App\Backuper\BackupSender;
use App\Backuper\NotAnAkeebaInstallationException;
use App\Domain;
use App\Jobs\Job;
use Carbon\Carbon;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Queue\SerializesModels;

class BackupStep extends Job implements ShouldQueue
{
	use InteractsWithQueue, SerializesModels;

	protected $backup;
	protected $salt = 'foo';
	protected $challenge;
	protected $domain;
	protected $___________marker___________;

	/**
	 * Create a new job instance.
	 *
	 * @param Domain $domain
	 * @param Backup $backup
	 */
	public function __construct(Domain $domain, Backup $backup)
	{
		$this->___________marker___________ = $domain->id;
		$this->backup = $backup;
		$this->domain = $domain;
		$this->challenge = $this->salt . ':' . md5($this->salt . $this->domain->getSetting('backup_secret'));
	}

	/**
	 * Execute the job.
	 *
	 * @return void
	 */
	public function handle()
	{
		$json = [
			'encapsulation' => 1,
			'body' => json_encode([
				'challenge' => $this->challenge,
				'method' => 'stepBackup',
				'data' => [
					/*'description' => 'Backup taken',
					'comment' => 'My comment',*/
					'backupid' => $this->backup->backupid
				],
			])];

		try{
			$body = BackupSender::callAkeeba($this->domain->url, $json);
		} catch(NotAnAkeebaInstallationException $e) {
			echo "Not an akeeba installation exception received.\n";
			\Log::error('Dropping the backup of ' . $this->domain->url . ' because it does not look like a proper akeeba page. ' . $e->getMessage());

			$this->domain->verified_backup = 0;
			$this->domain->has_backup_queued = false;
			$this->domain->save();
			$this->domain->logs()->create([
				'log' => json_encode(['message' => $e->getMessage()]),
				'log_type' => 'Backup',
				'status' => 'failed',
				'model_state' => json_encode($e)
			]);
			return;
		} catch(AkeebaException $e){
			echo "Akeeba exception.\n";
			\Log::error('Dropping the backup of ' . $this->domain->url . ' because we received some exception that shouldn\' be sent. ' . $e->getMessage());

			$this->domain->verified_backup = 0;
			$this->domain->has_backup_queued = false;
			$this->domain->save();
			$this->domain->logs()->create([
				'log' => json_encode(['message' => $e->getMessage()]),
				'log_type' => 'Backup',
				'status' => 'failed',
				'model_state' => json_encode($e)
			]);
			return;
		} catch(BackupException $e) {
			echo "Akeeba exception received.\n";
			\Log::error('The akeeba installation returned an exception ' . $e->getMessage());
			$this->domain->logs()->create([
				'log' => json_encode(['message' => $e->getMessage()]),
				'log_type' => 'Backup',
				'status' => 'failed',
				'model_state' => json_encode($e)
			]);
			throw $e;
		}

		if (!$this->backup->archive) {
			$this->backup->archive = $body->Archive;
			$this->backup->save();
		}

		$status = $this->backup->createStatusFromAkeeba($body);
		if ($status->errors === '' && $status->stepstate !== 'finished') {
			$job = new BackupStep($this->domain, $this->backup);
			dispatch($job->onQueue('backup'));
		} else {
			$this->backup->stopped_at = Carbon::now();
			$this->backup->save();
			$this->domain->has_backup_queued = false;
			$this->domain->save();

			if($this->backup->initiated_by === "scheduling"){
				$scheduledBackup = $this->domain->user->scheduledBackup;
				$job = new BackupStart($this->domain, true);
				$delay = $scheduledBackup->wait_time;
				dispatch($job->delay($delay));
				$scheduledBackup->forceFill([
					'last_backup' => Carbon::now(),
					'next_backup' => Carbon::now()->addSeconds($delay)
				])->save();
			}
		}
	}
}
