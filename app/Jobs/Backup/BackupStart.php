<?php

namespace App\Jobs\Backup;

use App\Backup;
use App\Backuper\AkeebaException;
use App\Backuper\BackupException;
use App\Backuper\BackupSender;
use App\Backuper\NotAnAkeebaInstallationException;
use App\Domain;
use App\Jobs\Job;
use Carbon\Carbon;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Queue\SerializesModels;

class BackupStart extends Job implements ShouldQueue
{
	use InteractsWithQueue, SerializesModels;

	protected $salt = 'foo';
	protected $challenge;
	protected $scheduled;
	protected $domain;
	protected $___________marker___________;

	/**
	 * Create a new job instance.
	 *
	 * @param Domain $domain
	 * @param bool $scheduled
	 */
	public function __construct(Domain $domain, $scheduled = false)
	{
		$this->___________marker___________ = $domain->id;
		$this->domain = $domain;
		$this->scheduled = $scheduled;
		$this->challenge = $this->salt . ':' . md5($this->salt . $this->domain->getSetting('backup_secret'));
	}

	/**
	 * Execute the job.
	 *
	 * @throws AkeebaException
	 */
	public function handle()
	{
		$json = [
			'encapsulation' => 1,
			'body' => json_encode([
				'challenge' => $this->challenge,
				'method' => 'startBackup',
				'data' => [
					'description' => 'Backup taken',
					'comment' => 'My comment'
				],
			])];
		try{
			$body = BackupSender::callAkeeba($this->domain->url, $json);
		} catch(NotAnAkeebaInstallationException $e) {
			echo "Not an akeeba installation exception received.\n";
			\Log::error('Dropping the backup of ' . $this->domain->url . ' because it does not look like a proper akeeba page. ' . $e->getMessage());

			$this->domain->verified_backup = 0;
			$this->domain->has_backup_queued = false;
			$this->domain->save();
			$this->domain->logs()->create([
				'log' => json_encode(['message' => $e->getMessage()]),
				'log_type' => 'Backup',
				'status' => 'failed',
			]);
			return;
		} catch(BackupException $ae) {
			echo "Akeeba exception received.\n";
			\Log::error('The akeeba installation returned an exception ' . $ae->getMessage());
			throw $ae;
		}

		$backup = Backup::create([
			'started_at' => Carbon::now(),
			'initiated_by' => $this->scheduled ? 'scheduling' : 'user',
			'domain_id' => 3,
			'backupid' => $body->backupid,
		]);

		$backup->createStatusFromAkeeba($body);
		
		if($body->Archive){
			$backup->archive = $body->Archive;
			$backup->save();
		}

		$job = new BackupStep($this->domain, $backup);
		dispatch($job->onQueue('backup'));
	}
}
