<?php

namespace App\Jobs;

use App\AlertMethod;
use App\Domain;
use App\Repositories\AlertRepository;
use App\Url;
use Carbon\Carbon;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Queue\SerializesModels;
use Illuminate\Support\Collection;

class Alert extends Job implements ShouldQueue
{
	use InteractsWithQueue, SerializesModels;

	/* @var \App\Domain $domain */
	protected $domain = null;
	protected $url = null;
	protected $alertType = '';
	protected $alertMethods = null;
	protected $message = '';
	protected $repo;

	/**
	 * Create a new job instance.
	 *
	 * @param Url $url
	 * @param Domain $domain
	 * @param $type
	 * @param string $message
	 */
	public function __construct(Url $url = null, Domain $domain, $type, $message = '')
	{
		$this->url = $url;
		$this->domain = $domain;
		$this->alertType = $type;
		$this->message = $message;
		$this->repo = new AlertRepository();
	}

	/**
	 * Execute the job.
	 *
	 * @return void
	 */
	public function handle()
	{
		$methods = collect($this->domain->user->alert_methods);
		$alertType = $this->alertType;

		$this->alertMethods = $this->getAlertMethods($methods, $alertType);

		if ($this->alertMethods->count()) {
			$this->doAlert();
		} else {
			echo "No methods found for that url and alert type.\n";
		}
	}

	/**
	 * @param Collection $allMethods
	 * @param string $alertType
	 *
	 * @return Collection
	 */
	protected function getAlertMethods(Collection $allMethods, string $alertType)
	{
		// Filter out all methods that aren't relevant
		// First the methods that aren't targeted to this domain
		// Then check if the alertType is in the method types
		// Then if the alert way is unique, i.e. no two emails with the same domain
		return $allMethods->filter(function ($method) {
			return $method->alert_domains === "all" || in_array($this->domain, $method->alert_domains);
		})->filter(function ($value) use ($alertType) {
			return Alert::checkIfTypeInTypes($alertType, collect($value->types));
		})->unique(function ($item) {
			return $item->alert_way;
		});
	}

	/**
	 * @param string $alertType
	 * @param Collection $types
	 *
	 * @return bool
	 */
	protected static function checkIfTypeInTypes(string $alertType, Collection $types)
	{
		return in_array(mb_strtolower($alertType), $types->map(function ($type) {
			return Alert::convertTypeToLower($type);
		})->toArray());
	}

	/**
	 * @param string $type
	 *
	 * @return mixed|string
	 */
	protected static function convertTypeToLower(string $type)
	{
		return mb_strtolower($type);
	}

	/**
	 *
	 */
	public function doAlert()
	{
		if ($this->shouldAlertBasedOnTimestamps()) {
			echo "Doing this alert.\n";
			$this->saveLastAlerts();
			if ($this->url === null) {
				$url = $this->domain->url;
			} else {
				$url = $this->url->url;
			}

			foreach ($this->alertMethods as $index => $method) {
				/** @var AlertMethod $method */
				$alert = $this->repo->createAlert($method->id, [
					'message' => $this->message .
						' ' .
						$url .
						' ' .
						call_user_func('App\AlertMethod::get' .
							str_replace(' ', '', $this->alertType) .
							'AlertMessage'),
					'domain_id' => $this->domain->id,
				]);
				$method->doAlert($alert, $this->alertType);
			}
		} else {
			echo "Skipping this alert.\n";
		}
	}

	/**
	 * Decides whether to send an alert based on the timestamps found in last_alerts,
	 * so that we do not send alerts to the same domain all the time, if the checking has a short time
	 *
	 * @return bool
	 */
	protected function shouldAlertBasedOnTimestamps()
	{
		if($this->url === null){
			$url = new \stdClass();
			$url->url = $this->domain->url;
		} else {
			$url = $this->url;
		}
		$latestAlert = $this->domain->getLatestAlert($url, $this->alertType);
		$time = Carbon::createFromTimestamp($latestAlert)->addMinutes($this->domain->getTimeBetweenAlerts());
		$result = $time->lt(Carbon::now());
		return $result;
	}

	/**
	 * Messy but functional
	 * Saves data to the domain and url regarding when things were alerted last time
	 * So that you can enable selective alerting
	 */
	protected function saveLastAlerts()
	{
		if ($this->url === null) {
			$url = $this->domain->url;
		} else {
			$url = $this->url->url;
		}
		$alert = [$this->alertType => time()];

		$lastAlerts = json_decode($this->domain->last_alerts, true);
		if ($lastAlerts === NULL) {
			$lastAlerts = [$url => $alert];
		} else {
			$lastAlerts[$url] = array_merge($lastAlerts[$url], $alert);
		}
		$this->domain->last_alerts = json_encode($lastAlerts);
		$this->domain->save();

		if ($this->url !== null) {
			$urlLastAlerts = json_decode($this->url->last_alerts, true);
			if ($urlLastAlerts === NULL) {
				$urlLastAlerts = $alert;
			} else {
				$urlLastAlerts = array_merge($urlLastAlerts, $alert);
			}
			$this->url->last_alerts = json_encode($urlLastAlerts);
			$this->url->save();
		}
	}
}
