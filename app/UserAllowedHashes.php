<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class UserAllowedHashes extends Model
{
    protected $table = 'user_allowed_file_hashes';
	protected $fillable = ['md5'];

	protected function user()
	{
		return $this->belongsTo('App\User');
	}
}
