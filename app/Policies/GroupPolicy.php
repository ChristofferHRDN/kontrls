<?php

namespace App\Policies;

use App\Group;
use App\User;
use Illuminate\Auth\Access\HandlesAuthorization;
use Laravel\Spark\Spark;

class GroupPolicy
{
	use HandlesAuthorization;

	public function before($user, $ability)
	{
		if (Spark::developer($user->email)) {
			return true;
		}
	}

	public function destroy(User $user, Group $group)
	{
		return $user->id === $group->user_id;
	}

	public function update(User $user, Group $group)
	{
		return $user->id === $group->user_id;
	}

	public function add_domain(User $user, Group $group)
	{
		return $user->id === $group->user_id;
	}

	public function remove_domain(User $user, Group $group)
	{
		return $user->id === $group->user_id;
	}
}
