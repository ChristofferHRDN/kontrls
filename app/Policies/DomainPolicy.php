<?php

namespace App\Policies;

use App\Domain;
use App\User;
use Illuminate\Auth\Access\HandlesAuthorization;
use Laravel\Spark\Spark;

class DomainPolicy
{
    use HandlesAuthorization;

    public function before($user, $ability, Domain $domain)
    {
        if(Spark::developer($user->email)){
            return true;
        }
        // TODO: This is temporary, and should be overridden later so that the user is limited by their spark plan
        if($user->id === $domain->user_id){
            return true;
        }
    }
    /**
     * Determine if the given user can delete the given domain
     * @param User $user
     * @param Domain $domain
     * @return bool
     */

    public function create(User $user)
    {
        return true;
    }
}
