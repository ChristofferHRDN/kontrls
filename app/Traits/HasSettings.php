<?php
namespace App\Traits;

use App\Settings;

trait HasSettings
{
	public function settings()
	{
		return $this->morphOne('App\Settings', 'settingsable');
	}

	public function setSettings($merge = true, ...$values)
	{
		$settings = [];
		$settings = array_merge($settings, ...$values);

		if ($merge) {
			$settings = array_merge($this->settings->values, $settings);
		}
		return $this->settings->setValues($settings);
	}

	public function setSettingsArrayAttribute($values)
	{
		$this->settings->update($values);
	}

	public function getDefaults($jsonify = true)
	{
		$function = 'get' . $this->getSettingsDefaultName() . 'Defaults';
		return Settings::$function($jsonify);
	}

	public function getSetting($key, $default = null)
	{
		return $this->settings->getValue($key, $default);
	}
}