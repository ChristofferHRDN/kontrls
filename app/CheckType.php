<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class CheckType extends Model
{
    protected $fillable = ['name'];

    public function monitors()
    {
        return $this->hasMany('App\Monitor');
    }
}
