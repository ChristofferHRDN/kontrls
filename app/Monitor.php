<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

/**
 * @property bool enabled
 */
class Monitor extends Model
{
    use Traits\HasSettings;
    
    protected $fillable = ['url_id', 'check_type_id', 'check'];
    protected $with = ['url','check_type'];
    
    public function domain()
    {
        return $this->belongsTo('App\Domain');
    }

    public function check_type()
    {
        return $this->belongsTo('App\CheckType');
    }

    public function url()
    {
        return $this->belongsTo('App\Url');
    }
}
