<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Note extends Model
{
	protected $fillable = ['note', 'user_id'];
	
    public function notable()
	{
		return $this->morphTo();
	}
}
