<?php

namespace App\Repositories;

use App\Alert;
use Hashids\Hashids;

class AlertRepository
{
	public static function createAlert($id, array $values)
	{
		$alert = new Alert($values);
		$alert->alert_method_id = $id;
		$alert->save();

		$hashids = new Hashids('alert_key_id', 8);

		$alert->hashid = $hashids->encode($alert->id);
		$alert->read_id = (new Hashids('alert_key', 40))->encode($alert->id);
		$alert->save();

		return $alert;
	}
}