<?php

namespace App\Repositories;

use App\Audit;
use Hashids\Hashids;

class AuditRepository
{
	public function create(array $values)
	{
		$hashids = new Hashids('audit_key', 8);

		$audit = new Audit($values);
		$audit->save();
		$audit->hashid = $hashids->encode($audit->id);
		$audit->save();

		return $audit;
	}
}