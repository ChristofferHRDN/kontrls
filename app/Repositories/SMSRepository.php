<?php

namespace App\Repositories;

use App\SentSMS;
use GuzzleHttp\Client;

class SMSRepository
{
	public static function sendSMS($number, $message, $from = null)
	{
		if($from === null){
			$from = substr(env('ELKSSMS_FROM', 'Kontrlio'), 0, 11);
		}
		$sms = array(
			'from' => $from,   /* Can be up to 11 alphanumeric characters */
			'to' => '+46700000000',  /* The mobile number you want to send to */
			'message' => $message
		);
		$client = new Client();

		$response = $client->post('https://api.46elks.com/a1/SMS', [
			'auth' => [
				env('ELKSUSERNAME'),
				env('ELKSPASSWORD'),
			],
			'body' => http_build_query($sms),
			'timeout' => 10
		]);
		
		return [SentSMS::create(['from' => $from, 'number' => $number, 'message' => $message]), $response];
	}
}