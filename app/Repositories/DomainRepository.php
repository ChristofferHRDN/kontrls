<?php

namespace App\Repositories;

use App\Domain;
use App\User;
use Carbon\Carbon;

class DomainRepository
{
    public function forUser(User $user)
    {
        return Domain::where('user_id', $user->id)
            ->orderBy('created_at', 'asc')
            ->get();
    }

    public function getUptime(Domain $domain, $limit = 100, $limitToDate = true)
    {
        $limit = 100;
        $query = $domain->logs()
                       ->where('log_type', 'Uptime');

        if($limitToDate){
            //$query = $query->whereDate('created_at', '=', Carbon::today()->toDateString());
        }

        $query = $query->orderBy('created_at', 'desc')->take($limit);

        return $query->get();
    }
}