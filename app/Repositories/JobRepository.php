<?php

namespace App\Repositories;

use App\Alert;
use Hashids\Hashids;
use Illuminate\Support\Facades\DB;

class JobRepository
{
    public static function getJobs()
    {
        return DB::select('select * from jobs');
    }
}