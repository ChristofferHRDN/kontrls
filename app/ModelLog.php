<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class ModelLog extends Model
{
    protected $guarded = [];

    protected $table = 'logs';
    
    public function loggable()
    {
        return $this->morphTo();
    }
}
