<?php

namespace App;

use Carbon\Carbon;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\DB;

/**
 * @property bool set_via_group
 * @property mixed wait_time
 * @property int enabled
 * @property mixed last_audit
 * @property Domain domain
 */
class ScheduledBackup extends Model
{
	protected $guarded = [];

	public function domain()
	{
		return $this->belongsTo('App\Domain');
	}

	public function isQueued()
	{
		return DB::table('jobs')
				 ->select('*')
				 ->where('queue', '=', 'backup')
				 ->where('payload', 'like', DB::raw('"%___________marker___________\\\\\";i:' .
					 $this->domain->id .
					 '%" ESCAPE \'≤\''))
				 ->count() === 1;
	}

	public function removeFromQueueAndDisable()
	{
		$this->enabled = 0;
		return $this->removeFromQueue();
	}

	public function removeFromQueue()
	{
		$id = DB::table('jobs')
				->select('id')
				->where('queue', '=', 'backup')
				->where('payload', 'like', DB::raw('"%___________marker___________\\\\\";i:' .
					$this->domain->id .
					'%" ESCAPE \'≤\''))
				->get();
		if (count($id)) {
			$id = $id[0]->id;
			DB::table('jobs')->where('id', '=', $id)->delete();
			return true;
		}
		return false;
	}

	public function updateScheduledBackupTime(int $domainId, int $userId, int $autoWaitTime, bool $setViaGroup = false)
	{
		if ($autoWaitTime !== -1) {
			$waitTimeBefore = $this->wait_time;
			$this->wait_time = (int)$autoWaitTime;
			$this->enabled = 1;

			if ((int)$autoWaitTime !== $waitTimeBefore || !$this->isQueued()) {
				$this->removeFromQueue();

				$scheduledBackupJob = new \App\Jobs\Backup\BackupStart(Domain::find($domainId), true);

				$newTime = Carbon::parse($this->last_audit)->addSeconds($autoWaitTime);

				if ($newTime->lt(Carbon::now())) {
					$waitTime = 0;
				} else {
					$waitTime = $newTime->diffInSeconds(Carbon::now());
				}
				dispatch($scheduledBackupJob->onQueue('backup')->delay($waitTime));
			}
		} else {
			$this->removeFromQueueAndDisable();
		}
		$this->set_via_group = $setViaGroup;
		$this->save();
	}

	public function updateScheduledBackupTimeFromGroup(int $domainId, int $userId, int $autoWaitTime)
	{
		if($this->set_via_group || ($autoWaitTime < $this->wait_time || $this->wait_time === 0 || $this->enabled === 0)){
			$this->updateScheduledBackupTime($domainId, $userId, $autoWaitTime, true);
		}
	}
}
