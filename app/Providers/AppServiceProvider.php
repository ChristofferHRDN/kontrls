<?php

namespace App\Providers;

use App\Alert;
use App\Audit;
use Illuminate\Support\ServiceProvider;

use App\Domain;
use App\Group;
use App\Settings;
use Hashids\Hashids;
use Ramsey\Uuid\Uuid;

class AppServiceProvider extends ServiceProvider
{
    /**
     * Bootstrap any application services.
     *
     * @return void
     */
    public function boot()
    {
        Group::created(function (Group $group) {
            $group->settings()->create(['values' => $group->getDefaults()]);

            // Assign a new hashid, so that we do not use incremental ids as base
            // But it could still be nice to have the id some time, so just have a different field for it
            $hashids = new Hashids('group_key', 8);
            $group->hashid = $hashids->encode($group->id);
            $group->save();
        });
        Domain::created(function (Domain $domain) {
            if(!$domain->name){
                $domain->name = $domain->url;
            }
            $domain->settings()->create(['values' => $domain->getDefaults()]);

            $domain->audit_url = rtrim($domain->url, "/") . '/' . env('SYNCFILE_NAME', 'kntrliosync.php');

            // Assign a new hashid, so that we do not use incremental ids as base
            // But it could still be nice to have the id some time, so just have a different field for it
            $hashids = new Hashids('domain_key', 8);
            $domain->hashid = $hashids->encode($domain->id);
            $domain->uniq = Uuid::uuid4();
            $domain->identifier = Uuid::uuid4();
            $domain->save();

            $log = ['message' => 'Domain was created.'];

            // Log that we created the domain
            $domain->logs()->create([
                'log' => json_encode($log),
                'log_type' => 'Created',
                'model_state' => $domain->toJson(),
            ]);
        });
    }

    /**
     * Register any application services.
     *
     * @return void
     */
    public function register()
    {
        $this->app->singleton(\App\Http\FortySixElks::class, function($app){
            return new \App\Http\FortySixElks();
        });
    }
}
