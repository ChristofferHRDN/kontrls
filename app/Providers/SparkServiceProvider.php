<?php

namespace App\Providers;

use Laravel\Spark\Providers\AppServiceProvider as ServiceProvider;
use Laravel\Spark\Spark;

class SparkServiceProvider extends ServiceProvider
{
	/**
	 * Your application and company details.
	 *
	 * @var array
	 */
	protected $details = [
		'vendor' => 'Your Company',
		'product' => 'Your Product',
		'street' => 'PO Box 111',
		'location' => 'Your Town, NY 12345',
		'phone' => '555-555-5555',
	];

	/**
	 * The address where customer support e-mails should be sent.
	 *
	 * @var string
	 */
	protected $sendSupportEmailsTo = "christoffer@hrdn.se";

	/**
	 * All of the application developer e-mail addresses.
	 *
	 * @var array
	 */
	protected $developers = [
		'christoffer@hrdn.se'
		//
	];

	/**
	 * Indicates if the application will expose an API.
	 *
	 * @var bool
	 */
	protected $usesApi = false;

	/**
	 * Finish configuring Spark for the application.
	 *
	 * @return void
	 */
	public function booted()
	{
		\Laravel\Cashier\Cashier::useCurrency('eur', '€');
		Spark::collectEuropeanVat('SE');

		Spark::useStripe()->noCardUpFront()->trialDays(7);

		Spark::freePlan()
			 ->features([
				 'First',
				 'Second',
				 'Third'
			 ])
			 ->attributes(['max_sites' => 5]);

		Spark::plan('Freelancer', 'freelancer-plan')
			 ->price(10)
			 ->features([
				 '10 Sites'
			 ])
			 ->attributes(['max_sites' => 10]);
		Spark::plan('Startup', 'startup-plan')
			 ->price(30)
			 ->features([
				 '50 Sites'
			 ])
			 ->attributes(['max_sites' => 50]);
		Spark::plan('Business', 'business-plan')
			 ->price(45)
			 ->features([
				 '100 Sites'
			 ])
			 ->attributes(['max_sites' => 100]);
		Spark::plan('Enterprise', 'enterprise-plan')
			 ->price(60)
			 ->features([
				 '200 Sites'
			 ])
			 ->attributes(['max_sites' => 200]);
	}
}
