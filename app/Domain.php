<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\DB;

/**
 * @property Settings settings
 * @property string url
 * @property string name
 * @property string hashid
 * @property mixed settings_id
 * @property ScheduledAudit scheduledAudit
 * @property mixed audits
 * @property mixed audit_url
 * @property bool verified
 * @property int id
 * @property int group_id
 * @property mixed uniq
 * @property mixed identifier
 */
class Domain extends Model
{
	use Traits\HasSettings;

	private static $sql_where_part = 'from jobs where payload like "%App\\\\\\\\Jobs\\\\\\\\Ping\\\\%';
	protected $hidden = ['id', 'group_id', 'user_id'];
	protected $fillable = ['url', 'created_by', 'name'];
	protected $with = ['group', 'settings', 'monitors', 'user'];

	public function logs()
	{
		return $this->morphMany('App\ModelLog', 'loggable');
	}

	public function group()
	{
		return $this->belongsTo('App\Group');
	}

	public function user()
	{
		return $this->belongsTo('App\User');
	}

	public function monitors()
	{
		return $this->hasMany('App\Monitor');
	}

	public function urls()
	{
		return $this->hasMany('App\Url');
	}

	public function alerts()
	{
		return $this->hasMany('App\Alert');
	}

	public function scheduledAudit()
	{
		return $this->hasOne('App\ScheduledAudit');
	}

	public function scheduledBackup()
	{
		return $this->hasOne('App\ScheduledBackup');
	}

	public function notes()
	{
		return $this->morphMany('App\Note', 'notable');
	}

	public function getRouteKeyName()
	{
		return 'hashid';
	}

	/**
	 * @return bool
	 */
	public function canStartBackup()
	{
		return $this->verified_backup && $this->getSetting('backup_secret') && !$this->hasBackupRunning();
	}

	/**
	 * @return bool
	 */
	public function hasBackupRunning()
	{
		if (!$this->backups()->count()) {
			return false;
		}
		if ($this->has_backup_queued) {
			return true;
		}
		return $this->backups()->latest()->first()->stopped_at === '0000-00-00 00:00:00';
	}

	/**
	 * @return \Illuminate\Database\Eloquent\Relations\HasMany
	 */
	public function backups()
	{
		return $this->hasMany('App\Backup');
	}

	public function getLatestAlert($url, $type)
	{
		$latestAlerts = json_decode($this->last_alerts, true);
		if ($latestAlerts && array_get($latestAlerts, $url->url) && array_get($latestAlerts[$url->url], $type)) {
			return $latestAlerts[$url->url][$type];
		}
		return time() - 3600 * 365 * 24;
	}

	/**
	 * @return bool
	 */
	public function hasScheduledAuditInQueue()
	{
		return DB::table('jobs')
				 ->select('*')
				 ->where('queue', '=', 'audit')
				 ->where('payload', 'like', DB::raw('"%___________marker___________\\\\\";i:' .
					 $this->id .
					 '%" ESCAPE \'≤\''))
				 ->count() === 1;
	}

	/**
	 * Returns the polltime for this domain
	 * This is done by choosing the lowest in the set (this->settings->polltime and this->group->settings->polltime)
	 *
	 * @return int
	 */
	public function getPollTimeInSeconds()
	{
		// Clamps the values between 1 and PHP_INT_MAX
		// Also chooses the lowest polltime available between this domain and group,
		// so if group has one time and this domain has a lower one, it chooses the domain polltime
		return max(
			min(
				min(
					$this->settings->getValue("polltime"),
					$this->group !== null ? $this->group->settings->getValue("polltime") :
						Settings::getGroupDefaults('polltime')
				),
				PHP_INT_MAX),
			1);
	}

	/**
	 * @return mixed
	 */
	public function getTimeBetweenAlerts()
	{
		return max(
			min(
				min($this->getSetting('time_between_alerts'),
					$this->group !== null ? $this->group->getSetting('time_between_alerts') :
						Settings::getGroupDefaults('time_between_alerts'))
				, PHP_INT_MAX
			), 1
		);
	}

	/**
	 * @param $key
	 * @param null $default
	 * @param int $min
	 * @param $max
	 *
	 * @return mixed
	 */
	public function getLowestIntFromSettingsAndGroup($key, $default = null, $min = 1, $max = PHP_INT_MAX)
	{
		return max(
			min(
				min($this->getSetting($key, $default),
					$this->group !== null ? $this->group->getSetting($key, $default) :
						Settings::getGroupDefaults($key))
				, $max
			), $min
		);
	}

	/**
	 * @return bool
	 */
	public function isQueued()
	{
		return DB::table('jobs')
		  ->select('*')
		  ->where('queue', '=', 'ping')
		  ->where('payload', 'like', DB::raw('"%___________marker___________\\\\\";i:' .
			  $this->id .
			  '%" ESCAPE \'≤\''))
		  ->count() >= 1;
	}

	/**
	 * @return mixed
	 */
	public function removeFromQueue()
	{
		return DB::table('jobs')
				 ->where('queue', '=', 'ping')
				 ->where('payload', 'like', DB::raw('"%___________marker___________\\\\\";i:' .
					 $this->id .
					 '%" ESCAPE \'≤\''))
				 ->delete();
	}

	/**
	 * @return bool
	 */
	public function isVerified()
	{
		return $this->verified == true;
	}

	/**
	 * @return bool
	 */
	public function hasAuditRunning()
	{
		return $this->audits()->where('running', true)->where('stopped', false)->count() > 0;
	}
	
	public function audits()
	{
		return $this->hasMany('App\Audit');
	}

	/**
	 * @return string
	 */
	public function getSettingsDefaultName()
	{
		return 'Domain';
	}

	/**
	 * @param $logs
	 *
	 * @return float|int
	 */
	public function calculateDowntime($logs)
	{
		if(!env('CALCULATE_DOWNTIME', true)){
			return 0;
		}
		$log_count = count($logs);
		$downtime = 0;

		if ($log_count > 0) {

			$downtime = collect($logs)->reject(function($log_entry){
					$result = $log_entry->log_type !== "Uptime";
					return $result;
				})->map(function ($log_entry) {
					return json_decode($log_entry["log"]);
				})->map(function ($elm) {
					//return array_get($elm, 'succeeded');
					return $elm->succeeded;
				})->filter(function ($elm) {
					return $elm == false;
				})->count() / count($logs);
		}

		return $downtime;
	}
}
