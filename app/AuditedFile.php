<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class AuditedFile extends Model
{
	protected $fillable = ['audit_id', 'filename', 'caught', 'weight', 'lines', 'explanations', 'stats', 'md5'];
    public function audit()
	{
		return $this->belongsTo('App\Audit');
	}
}
