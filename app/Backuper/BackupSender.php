<?php

namespace App\Backuper;

use GuzzleHttp\Client;
use GuzzleHttp\Exception\ConnectException;

class BackupException extends \Exception
{
}

class NotAnAkeebaInstallationException extends BackupException
{
}

class AkeebaException extends BackupException
{
}

// 401
class AkeebaInvalidCredentials extends AkeebaException
{
}

// 403
class AkeebaInadequatePrivileges extends AkeebaException
{
}

// 404
class AkeebaRequestedResourceNotFound extends AkeebaException
{
}

// 405
class AkeebaUnknownJSONMethod extends AkeebaException
{
}

// 500
class AkeebaServerException extends AkeebaException
{
}

// 501
class AkeebaNotImplemented extends AkeebaException
{
}

// 503
class AkeebaRemoteServiceNotActivated extends AkeebaException
{
}

class BackupSender
{
	public static function callAkeeba($url, $callData)
	{
		$client = new Client();
		$json = json_encode($callData);
		$url = rtrim($url, '/');
		$url .= "/index.php?option=com_akeeba&view=json&format=raw&json=" . $json;
		try{
			$res = $client->get($url);
		} catch(ConnectException $e){
			throw new BackupException($e->getMessage(), 0, $e);
		}


		$res = $res->getBody()->__toString();

		$res = self::removeAkeebaPadding($res, '###');

		if (!$res) {
			throw new NotAnAkeebaInstallationException('That does not look like a valid akeeba backup page');
		}

		$data = json_decode($res);
		$body = json_decode($data->body->data);

		if ($data->body->status !== 200) {
			$message = $data->body->status . ' ' . $body;
			switch ($data->body->status) {
				case 401:
					throw new AkeebaInvalidCredentials($message);
					break;
				case 403:
					throw new AkeebaInadequatePrivileges($message);
					break;
				case 404:
					throw new AkeebaRequestedResourceNotFound($message);
					break;
				case 405:
					throw new AkeebaUnknownJSONMethod($message);
					break;
				case 500:
					throw new AkeebaServerException($message);
					break;
				case 501:
					throw new AkeebaNotImplemented($message);
					break;
				case 503:
					throw new AkeebaRemoteServiceNotActivated($message);
					break;
				default:
					throw new AkeebaException($message);
			}
		}

		return $body;
	}

	protected static function removeAkeebaPadding(string $string, string $search)
	{
		$pos = strpos($string, $search);
		$string = substr($string, $pos + strlen($search));
		$pos = strpos($string, $search);
		return substr($string, 0, $pos);
	}
}