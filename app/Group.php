<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

/**
 * @property string hashid
 * @property mixed settings_id
 * @property mixed id
 * @property Domain domains
 */
class Group extends Model
{
	use Traits\HasSettings;
	protected $fillable = ['name', 'user_id'];
	protected $hidden = ['id', 'user_id'];

	public function domains()
	{
		return $this->hasMany('App\Domain');
	}

	public function logs()
	{
		return $this->morphMany('App\ModelLog', 'loggable');
	}
	public function user()
	{
		return $this->belongsTo('App\User', 'created_by');
	}

	public function notes()
	{
		return $this->morphMany('App\Note', 'notable');
	}

	public function getRouteKeyName()
	{
		return 'hashid';
	}
	public function getSettingsDefaultName()
	{
		return 'Group';
	}
}
