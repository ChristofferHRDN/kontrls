<?php

namespace App\Events;

use App\Audit;
use App\Events\Event;
use Illuminate\Queue\SerializesModels;
use Illuminate\Contracts\Broadcasting\ShouldBroadcast;

class AuditFinished extends Event
{
    use SerializesModels;
    
    public $audit;

    /**
     * Create a new event instance.
     *
     * @param Audit $audit
     */
    public function __construct(Audit $audit)
    {
        $this->audit = $audit;
    }

    /**
     * Get the channels the event should be broadcast on.
     *
     * @return array
     */
    public function broadcastOn()
    {
        return [];
    }
}
