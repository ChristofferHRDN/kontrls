<?php

namespace App;

use App\Repositories\AuditRepository;
use Carbon\Carbon;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\DB;

/**
 * @property static last_audit
 * @property static next_audit
 * @property mixed wait_time
 */
class ScheduledAudit extends Model
{
	protected $guarded = [];

	public function domain()
	{
		return $this->belongsTo('App\Domain');
	}

	public function isQueued()
	{
		return DB::table('jobs')
				 ->select('*')
				 ->where('queue', '=', 'audit')
				 ->where('payload', 'like', DB::raw('"%___________marker___________\\\\\";i:' .
					 $this->domain->id .
					 '%" ESCAPE \'≤\''))
				 ->count() === 1;
	}

	public function removeFromQueueAndDisable()
	{
		$this->enabled = 0;
		return $this->removeFromQueue();
	}

	public function removeFromQueue()
	{
		$id = DB::table('jobs')
				->select('id')
				->where('queue', '=', 'audit')
				->where('payload', 'like', DB::raw('"%___________marker___________\\\\\";i:' .
					$this->domain->id .
					'%" ESCAPE \'≤\''))
				->get();
		if (count($id)) {
			$id = $id[0]->id;
			DB::table('jobs')->where('id', '=', $id)->delete();
			return true;
		}
		return false;
	}

	public function updateScheduledAuditTime(AuditRepository $auditRepository, int $domainId, int $userId, int $autoWaitTime, bool $setViaGroup = false)
	{
		if ($autoWaitTime !== -1) {
			$waitTimeBefore = $this->wait_time;
			$this->wait_time = (int)$autoWaitTime;
			$this->enabled = 1;

			if ((int)$autoWaitTime !== $waitTimeBefore || !$this->isQueued()) {
				$this->removeFromQueue();

				$audit = $auditRepository->create(['domain_id' => $domainId, 'user_id' => $userId, 'initiated_by' => 'scheduling']);
				$scheduledAuditJob = new \App\Jobs\Audit\ScheduledAudit($audit, $this);

				$newTime = Carbon::parse($this->last_audit)->addSeconds($autoWaitTime);

				if ($newTime->lt(Carbon::now())) {
					$waitTime = 0;
				} else {
					$waitTime = $newTime->diffInSeconds(Carbon::now());
				}
				dispatch($scheduledAuditJob->onQueue('audit')->delay($waitTime));
			}
		} else {
			$this->removeFromQueueAndDisable();
		}
		$this->set_via_group = $setViaGroup;
		$this->save();
	}

	public function updateScheduledAuditTimeFromGroup(AuditRepository $auditRepository, int $domainId, int $userId, int $autoWaitTime)
	{
		if($this->set_via_group){
			$this->updateScheduledAuditTime($auditRepository, $domainId, $userId, $autoWaitTime, true);
		} else if($autoWaitTime < $this->wait_time || $this->wait_time === 0 || $this->enabled === 0){
			$this->updateScheduledAuditTime($auditRepository, $domainId, $userId, $autoWaitTime, true);
		}
	}
}
