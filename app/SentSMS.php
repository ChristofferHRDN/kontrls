<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class SentSMS extends Model
{
    protected $table = 'sent_sms';
    protected $fillable = ['from', 'message', 'number', 'response'];
}
