<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Url extends Model
{
	protected $fillable = ['url', 'domain_id'];

    public function monitors()
	{
		return $this->hasMany('App\Monitor');
	}

	public function notes()
	{
		return $this->morphMany('App\Note', 'notable');
	}

	public function domain()
	{
		return $this->belongsTo('App\Domain');
	}
}
