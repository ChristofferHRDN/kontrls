<?php

namespace App\Alerter;

use App\Http\FortySixElks;
use Illuminate\Support\Facades\App;
use Illuminate\Support\Facades\Mail;

class AlertSender
{
	public static function getMethodName($alert_method)
	{
		$methods = [
			'E-Mail' => 'email',
			'SMS' => 'sms'
		];

		return $methods[$alert_method];
	}
	public static function email($alert, $type, $where)
	{
		Mail::send(['email.alert.' . $type,
			'email.alert.' . $type . '-plain'], ['data' => $alert->message,
			'alert' => $alert], function ($message) use ($alert, $where) {
			$message->subject('Alert from: ' . $alert->domain->name);
			$message->from('alert@kntrl.io', 'Alert from Kontrl.io');
			$message->to($where);
		});
	}

	public static function sms($alert, $type, $where)
	{
		$elk = App::make(FortySixElks::class);
		$elk->sendSMS($alert->message . ' At your domain: ' . $alert->domain->name);
	}
}