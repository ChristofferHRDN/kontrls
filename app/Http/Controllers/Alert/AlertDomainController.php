<?php

namespace App\Http\Controllers\Alert;

use App\Http\Controllers\Controller;
use App\Alert;
use App\Domain;
use Illuminate\Http\Request;

use App\Http\Requests;

class AlertDomainController extends Controller
{
	public function delete(Request $request, Domain $domain, Alert $alert)
	{
		$alert->delete();

		return redirect()->route('domain.alerts.list', [$domain, 'page' => $request->query->get('page')]);
	}

	public function list(Request $request, Domain $domain)
	{
		return view('alerts.list', ['domain' => $domain,
			'alerts' => $domain->alerts()->paginate(100),
			'page' => $request->query->get('page')]);
	}

	public function clearAll(Request $request, Domain $domain)
	{
		$domain->alerts()->delete();

		return redirect()->route('domain.alerts.list', $domain);
	}

	public function readAll(Request $request, Domain $domain)
	{
		$domain->alerts()->update(['read' => true]);

		return redirect()->route('domain.alerts.list', $domain)->with('status', 'All alerts were marked read!');
	}
}
