<?php

namespace App\Http\Controllers\Alert;

use App\Alert;
use App\Http\Controllers\Controller;
use Carbon\Carbon;
use Illuminate\Http\Request;

use App\Http\Requests;

class AlertWithoutDomainController extends Controller
{
	public function listWithoutDomain(Request $request)
	{
		$alerts = $request->user()->alerts()->select('alerts.*')->paginate(100);
		return view('alerts.list-without-domain', ['alerts' => $alerts, 'page' => $request->query->get('page')]);
	}

	public function clearAllWithoutDomain(Request $request)
	{
		$request->user()->alerts()->delete();

		return redirect()->route('alerts.list');
	}

	public function readAllWithoutDomain(Request $request)
	{
		$request->user()->alerts()->getQuery()->getQuery()->update(['alerts.read' => true,
			'alerts.updated_at' => Carbon::now()]);

		return redirect()->route('alerts.list')->with('status', 'All alerts were marked read!');
	}
	public function delete(Request $request, Alert $alert)
	{
		$alert->delete();

		return redirect()->route('alerts.list', ['page' => $request->query->get('page')]);
	}
}
