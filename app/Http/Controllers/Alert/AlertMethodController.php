<?php

namespace App\Http\Controllers\Alert;

use App\Http\Controllers\Controller;
use App\AlertMethod;
use Illuminate\Http\Request;

use App\Http\Requests;

class AlertMethodController extends Controller
{
	public function delete(Request $request, AlertMethod $alert_method)
	{
		$alert_method->delete();
		return redirect('/settings#/alerts');
	}
	
	public function store(Request $request)
	{
		if ($request->types === "") {
			$types = [];
		} else {
			$types = $request->types;
		}
		$alert_domains = $request->alert_domains ?? "all";

		$alert_domains = json_encode($alert_domains);
		$types = json_encode($types);

		$request->user()->alert_methods()->create([
			'method' => $request->request->get('method'),
			'types' => $types,
			'alert_domains' => $alert_domains,
			'alert_way' => $request->alert_way
		]);

		return redirect('/settings#/alerts');
	}
}
