<?php

namespace App\Http\Controllers\Alert;

use App\Alert;
use App\Http\Controllers\Controller;
use App\Http\Requests;
use Illuminate\Http\Request;

class AlertController extends Controller
{
	public function show(Request $request, Alert $alert)
	{
		$alert->read = 1;
		$alert->save();
		return view('alerts.show', [
			'alert' => $alert,
			'domain' => $alert->domain
		]);
	}

	public function showByReadId(Request $request, $readId)
	{
		$alert = Alert::where('read_id', $readId)->first();
		if ($alert->read === 0) {
			$alert->read = 1;
			$alert->save();

			return view('alerts.show', [
				'alert' => $alert
			]);
		}
		return view('alerts.already-displayed');
	}
}
