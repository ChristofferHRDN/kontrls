<?php

namespace App\Http\Controllers;

use App\Http\Requests;
use App\VulnerabilityScanners\Scanners\GeneralScanner;
use Illuminate\Http\Request;

class ScanFilesController extends Controller
{
	public function scan(Request $request)
	{
		set_time_limit(0);
		$entry = $request->query->get('file');
		$scanner = GeneralScanner::detectScanType($entry);
		$files = $scanner->scan()["files"];
		$arr = array_map(function($file){
			return mb_strtolower($file["file"]);
		},$files);
		sort($arr);
		foreach($arr as $file){
			//var_dump($file);
		}
		dd($files);
	}
}
