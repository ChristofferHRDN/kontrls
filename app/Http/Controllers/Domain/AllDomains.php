<?php

namespace App\Http\Controllers\Domain;

use App\Domain;
use App\Http\Controllers\Controller;
use Illuminate\Http\Request;

class AllDomains extends Controller
{
	/**
	 * @param Request $request
	 *
	 * @return array
	 */
	public function index(Request $request)
	{
		if($request->user()->cannot('show-all-domains')){
			abort(403);
		}

		$domains = Domain::paginate(500);

		return ['domains' => $domains];
	}
}