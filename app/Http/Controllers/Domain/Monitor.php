<?php

namespace App\Http\Controllers\Domain;

use App\CheckType;
use App\Domain;
use App\Http\Requests;
use App\Monitor as MonitorModel;
use App\MonitorCheck;
use Illuminate\Http\Request;

use App\Http\Controllers\Controller;

class Monitor extends Controller
{
	public function store(Request $request, \App\Domain $domain)
	{
		$this->authorize('addMonitor', $domain);

		$this->validate($request, [
			'url' => 'required|max:255',
			'check' => 'required|max:255',
		]);

		$check_type = CheckType::firstOrCreate(['id' => $request->check]);

		$monitor = $domain->monitors()->create([
			'url_id' => $request->url,
			'check_type_id' => $check_type->id,
			'check' => json_encode([
				'expected' => $request->expected_value,
				'check_value' => $request->check_value,
				'should_fail' => $request->is_negative_check
			])
		]);

		return redirect()->route('domain.show', $domain);
	}

	public function destroy(Request $request, \App\Domain $domain, MonitorModel $monitor)
	{
		$this->authorize('destroyMonitor', [$domain, $monitor]);

		$monitor->delete();

		if($request->wantsJson()){
			return ['status' => 'Success'];
		}

		return redirect()->route('domain.show', $domain);
	}

	public function edit(Request $request, \App\Domain $domain, MonitorModel $monitor)
	{
		$check_type = CheckType::firstOrCreate(['id' => $request->check]);
		$monitor->fill(['url' => $request->url,
			'check_type_id' => $check_type->id,
			'check' => json_encode([
				'expected' => $request->expected_value,
				'check_value' => $request->check_value,
				'should_fail' => $request->is_negative_check == 'on' ? 1 : 0
			])]);

		$monitor->save();

		if ($request->wantsJson()) {
			return ['monitor' => $monitor];
		}

		return redirect()->route('domain.show', $domain);
	}

	public function toggle(Request $request, \App\Domain $domain, MonitorModel $monitor)
	{
		$this->authorize('toggleMonitor', [$domain, $monitor]);

		$monitor->enabled = !$monitor->enabled;
		$monitor->save();

		if ($request->wantsJson()) {
			return ["enabled" => $monitor->enabled];
		}

		return redirect()->route('domain.show', $domain);
	}
}