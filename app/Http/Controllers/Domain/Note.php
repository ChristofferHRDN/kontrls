<?php

namespace app\Http\Controllers\Domain;

use App\Domain;
use App\Http\Controllers\Controller;
use App\Note as NoteModel;
use Illuminate\Http\Request;

class Note extends Controller
{
	/**
	 * @param Request $request
	 * @param Domain $domain
	 *
	 * @return View
	 */
	public function listNotes(Request $request, \App\Domain $domain)
	{
		return view('domains.domain.notes', [
			'domain' => $domain,
			'note' => new NoteModel()
		]);
	}

	/**
	 * @param Request $request
	 * @param Domain $domain
	 *
	 * @return \Illuminate\Http\RedirectResponse
	 */
	public function addNote(Request $request, \App\Domain $domain)
	{
		$note = NoteModel::find($request->noteid);
		if ($note == null) {
			$domain->notes()->create([
				'note' => $request->note,
				'user_id' => $request->user()->id
			]);
		} else {
			$note->note = $request->note;
			$note->save();
		}
		return redirect()->route('domain.notes', $domain);
	}

	/**
	 * @param Request $request
	 * @param Domain $domain
	 * @param Note $note
	 *
	 * @return \Illuminate\Http\RedirectResponse
	 * @throws \Exception
	 */
	public function removeNote(Request $request, \App\Domain $domain, NoteModel $note)
	{
		$note->delete();

		return redirect()->route('domain.notes', $domain);
	}

	/**
	 * @param Request $request
	 * @param Domain $domain
	 * @param Note $note
	 *
	 * @return View
	 */
	public function editNote(Request $request, \App\Domain $domain, NoteModel $note)
	{
		return view('domains.domain.notes', [
			'domain' => $domain,
			'note' => $note
		]);
	}
}