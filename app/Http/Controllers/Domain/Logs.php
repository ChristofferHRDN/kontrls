<?php

namespace App\Http\Controllers\Domain;

use App\Domain;
use App\Http\Controllers\Controller;
use Illuminate\Http\Request;

class Logs extends Controller
{
	/**
	 * Either returns the logs of the given domain as an array to be converted to json,
	 * or it renders the domains.domain.logs view with the necessary data
	 *
	 * @param Request $request
	 * @param Domain $domain
	 *
	 * @return array|View
	 */
	public function index(Request $request, \App\Domain $domain)
	{
		$this->authorize('viewLogs', $domain);

		$limit = $request->input('limit', 50);
		$logs = $domain->logs()
					   ->where('log_type', '!=', 'Update')
					   ->orderBy('created_at', 'desc')
					   ->take($limit)
					   ->get();

		if ($request->wantsJson()) {
			return ['logs' => $logs];
		}

		$uptimeLogs = $domain->logs()->where('log_type', '=', 'Uptime')->orderBy('created_at', 'desc')->take(100)->get();

		$downtime = $domain->calculateDowntime($uptimeLogs);

		return view('domains.domain.logs', [
			'logs' => $logs,
			'domain' => $domain,
			'downtime' => $downtime
		]);
	}
}