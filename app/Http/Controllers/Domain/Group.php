<?php

namespace App\Http\Controllers\Domain;

use App\Domain;
use App\Http\Controllers\Controller;
use Illuminate\Http\Request;

class Group extends Controller
{
	/**
	 * @param Request $request
	 * @param Domain $domain
	 *
	 * @return array|\Illuminate\Http\RedirectResponse
	 */
	public function store(Request $request, \App\Domain $domain)
	{
		$this->authorize('addGroup', $domain);
		$group = \App\Group::where('hashid', '=', $request->group_id)->first();

		$domain->group_id = $group->id;
		$domain->save();

		if ($request->wantsJson()) {
			return ['group' => $group];
		}

		return redirect()->route('domain.show', $domain);
	}

	/**
	 * @param Request $request
	 * @param Domain $domain
	 *
	 * @return \Illuminate\Http\RedirectResponse|string
	 */
	public function delete(Request $request, \App\Domain $domain)
	{
		$this->authorize('deleteGroup', $domain);
		
		$domain->group()->dissociate();
		$domain->save();
		if ($request->wantsJson()) {
			return "200 OK";
		}

		return redirect()->route('domain.show', $domain);
	}
}