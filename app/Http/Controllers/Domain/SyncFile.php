<?php
/**
 * Created by PhpStorm.
 * User: christoffer
 * Date: 31/05/16
 * Time: 13:46
 */

namespace App\Http\Controllers\Domain;

use App\Domain;
use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Storage;

class SyncFile extends Controller
{
	/**
	 * Sends the /storage/app/public/sync/file.txt file, with $domain->uniq substituted
	 * This file is for putting in your public directory
	 *
	 * @param Request $request
	 * @param Domain $domain
	 *
	 * @return mixed
	 */
	public function create(Request $request, \App\Domain $domain)
	{
		$content = preg_replace('/\[\[\[\[uniq\]\]\]\]/', $domain->uniq, Storage::disk('sync')->get('file.txt'));
		$content = preg_replace('/\[\[\[\[secret\]\]\]\]/', env('AUDITER_SECRET'), $content);
		$content = preg_replace('/\[\[\[\[identifier\]\]\]\]/', $domain->identifier, $content);
		$filename = env('SYNCFILE_NAME', 'kntrliosync.php');
		$headers = ['Content-type' => 'text/plain',
			'Content-Length' => strlen($content),
		];
		if ($request->query->get('download') === '1') {
			$headers['Content-Disposition'] = sprintf('attachment; filename="%s"', $filename);
		}

		return \Response::make($content, 200, $headers);
	}
}