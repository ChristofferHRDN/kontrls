<?php

namespace app\Http\Controllers\Domain;

use App\Http\Controllers\Controller;
use App\Repositories\DomainRepository;
use Illuminate\Http\Request;

class Domains extends Controller
{
	protected $domains;

	public function __construct(DomainRepository $domains)
	{
		$this->domains = $domains;
	}

	/**
	 * List the domains
	 * Only lists the domains that a given user has created
	 *
	 * @param Request $request
	 *
	 * @return mixed
	 */
	public function index(Request $request)
	{
		// This uses the App\Repositories/DomainRepository\forUser method
		// Since it might be used later sometime
		// Also nice to separate DB queries from controller code
		$domains = $this->domains->forUser($request->user());

		if ($request->wantsJson()) {
			return ['domains' => $domains];
		}
		return view('domains.list', [
			'domains' => $domains
		]);
	}
}