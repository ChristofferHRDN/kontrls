<?php

namespace App\Http\Controllers\Domain;

use App\Domain;
use App\Http\Controllers\Controller;
use App\Http\Requests;
use App\Repositories\AuditRepository;
use App\Repositories\DomainRepository;
use Illuminate\Http\Request;

class Settings extends Controller
{
	protected $domains;
	protected $auditRepo;

	public function __construct(DomainRepository $domains, AuditRepository $auditRepository)
	{
		$this->middleware('auth');

		$this->domains = $domains;
		$this->auditRepo = $auditRepository;
	}

	/**
	 * Update related settings object
	 *
	 * @param Request $request
	 * @param Domain $domain
	 *
	 * @return mixed
	 */
	public function update(Request $request, \App\Domain $domain)
	{
		$this->authorize('update', $domain);

		$polltime = max($request->polltime, 1);
		$time_between_alerts = max($request->time_between_alerts, 1);
		$automatic_audit_wait_time =
			in_array($request->automatic_audit_wait_time, ['-1', '3600', '86400', '604800', '2592000']) ?
				(int)$request->automatic_audit_wait_time : -1;

		$automatic_backup_wait_time =
			in_array($request->automatic_backup_wait_time, ['-1', '3600', '86400', '604800', '2592000']) ?
				(int)$request->automatic_backup_wait_time : -1;

		if ($domain->scheduledAudit !== null) {
			$sa = $domain->scheduledAudit;
		} else {
			$sa = $domain->scheduledAudit()->create([]);
		}
		if ($domain->scheduledBackup !== null) {
			$sb = $domain->scheduledBackup;
		} else {
			$sb = $domain->scheduledBackup()->create([]);
		}

		$array = ['polltime' => $polltime,
			'time_between_alerts' => $time_between_alerts];

		$array['automatic_audit_wait_time'] = (int)$automatic_audit_wait_time;
		$array['automatic_backup_wait_time'] = (int)$automatic_backup_wait_time;

		$sa->updateScheduledAuditTime($this->auditRepo, $domain->id, $request->user()->id, $automatic_audit_wait_time);
		$sb->updateScheduledBackupTime($domain->id, $request->user()->id, $automatic_backup_wait_time);

		$backup_secret = $domain->getSetting('backup_setting');

		if ($backup_secret !== $request->backup_secret) {
			$domain->verified_backup = 0;
			$domain->save();
			$domain->logs()->create([
				'log' => json_encode(['message' => 'You recently changed your backup secret, and as such need to verify your domain again before doing another backup.']),
				'log_type' => 'Backup',
				'status' => 'failed'
			]);
		}

		$domain->setSettings(true, $array, $request->only(['useragent',
			'backup_secret',
			'isjoomlasite',
			'minimum_audit_alert_weight']));

		return redirect()->route('domain.settings', $domain);
	}

	/**
	 * @param Request $request
	 * @param Domain $domain
	 *
	 * @return View
	 */
	public function index(Request $request, \App\Domain $domain)
	{
		return view('domains.domain.settings', ['domain' => $domain]);
	}
}