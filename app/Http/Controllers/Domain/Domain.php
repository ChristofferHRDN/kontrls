<?php

namespace App\Http\Controllers\Domain;

use App\CheckType;
use App\Domain as DomainModel;
use App\Http\Controllers\Controller;
use App\Repositories\AuditRepository;
use App\Repositories\DomainRepository;
use Illuminate\Http\Request;

class Domain extends Controller
{
	protected $domains;
	protected $auditRepo;

	public function __construct(DomainRepository $domains, AuditRepository $auditRepository)
	{
		$this->middleware('auth');

		$this->domains = $domains;
		$this->auditRepo = $auditRepository;
	}

	/**
	 * Display the domain
	 *
	 * @param Request $request
	 * @param Domain $domain
	 *
	 * @return mixed
	 */
	public function show(Request $request, DomainModel $domain)
	{
		$this->authorize('show', $domain);

		$logs = $this->domains->getUptime($domain, null, false);
		$log_count = count($logs);

		$check_types = CheckType::pluck('name', 'id');

		$urls = \App\Url::leftJoin('domains', 'urls.domain_id', '=', 'domains.id')
						->where('domains.user_id', $request->user()->id)
						->where('urls.domain_id', $domain->id)
						->pluck('urls.url', 'urls.id');

		$groups = \App\Group::where('user_id', $request->user()->id)->lists('name', 'hashid');

		if ($request->wantsJson()) {
			return [
				'domain' => $domain,
				'check_types' => $check_types,
				'groups' => $groups->toArray(),
				'urls' => $urls,
				'queued' => $domain->isQueued(),
			];
		}

		return view('domains.domain', [
			'domain' => $domain,
			'check_types' => $check_types,
			'groups' => $groups,
			'urls' => $urls
		]);
	}

	/**
	 * @param $log
	 *
	 * @return array
	 */
	protected function calculateDataFromLogs($logs)
	{
		return $logs->map(function($elm){
				$decoded = json_decode($elm["log"]);
				if ($decoded->succeeded === false ||
					property_exists($decoded, "check") &&
					$decoded->check->result === false
				) {
					return 0;
				}
				return $decoded->stats->transfertime * 1000;
		})->reverse();
	}

	/**
	 * @param $log
	 *
	 * @return array
	 */
	protected function calculateDateFromLogs($logs)
	{
		return $logs->map(function($elm){
			return $elm->created_at;
		})->reverse();
	}

	/**
	 * @param Domain $domain
	 *
	 * @return array
	 */
	public function getUptimeGraphData(DomainModel $domain)
	{
		$logs = $this->domains->getUptime($domain);

		$data = $this->calculateDataFromLogs($logs)->reverse()->values();
		$dates = $this->calculateDateFromLogs($logs)->map(function($date){
			return $date->toDateTimeString();
		})->values();

		return [
			'data' => $data,
			'dates' => $dates
		];
	}

	/**
	 * Create a new domain
	 *
	 * @param Request $request
	 *
	 * @return mixed
	 */
	public function store(Request $request)
	{
		$request->user()->can('create-domain');

		$this->validate($request, [
			'url' => 'required|url|unique:domains|max:255',
		]);
		$domain = $request->user()->domains()->create([
			'url' => $request->url,
			'name' => $request->name,
		]);

        if($request->wantsJson()){
            return ['domain' => $domain, 'route' => route('domain.show', $domain)];
        }

		return redirect()->route('domain.show', $domain);
	}

	/**
	 * Update the domain
	 *
	 * @param Request $request
	 * @param Domain $domain
	 *
	 * @return mixed
	 */
	public function update(Request $request, DomainModel $domain)
	{
		$this->authorize('update', $domain);

		$this->validate($request, [
			'audit_url' => 'unique:domains,audit_url,' . $domain->id
		], [
			'unique' => 'The audit url has already been taken by another domain. ' .
				'If you have added it to some other domain, then please go and change it first. ' .
				'Otherwise you might try to renaming the file to something else.'
		]);
		$url = $domain->url;

		if ($url !== $request->url) {
			$domain->verified_backup = 0;
			$domain->logs()->create([
				'log' => json_encode(['message' => 'You recently changed your url, and as such need to verify your domain again before doing another backup.']),
				'log_type' => 'Backup',
				'status' => 'failed'
			]);
		}

		$domain->url = $request->url;
		$domain->name = $request->name;

		if ($domain->audit_url !== $request->audit_url) {
			$domain->audit_url = $request->audit_url;
			$domain->verified = false;
			$domain->logs()->create([
				'log' => json_encode(['message' => 'You recently changed your audit url, and as such need to verify your domain again before you can do an audit.']),
				'log_type' => 'Audit',
				'status' => 'failed'
			]);
		}

		$domain->save();

		return redirect()->route('domain.settings', $domain);
	}

	/**
	 * Destroy the domain
	 *
	 * @param Request $request
	 * @param Domain $domain
	 *
	 * @return mixed
	 * @throws \Exception
	 */
	public function delete(Request $request, DomainModel $domain)
	{
		$this->authorize('destroy', $domain);

		$domain->delete();

		return redirect()->route('domains.list');
	}
}