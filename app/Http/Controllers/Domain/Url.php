<?php

namespace App\Http\Controllers\Domain;

use App\Url as UrlModel;
use App\Domain;
use App\Http\Controllers\Controller;
use App\Http\Requests;
use Illuminate\Http\Request;

class Url extends Controller
{
	public function store(Request $request, \App\Domain $domain)
	{
		$domain->urls()->create(['url' => $request->url, 'domain_id' => $domain->id]);

		return redirect()->route('domain.show', $domain);
	}

	public function destroy(Request $request, \App\Domain $domain, UrlModel $url)
	{
		$url->delete();

		return redirect()->route('domain.show', $domain);
	}

	public function update(Request $request, \App\Domain $domain, UrlModel $url)
	{
		$url->url = $request->url;
		$url->save();

		return redirect()->route('domain.show', $domain);
	}
}
