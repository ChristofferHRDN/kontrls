<?php

namespace App\Http\Controllers;

use App\Backuper\AkeebaException;
use App\Backuper\AkeebaInvalidCredentials;
use App\Backuper\AkeebaRemoteServiceNotActivated;
use App\Backuper\AkeebaServerException;
use App\Backuper\NotAnAkeebaInstallationException;
use App\Domain;
use App\Http\Requests;
use GuzzleHttp\Exception\ConnectException;
use Illuminate\Http\Request;

class BackupVerify extends Controller
{
	public function create(Request $request, Domain $domain)
	{
		$errorMessage = '';
		$exception = null;
		try {
			$res = \App\Backup::getVersion($domain->url, $domain->getSetting('backup_secret'));
		} catch (ConnectException $e) {
			$exception = $e;
			$errorMessage = 'Could not connect to ' . $domain->url . '. Are you sure the url is correct?';
		} catch (NotAnAkeebaInstallationException $e) {
			$exception = $e;
			$errorMessage = 'There does not seem to be a valid Akeeba installation at ' . $domain->url . '. Are you sure the url is correct?';
		} catch(AkeebaRemoteServiceNotActivated $e) {
			$exception = $e;
			$errorMessage = 'Akeeba Remove Service is not activated. Please activate it before trying again.';
		} catch (AkeebaInvalidCredentials $e) {
			$exception = $e;
			$errorMessage = 'The configured Akeeba secret does not seem to be correct';
		} catch(AkeebaServerException $e) {
			$exception = $e;
			$errorMessage = 'We got an exception from the server: ' . $e->getMessage();
		} catch (AkeebaException $e) {
			$exception = $e;
			$errorMessage = $e->getMessage();
		}
		if($errorMessage){
			$domain->logs()->create([
				'log' => json_encode(['message' => 'The domain failed the backup verification with the message "' . $errorMessage . '".']),
				'log_type' => 'Backup',
				'status' => 'failed',
				'model_state' => json_encode($exception)
			]);
			dd($errorMessage);
		}
		$domain->verified_backup = 1;
		$domain->save();
		$domain->logs()->create([
			'log' => json_encode(['message' => 'Domain was verified as a valid backup location.']),
			'log_type' => 'Backup',
			'status' => 'succeeded'
		]);
		return redirect()->route('domain.backup.list', $domain)->with('status', 'Your site has been verified and you can start doing backups now.');
	}
}
