<?php

namespace App\Http\Controllers;

use App\Repositories\SMSRepository;
use GuzzleHttp\Client;
use Illuminate\Http\Request;

class SendSMSController extends Controller
{
	public function create(Request $request)
	{
		list($sms, $response) = SMSRepository::sendSMS($request->query->get('number'), $request->query->get('message'));

		dd($response->getBody()->__toString());
	}
}
