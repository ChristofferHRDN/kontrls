<?php

namespace App\Http\Controllers;

use App\Domain;
use App\Jobs\Backup\BackupStart;
use Illuminate\Http\Request;

use App\Http\Requests;

class BackupController extends Controller
{
	/**
	 * @param Request $request
	 * @param Domain $domain
	 *
	 * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
	 */
	public function index(Request $request, Domain $domain)
	{
		$logs = $domain->logs()->where('log_type', 'Backup')->where('status', 'failed')->where('read', 0)->get();
		$logs->each(function($item, $key){
			$item->read = 1;
			$item->save();
		});
		return view('backups.list', ['domain' => $domain, 'backups' => $domain->backups, 'logs' => $logs]);
	}

	/**
	 * @param Request $request
	 * @param Domain $domain
	 *
	 * @return \Illuminate\Http\RedirectResponse
	 */
	public function create(Request $request, Domain $domain)
	{
		if(!$domain->hasBackupRunning()){
			$job = new BackupStart($domain);
			dispatch($job->onQueue('backup'));

			$domain->has_backup_queued = true;
			$domain->save();

			return redirect()->route('domain.backup.list', $domain)->with('status', 'Backup queued.');
		}
		return redirect()->route('domain.backup.list', $domain)->with('status', 'This domain already has a backup queued.');
	}
}
