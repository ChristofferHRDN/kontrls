<?php

namespace App\Http\Controllers;

use App\Domain;
use App\Group;
use App\Http\Requests;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

class DashboardController extends Controller
{
	public function index(Request $request)
	{
		if (!Auth::check()) {
			return redirect('/login')->with('status', 'You must login first!');
		}
		$user = Auth::user();

		$domains = Domain::where('user_id', $user->id)->get();
		$groups = Group::where('user_id', $user->id)->get();

		return view('dashboard.index', [
			'domains' => $domains,
			'groups' => $groups
		]);
	}

	public function home(Request $request)
	{
		if (Auth::check()) {
			return redirect('/home');
		}
		return view('dashboard.home');
	}
}
