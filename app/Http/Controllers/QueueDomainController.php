<?php

namespace App\Http\Controllers;

use App\Domain;
use App\Jobs\Ping;
use Illuminate\Http\Request;

use App\Http\Requests;

class QueueDomainController extends Controller
{
	/**
	 * Queue this domain for uptime checking
	 * This method prevents further queueing of a domain by doing dirtychecking against the database
	 * Inefficient, but it works
	 *
	 * @param Request $request
	 * @param Domain $domain
	 *
	 * @return mixed
	 */
	public function queue(Request $request, Domain $domain)
	{
		if (!$domain->isQueued() && count($domain->monitors)) {
			$this->dispatch((new Ping($domain))->onQueue('ping'));
		}

		if ($request->wantsJson()) {
			return $this->isQueued($request, $domain);
		}

		return redirect()->route('domain.show', $domain);
	}

	/**
	 * @param Request $request
	 * @param Domain $domain
	 *
	 * @return array
	 */
	public function isQueued(Request $request, Domain $domain)
	{
		return ["is_queued" => $domain->isQueued()];
	}

	/**
	 * @param Request $request
	 * @param Domain $domain
	 *
	 * @return array|\Illuminate\Http\RedirectResponse
	 */
	public function removeFromQueue(Request $request, Domain $domain)
	{
		$domain->removeFromQueue();

		if ($request->wantsJson()) {
			return $this->isQueued($request, $domain);
		}

		return redirect()->route('domain.show', $domain);
	}
}
