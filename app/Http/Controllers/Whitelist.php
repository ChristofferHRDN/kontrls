<?php

namespace App\Http\Controllers;

use App\Http\Requests;
use App\UserAllowedHashes;
use Illuminate\Http\Request;

class Whitelist extends Controller
{
	public function index(Request $request)
	{
		$md5s = $request->user()->allowed_hashes;

		return view('whitelist.index', ['md5s' => $md5s]);
	}
	public function store(Request $request)
	{
		$user = $request->user();
		$md5 = $user->allowed_hashes()->firstOrCreate($request->only(['md5']));

		if($request->next){
			return redirect($request->next)->with('status', 'The MD5 was added.');
		}

		return redirect()->route('whitelist.index')->with('status', 'The MD5 was added.');
	}
	public function delete(Request $request, UserAllowedHashes $md5)
	{
		$md5->delete();

		return redirect()->route('whitelist.index')->with('status', 'The MD5 was removed.');
	}
}
