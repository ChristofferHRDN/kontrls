<?php

namespace App\Http\Controllers;

use App\Domain;
use App\Group;
use App\Http\Requests;
use App\Note;
use App\Repositories\AuditRepository;
use Illuminate\Http\Request;

class GroupController extends Controller
{
	public function destroy(Request $request, Group $group)
	{
		$group->delete();

		return redirect(route('groups.list'));
	}

	public function listGroups(Request $request)
	{
		$groups = Group::where('user_id', '=', $request->user()->id)->get();
		if ($request->wantsJson()) {
			return ['groups' => $groups];
		}
		return view('groups.list', [
			'groups' => $groups
		]);
	}

	public function show(Request $request, Group $group)
	{
		$available_domains =
			Domain::where('group_id', null)->where('user_id', $request->user()->id)->lists('name', 'id');

		return view('groups.group', [
			'avail_domains' => $available_domains,
			'group' => $group
		]);
	}

	public function store(Request $request)
	{
		$this->validate($request, [
			'name' => 'required|max:255',
		]);

		$group = Group::create(['name' => $request->name, 'user_id' => $request->user()->id]);
		$group->save();

		return redirect()->route('group.show', $group);
	}

	public function update(Request $request, Group $group)
	{
		$this->authorize('update', $group);

		$group->name = $request->name;

		$group->save();

		return redirect()->route('group.show', $group);
	}

	public function update_settings(Request $request, Group $group)
	{
		$this->authorize('update', $group);

		$polltime = max($request->polltime, 1);
		$time_between_alerts = max($request->time_between_alerts, 1);

		$automatic_audit_wait_time =
			in_array($request->automatic_audit_wait_time, ['-1', '3600', '86400', '604800', '2592000']) ?
				(int)$request->automatic_audit_wait_time : -1;

		foreach ($group->domains as $domain) {
			if ($domain->scheduledAudit !== null) {
				$sa = $domain->scheduledAudit;
			} else {
				$sa = $domain->scheduledAudit()->create([]);
			}
			$sa->updateScheduledAuditTimeFromGroup(new AuditRepository(), $domain->id, $request->user()->id, $automatic_audit_wait_time);
		}

		$group->setSettings(true, ['polltime' => $polltime,
			'time_between_alerts' => $time_between_alerts, 'automatic_audit_wait_time' => (int)$automatic_audit_wait_time]);

		return redirect()->route('group.show', $group);
	}

	public function add_domain(Request $request, Group $group)
	{
		$domain = Domain::findOrFail($request->domain_id);

		$this->authorize('add_domain', $group);

		$group->domains()->save($domain);

		return redirect()->route('group.show', $group);
	}

	public function remove_domain(Request $request, Group $group, Domain $domain)
	{
		$this->authorize('remove_domain', $group);

		$domain->group()->dissociate();
		$domain->save();

		return redirect()->route('group.show', $group);
	}

	public function add_note(Request $request, Group $group)
	{
		$group->notes()->create([
			'note' => $request->note,
			'user_id' => $request->user()->id
		]);
		return redirect()->route('group.show', $group);
	}

	public function remove_note(Request $request, Group $group, Note $note)
	{
		$note->delete();

		return redirect()->route('group.show', $group);
	}
}
