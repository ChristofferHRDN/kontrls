<?php

namespace App\Http\Controllers\Audit;

use App\Audit;
use App\Http\Controllers\Controller;
use Illuminate\Http\Request;

use App\Http\Requests;

class AllAudits extends Controller
{
	/**
	 * @param Request $request
	 *
	 * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
	 */
	public function index(Request $request)
	{
		$audits = Audit::whereHas('domain', function ($query) use ($request) {
			$query->where('user_id', $request->user()->id);
		})->get();

		return view('audits.list.all', ['audits' => $audits]);
	}
}
