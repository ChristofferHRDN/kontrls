<?php

namespace App\Http\Controllers\Audit;

use App\Domain;
use App\Http\Controllers\Controller;
use Illuminate\Http\Request;

use App\Http\Requests;

class Audit extends Controller
{
	public function index(Request $request, Domain $domain, \App\Audit $audit)
	{
		if ($request->wantsJson()) {
			return ['audit' => $audit];
		}
		if ($request->query->get('f') === 'w') {
			$files = $audit->files()->where('weight', '>', 0)->where('caught', 1)->orderBy('weight', 'desc')->paginate(200);
		} else {
			$files = $audit->files()->orderBy('weight', 'desc')->paginate(200);
		}

		$files->appends('f', $request->query->get('f'));

		return view('audits.audit.show', ['audit' => $audit,
			'files' => $files,
			'filefilter' => $request->query->get('f')]);
	}

	public function delete(Request $request, \App\Domain $domain, \App\Audit $audit)
	{
		$audit->files()->delete();
		$audit->statuses()->delete();
		$audit->structure()->delete();
		$audit->interestingStructure()->delete();
		$audit->delete();

		return redirect()->route('domain.audits.list', $domain)->with('message', 'The audit was removed.');
	}
}
