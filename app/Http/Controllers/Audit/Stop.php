<?php

namespace App\Http\Controllers\Audit;

use App\Audit;
use App\Domain;
use App\Http\Controllers\Controller;
use Carbon\Carbon;
use Illuminate\Http\Request;

use App\Http\Requests;

class Stop extends Controller
{
	public function post(Request $request, \App\Domain $domain, \App\Audit $audit)
	{
		$audit->stopped = true;
		$audit->running = false;
		$audit->finished = false;
		$audit->stopped_audit = Carbon::now();
		$audit->save();

		return redirect()->route('domain.audit.show', [$domain, $audit]);
	}
}
