<?php

namespace App\Http\Controllers\Audit;

use App\Audit;
use App\Domain;
use App\Http\Controllers\Controller;
use Illuminate\Http\Request;

use App\Http\Requests;

class FileList extends Controller
{
	public function index(Request $request, \App\Domain $domain, \App\Audit $audit)
	{
		return view('audits.files.all', ['audit' => $audit,
			'domain' => $domain,
			'files' => $audit->structure()->paginate(200)]);
	}
}
