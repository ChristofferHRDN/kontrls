<?php

namespace App\Http\Controllers\Audit;

use App\Auditer\AuditParseException;
use App\Auditer\AuditSender;
use App\Domain;
use App\Http\Controllers\Controller;
use GuzzleHttp\Exception\ClientException;
use Illuminate\Http\Request;

use App\Http\Requests;

class Verify extends Controller
{
	public function create(Request $request, \App\Domain $domain)
	{
		$message = '';
		$error = '';
		if (!$domain->isVerified() && $domain->audit_url) {
			try {
				$message = AuditSender::sendVerifyRequest($domain);
			} catch (AuditParseException $e){
				$error = $e->getMessage();
			} catch(ClientException $e){
				$error = "Exception: " . $e->getMessage();
			} catch(\ErrorException $e){
				$error = "Error: " . $e->getMessage();
			}
			if($error !== ''){
				\Log::error($e);
			}

		}
		return redirect()->route('domain.settings', $domain)->with('verifymessage', $message)->with('error', $error);
	}
}
