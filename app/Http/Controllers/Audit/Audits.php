<?php

namespace App\Http\Controllers\Audit;

use App\Domain;
use App\Http\Controllers\Controller;
use Illuminate\Http\Request;

use App\Http\Requests;

class Audits extends Controller
{
	public function index(Request $request, \App\Domain $domain)
	{
		$audits = $domain->audits;
		return view('audits.list', ['audits' => $audits, 'domain' => $domain]);
	}
}
