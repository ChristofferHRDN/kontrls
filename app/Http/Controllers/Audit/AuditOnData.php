<?php

namespace App\Http\Controllers\Audit;

use App\Audit;
use App\Auditer\Crypter;
use App\Http\Controllers\Controller;
use App\Jobs\Audit\AuditHandleInfo;
use Illuminate\Http\Request;
use Log;

use App\Http\Requests;

class AuditOnData extends Controller
{
	protected $stopMessage = "STOP!!!!!!";

	public function post(Request $request)
	{
		$time = -microtime(true);
		Log::info("onData method visited");
		$audit = Audit::whereHas('domain', function ($query) {
			$query->where('uniq', '=', array_get($_POST, 'uniq'));
		})->where('running', 1)->where('stopped', 0)->first();

		if ($audit === null) {
			echo $this->stopMessage;
			return;
		}

		$key = $audit->domain->identifier;

		try {
			list($data, $result, $type) = $this->parsePost($key);
		} catch (\ErrorException $e) {
			Log::error($e);
			echo $this->stopMessage;
			return;
		}
		Log::info($type);

		if ($type === 'verify') {
			Log::warning('Type was "verify" which should not get here.');
			echo $this->stopMessage;
			return;
		}

		if (in_array($type, ['interestingfilecount',
			'dircount',
			'filecount',
			'structure',
			'interestingstructure',
			'filesandinfo',
			'allcount'])) {
			$job = new AuditHandleInfo($type, $audit, $result);

			dispatch($job->onQueue('audit_work'));
			Log::info("Dispatched $type with time: " . ($time + microtime(true)));
		} else {
			echo $this->stopMessage;
			return;
		}
	}
	protected function parsePost($key)
	{
		return self::parseData(array_get($_POST, 'data', null), $key);
	}

	public static function parseData($data, $key)
	{
		$data = Crypter::decrypt($data, $key);
		$result = json_decode($data, true);
		$type = array_get($result, 'info.type');
		return [$data, $result, $type];
	}
}
