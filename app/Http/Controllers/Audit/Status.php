<?php

namespace App\Http\Controllers\Audit;

use App\Audit;
use App\Domain;
use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Log;

use App\Http\Requests;

class Status extends Controller
{
	/**
	 * @param Request $request
	 * @param Domain $domain
	 * @param Audit $audit
	 *
	 * @return array
	 */
	public function index(Request $request, \App\Domain $domain, \App\Audit $audit)
	{
		Log::info("Get latest status");
		$status = $audit->getStatus();
		return ['status' => $status];
	}
}
