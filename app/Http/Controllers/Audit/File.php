<?php

namespace App\Http\Controllers\Audit;

use App\Auditer\AuditSender;
use App\Auditer\Crypter;
use App\Domain;
use App\Http\Controllers\Controller;
use Illuminate\Http\Request;

use App\Http\Requests;

class File extends Controller
{
	public function index(Request $request, \App\Domain $domain, \App\Audit $audit)
	{
		$result = AuditSender::sendAuditRequest($audit, 'getfile', $request->query->get('f'));
		$result = Crypter::decrypt($result, $domain->identifier);
		$result = json_decode($result, true);

		return view('audits.files.file', ['file' => $result['getfile'],
			'filename' => $result['filename'],
			'result' => $result,
			'audit' => $audit,
		]);
	}
}
