<?php

namespace App\Http\Controllers\Audit;

use App\Domain;
use App\Http\Controllers\Controller;
use App\Jobs\Audit\ScheduledAudit;
use Illuminate\Http\Request;

use App\Http\Requests;

class Schedule extends Controller
{
	public function create(Request $request, \App\Domain $domain)
	{
		if (!$domain->hasScheduledAuditInQueue()) {
			$audit = $this->auditRepo->create(['domain_id' => $domain->id,
				'user_id' => $request->user()->id,
				'initiated_by' => 'scheduling']);
			$scheduledAudit = $domain->scheduledAudit;
			$scheduledAuditJob = new ScheduledAudit($audit, $scheduledAudit);
			dispatch($scheduledAuditJob->onQueue('audit'));
		}

		$audits = $domain->audits;
		return redirect()->route('domain.audits.list', ['audits' => $audits, 'domain' => $domain]);
	}
}
