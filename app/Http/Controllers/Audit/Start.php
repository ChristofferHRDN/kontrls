<?php

namespace App\Http\Controllers\Audit;

use App\Audit;
use App\Domain;
use App\Http\Controllers\Controller;
use App\Http\Requests;
use App\Jobs\Audit\AuditJob;
use App\Repositories\AuditRepository;
use Illuminate\Http\Request;

class Start extends Controller
{
	protected $auditRepo;

	public function __construct(AuditRepository $auditRepository)
	{
		$this->auditRepo = $auditRepository;
	}

	public function create(Request $request, \App\Domain $domain)
	{
		if(!$domain->verified){
			return redirect()->route('domain.audits.list', $domain)->with('error','You must verify your domain before you can audit it. You do that on the settings page.');
		}
		$audit = Audit::where('running', 1)->first();

		$message = 'You already have an audit running. Check it for the results of the audit.';

		if ($audit === null) {
			$audit = $this->auditRepo->create(['domain_id' => $domain->id,
				'user_id' => $request->user()->id,
				'initiated_by' => 'user']);
			dispatch((new AuditJob($audit))->onQueue('audit'));

			$message = 'Started an audit. Check back later for results.';

			$audit->statuses()->create(['status' => 'Queued, awaiting to be processed.']);
		}

		return redirect()->route('domain.audit.show', [$domain, $audit])->with('message', $message);
	}
}
