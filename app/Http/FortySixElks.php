<?php
namespace App\Http;

use GuzzleHttp\Client;

class FortySixElks
{
	private $username;
	private $password;

	public function __construct()
	{
		$this->username = env('ELKSUSERNAME');
		$this->password = env('ELKSPASSWORD');
	}

	public function sendSMS($message, $number = '+46700000000')
	{
		$client = new CLient();

		$sms = array(
			'from' => env('ELKSSMS_FROM'),   /* Can be up to 11 alphanumeric characters */
			'to' => $number,  /* The mobile number you want to send to */
			'message' => $message
		);

		$response = $client->post('https://api.46elks.com/a1/SMS', [
			'auth' => [
				$this->username,
				$this->password,
			],
			'body' => http_build_query($sms),
			'timeout' => 10
		]);
	}
}