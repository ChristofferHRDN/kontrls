<?php

/*
|--------------------------------------------------------------------------
| Application Routes
|--------------------------------------------------------------------------
|
| Here is where you can register all of the routes for an application.
| It's a breeze. Simply tell Laravel the URIs it should respond to
| and give it the controller to call when that URI is requested.
|
*/

Route::get('/', 'WelcomeController@show');

Route::get('/register', function(){
	return redirect('/');
});

Route::post('/audit', 'Audit\AuditOnData@post');

// Prevent people to access if they aren't logged in
Route::group(['middleware' => ['auth']], function () {
	Route::get('/home', 'HomeController@show');

	Route::get('/file', 'ScanFilesController@scan');

	Route::get('/audits', 'Audit\AllAudits@index');

	Route::get('/play/{domain}', 'PlayController@play');

	Route::get('/whitelist', ['as' => 'whitelist.index', 'uses' => 'Whitelist@index']);
	Route::post('/whitelist', ['as' => 'whitelist.store', 'uses' => 'Whitelist@store']);
	Route::delete('/whitelist/{md5}', ['as' => 'whitelist.delete', 'uses' => 'Whitelist@delete']);

	Route::group(['prefix' => 'alerts', 'namespace' => 'Alert'], function(){
		Route::get('/', ['as' => 'alerts.list', 'uses' => 'AlertWithoutDomainController@listWithoutDomain']);
		Route::post('/', ['as' => 'alert_methods.add', 'uses' => 'AlertMethodController@store']);
		Route::delete('/clear-all', ['as' => 'alerts.clear-all', 'uses' => 'AlertWithoutDomainController@clearAllWithoutDomain']);
		Route::post('/read-all', ['as' => 'alerts.read-all', 'uses' => 'AlertWithoutDomainController@readAllWithoutDomain']);
		Route::delete('/{alert}', ['as' => 'alert.destroy', 'uses' => 'AlertWithoutDomainController@delete']);

		Route::delete('/methods/{alert_method}',['as' => 'alert_method.destroy', 'uses' => 'AlertMethodController@delete']);
		Route::get('/alertid/{alertid}', ['as' => 'alert.show-by-id', 'uses' => 'AlertController@showByReadId']);
		Route::get('/{alert}', ['as' => 'alert.show', 'uses' => 'AlertController@show']);
	});


	Route::get('/sendsms/', 'SendSMSController@create');

	Route::group(['prefix' => 'domains'], function () {
		Route::get('/', ['as' => 'domains.list', 'uses' => 'Domain\Domains@index']);
		Route::post('store', ['as' => 'domain.store', 'uses' => 'Domain\Domain@store']);

		Route::get('/all', ['as' => 'domains.all', 'uses' => 'Domain\AllDomains@index']);

		Route::group(['prefix' => '{domain}'], function () {
			Route::get('/', ['as' => 'domain.show', 'uses' => 'Domain\Domain@show']);
			Route::post('/', ['as' => 'domain.update', 'uses' => 'Domain\Domain@update']);
			Route::delete('/', ['as' => 'domain.destroy', 'uses' => 'Domain\Domain@delete']);

			Route::group(['prefix' => 'alerts', 'namespace' => 'Alert'], function(){
				Route::get('/', ['as' => 'domain.alerts.list', 'uses' => 'AlertDomainController@list']);
				Route::post('/read-all', ['as' => 'domain.alerts.read-all', 'uses' => 'AlertDomainController@readAll']);
				Route::delete('/clear-all', ['as' => 'domain.alerts.clear-all', 'uses' => 'AlertDomainController@clearAll']);
				Route::delete('/{alert}', ['as' => 'domain.alert.destroy', 'uses' => 'AlertDomainController@delete']);
			});


			Route::get('/schedule', 'Audit\Schedule@create');


			Route::group(['prefix' => 'audits', 'namespace' => 'Audit'], function(){
				Route::get('/', ['as' => 'domain.audits.list', 'uses' => 'Audits@index']);
				Route::get('/run', ['as' => 'domain.audit.run', 'uses' => 'Start@create']);

				Route::group(['prefix' => '{audit}'], function(){
					Route::get('/', ['as' => 'domain.audit.show', 'uses' => 'Audit@index']);
					Route::delete('/', ['as' => 'domain.audit.delete', 'uses' => 'Audit@delete']);
					Route::get('/files', ['as' => 'domain.audit.files.list', 'uses' => 'FileList@index']);
					Route::get('/file', ['as' => 'domain.file.get-content', 'uses' => 'File@index']);
					Route::get('/status', ['as' => 'domain.audit.status', 'uses' => 'Status@index']);
					Route::post('/stop', ['as' => 'domain.audit.stop', 'uses' => 'Stop@post']);
				});
			});


			Route::group(['prefix' => 'backups'], function(){
				Route::get('/', ['as' => 'domain.backup.list', 'uses' => 'BackupController@index']);
				Route::post('/', ['as' => 'domain.backup.start', 'uses' => 'BackupController@create']);
				Route::post('/verify', ['as' => 'domain.backup.verify', 'uses' => 'BackupVerify@create']);
			});

			Route::get('/data/uptime', ['as' => 'domain.data.uptime', 'uses' => 'Domain\Domain@getUptimeGraphData']);

			Route::post('/group', ['as' => 'domain.group.add', 'uses' => 'Domain\Group@store']);
			Route::delete('/group', ['as' => 'domain.group.remove', 'uses' => 'Domain\Group@delete']);

			Route::get('/logs', ['as' => 'domain.logs', 'uses' => 'Domain\Logs@index']);

			Route::get('/uptime', ['as' => 'domain.uptime', 'uses' => 'Domain\UptimeController@index']);

			Route::post('/monitors', ['as' => 'domain.monitors.store', 'uses' => 'Domain\Monitor@store']);
			Route::delete('/monitors/{monitor}', ['as' => 'domain.monitors.destroy', 'uses' => 'Domain\Monitor@destroy']);
			Route::post('/monitors/{monitor}', ['as' => 'domain.monitor.edit', 'uses' => 'Domain\Monitor@edit']);
			Route::post('/monitors/{monitor}/toggle', ['as' => 'domain.monitor.toggle', 'uses' => 'Domain\Monitor@toggle']);

			Route::get('/notes', ['as' => 'domain.notes', 'uses' => 'Domain\Note@listNotes']);
			Route::post('/notes', ['as' => 'domain.notes.store', 'uses' => 'Domain\Note@addNote']);
			Route::get('/notes/{note}', ['as' => 'domain.notes.edit', 'uses' => 'Domain\Note@editNote']);
			Route::delete('/notes/{note}', ['as' => 'domain.notes.destroy', 'uses' => 'Domain\Note@removeNote']);

			Route::get('/queue', ['as' => 'domain.queue.check', 'uses' => 'QueueDomainController@isQueued']);
			Route::post('/queue', ['as' => 'domain.queue', 'uses' => 'QueueDomainController@queue']);
			Route::get('/queue/remove', ['as' => 'domain.queue.remove', 'uses' => 'QueueDomainController@removeFromQueue']);

			Route::get('/settings', ['as' => 'domain.settings', 'uses' => 'Domain\Settings@index']);
			Route::post('/settings', ['as' => 'domain.settings.update', 'uses' => 'Domain\Settings@update']);

			Route::get('/syncfile', ['as' => 'domain.sync-file', 'uses' => 'Domain\SyncFile@create']);
			
			Route::post('/verify', ['as' => 'domain.verify', 'uses' => 'Audit\Verify@create']);
		});

	});

	Route::group(['prefix' => 'groups'], function () {
		Route::get('/', ['as' => 'groups.list', 'uses' => 'GroupController@listGroups']);
		Route::post('store', ['as' => 'group.store', 'uses' => 'GroupController@store']);

		Route::group(['prefix' => '{group}', 'as' => 'group.'], function () {
			Route::get('/', ['as' => 'show', 'uses' => 'GroupController@show']);
			Route::post('/', ['as' => 'domains.add', 'uses' => 'GroupController@add_domain']);
			Route::delete('/', ['as' => 'destroy', 'uses' => 'GroupController@destroy']);

			Route::delete('/domains/{domain}', ['as' => 'domains.remove', 'uses' => 'GroupController@remove_domain']);

			Route::post('/notes', ['as' => 'group.notes.store', 'uses' => 'GroupController@add_note']);
			Route::delete('/notes/{note}', ['as' => 'group.notes.destroy', 'uses' => 'GroupController@remove_note']);

			Route::post('/settings', ['as' => 'settings.update', 'uses' => 'GroupController@update_settings']);
		});
	});

	Route::group(['prefix' => 'urls'], function(){
		Route::post('store/{domain}', ['as' => 'url.store', 'uses' => 'Domain\Url@store']);
		Route::post('{domain}/{url}', ['as' => 'url.update', 'uses' => 'Domain\Url@update']);
		Route::delete('{domain}/urls/{url}', ['as' => 'url.destroy', 'uses' => 'Domain\Url@destroy']);
	});
});
