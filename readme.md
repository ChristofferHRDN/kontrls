# Kontrl.io - Kontrls

A project to control your domains, be it your own, or your clients.

A range of features are planned, including Uptime checking, Alerts when something goes wrong, source code control and more.

## Pings
To use Pings you first need to:
1. Create a Domain on /domains
2. Go into that Domain object.
3. Configure atleast one Url to be used. This is not bound to the actual domain name of the Domain object, it could be anything in theory.
4. Add a Monitor to that Url. Choose which Url to check, what check type, Checking conditions, and Expected values.
5. You can also check the Is this a negative match? box to have the check perform a negative check.

There are currently three types:
1. Uptime
2. String
3. Regex

Uptime only checks the uptime of the url. If it gets a response back on it's GET request, then it has succeeded.

String checks if a string is available on the page.

Regex checks if a regex matches with the expected value on the page.

When you have added a Monitor it will be visible in the Monitor table. Here you can edit, enable or disable, or remove the Monitor.


## Alerts
Alerts are ways to detect when things don't go as they should. To configure alerts you go to /settings#/alerts.
Here you can create Alert methods, ways that Kntrl.io can alert you if it detects that something is wrong.
Currently alerts only react to Uptime failures, when a page didn't load. If a Monitor fails a Uptime check, it will send out an Alert.

What you need to fill out to add an Alert method is this:

1. Alert method, the physial way that you will get alerted. Currently only E-mail is supported, but a SMS feature is planned.
2. Types that alert should trigger on. Currently only Uptime is supported, but more are planned.
3. Domains that the Alert method will be used with. If you do not choose one, "All" will be used, and the alert method will be alerted for all domains.
4. Alert way. If you choose E-mail, this is your e-mail address. If you choose SMS, this is your telephone number.

Currently you can only remove or create new alert methods, not edit the ones that exist.