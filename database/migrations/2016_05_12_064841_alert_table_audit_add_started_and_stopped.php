<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AlertTableAuditAddStartedAndStopped extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('audits', function(Blueprint $table){
            $table->dateTime('started_audit');
            $table->dateTime('stopped_audit');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('audits', function(Blueprint $table){
            $table->dropColumn(['started_audit', 'stopped_audit']);
        });
    }
}
