<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AlterTableMonitorsAddMonitorCheckId extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('monitors', function($table){
            $table->integer('monitor_check_id')->unsigned();

            $table->foreign('monitor_check_id')->references('id')->on('monitor_checks');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('monitors', function($table){
            $table->dropColumn('monitor_check_id');
            $table->dropForeign('monitor_check_id_foreign');
        });
    }
}
