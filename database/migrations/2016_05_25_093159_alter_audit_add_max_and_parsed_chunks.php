<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AlterAuditAddMaxAndParsedChunks extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('audits', function(Blueprint $table){
            $table->integer('total_chunks');
            $table->integer('parsed_chunks');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('audits', function(Blueprint $table){
            $table->dropColumn(['parsed_chunks', 'total_chunks']);
        });
    }
}
