<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AlterTableBackupStatusesRenamedColumn extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('backup_statuses', function(Blueprint $table){
            $table->renameColumn('backupid', 'bid');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('backup_statuses', function(Blueprint $table){
            $table->renameColumn('bid', 'backupid');
        });
    }
}
