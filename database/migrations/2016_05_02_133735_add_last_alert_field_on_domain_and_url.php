<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddLastAlertFieldOnDomainAndUrl extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('domains', function($table){
            $table->longText('last_alerts');
        });
        Schema::table('urls', function($table){
            $table->longText('last_alerts');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('domains', function($table){
            $table->dropColumn('last_alerts');
        });
        Schema::table('urls', function($table){
            $table->dropColumn('last_alerts');
        });
    }
}
