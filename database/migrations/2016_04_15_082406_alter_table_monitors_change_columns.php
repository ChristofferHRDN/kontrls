<?php

use Illuminate\Database\Migrations\Migration;

class AlterTableMonitorsChangeColumns extends Migration
{
	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::table('monitors', function ($table) {
			$table->dropColumn(['check', 'expected']);
		});
		Schema::table('monitors', function (\Illuminate\Database\Schema\Blueprint $table) {
			$table->longText('check');
		});
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		//
	}
}
