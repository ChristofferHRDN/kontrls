<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AlterTableDomainsAndGroupsAddSettingsId extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('domains', function($table){
            $table->integer('settings_id')->unsigned()->nullable();

            //$table->foreign('settings_id')->references('id')->on('settings');
        });
        Schema::table('groups', function($table){
            $table->integer('settings_id')->unsigned()->nullable();

            //$table->foreign('settings_id')->references('id')->on('settings');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('groups', function($table){
            $table->dropColumn('settings_id');
        });
        Schema::table('domains', function($table){
            $table->dropColumn('settings_id');
        });
    }
}
