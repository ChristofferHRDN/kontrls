<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateScheduledAuditsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('scheduled_audits', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('domain_id')->unsigned();
            $table->string('last_audit')->default('0000-00-00 00:00:00');
            $table->string('next_audit')->default('0000-00-00 00:00:00');
            $table->integer('wait_time');
            $table->boolean('enabled')->default(1);
            $table->timestamps();
            $table->boolean('set_via_group')->default(0);

            $table->foreign('domain_id')->references('id')->on('domains')->onDelete('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('scheduled_audits');
    }
}
