<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AlterTableMonitorsDropColumnMonitorCheckIdAddCheckTypeId extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('monitors', function($table){
            $table->dropColumn('monitor_check_id');
        });
        Schema::table('monitors', function($table){
            $table->integer('check_type_id')->unsigned()->nullable();
            $table->mediumText('check')->nullable();
            $table->mediumText('expected')->nullable();

            $table->foreign('check_type_id')->references('id')->on('check_types');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
    }
}
