<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateBackupStatusesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('backup_statuses', function (Blueprint $table) {
            $table->increments('id');
            $table->string('status');
            $table->integer('backup_id')->unsigned();
            $table->string('domain');
            $table->text('step');
            $table->text('errors');
            $table->mediumText('warnings');
            $table->integer('progress')->unsigned();
            $table->string('stepstate');
            $table->integer('stepnumber');
            $table->integer('backupid')->unsigned();
            $table->timestamps();

            $table->foreign('backup_id')->references('id')->on('backups')->onDelete('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('backup_statuses');
    }
}
