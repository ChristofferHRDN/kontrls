Vue.component('domain-uptime-graph', {
    props: {
        domainid: String,
    },
    template: `
<div>
    <div v-show="data.length > 0">
        <i v-if="entries === null" class="fa fa-spinner fa-spin"></i>
        <p v-if="entries !== null">{{entries}} of entries  in graph.</p>
        <p>Click to toggle livedata on or off: <button @click="toggleLiveData" class="button hollow secondary">{{liveData == true ? "Live data" : "Old data"}}</button> <i class="fa fa-spinner fa-spin" v-show="fetching"></i></p>
    </div>
    <div v-show="data.length == 0">
        <p>{{status}} <i class="fa fa-spinner fa-spin" v-if="fetching"></i></p>
    </div>
    <canvas id="domain-uptime-graph" v-show="data.length > 0"></canvas>
</div>
`,
    ready(){
        setTimeout(() => {
            this.fetchGraphData();
            this.timelyUpdateOfChart();
        }, 1000)
    },
    data(){
        return {
            data: [],
            dates: [],
            entries: null,
            chart: null,
            chartData: {},
            ctx: null,
            liveData: true,
            fetching: false,
            status: ''
        }
    },
    methods: {
        fetchGraphData(){
            this.fetching = true
            this.status = 'Fetching data'
            this.$http.get('/domains/' + this.domainid + '/data/uptime').then(response => {
                this.status = 'Got data. Parsing it now.'
                this.data = response.data.data
                this.dates = response.data.dates
                this.entries = this.data.length
                this.fetching = false

                this.chartData = {
                    labels: this.dates,
                    datasets: [
                        {
                            label: "Response time",
                            lineTension: 0,
                            // String - the color to fill the area under the line with if fill is true
                            backgroundColor: "rgba(76, 175, 80, 0.4)",

                            // String - Line color
                            borderColor: "rgba(76, 175, 80, 1)",
                            data: this.data
                        }
                    ]
                };
                this.ctx = document.getElementById("domain-uptime-graph");
                Chart.defaults.global.animation.duration = 500;
                let chart = new Chart(this.ctx, {
                    type: 'line',
                    data: this.chartData,
                    options: {
                        scales: {
                            yAxes: [
                                {
                                    ticks: {
                                        beginAtZero: true
                                    }
                                }
                            ],
                            xAxes: [
                                {
                                    type: "time",
                                    time: {
                                        displayFormats: {
                                            'second': 'HH:mm:ss',
                                            'minute': 'HH:mm:ss'
                                        }
                                    }
                                }
                            ]
                        }
                    }
                });
                this.chart = chart;
                this.chart.resize();
                if (this.data.length === 0) {
                    this.status = 'No graph data seems to be found.'
                }
            })
        },
        updateGraph(){
            this.chart.update();
        },
        fetchNewData(){
            this.fetching = true;
            this.$http.get('/domains/' + this.domainid + '/data/uptime').then(response => {
                this.data = response.data.data
                this.dates = response.data.dates
                this.entries = this.data.length
                this.fetching = false;

                this.chartData.labels = this.dates;
                this.chartData.datasets[0].data = this.data;

                console.log("Update chart");

                this.updateGraph();
            })
        },
        timelyUpdateOfChart(){
            if (this.liveData) {
                if(this.chart){
                    this.chart.resize();
                }

                this.fetchNewData();

                console.log("Timely update of chart");

                setTimeout(this.timelyUpdateOfChart, 5000);
            }
        },
        toggleLiveData(){
            this.liveData = !this.liveData;
            if (this.liveData) {
                this.timelyUpdateOfChart()
            }
        }
    }
});
