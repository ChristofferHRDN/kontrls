/**
 * Created by christoffer on 29/04/16.
 */
Vue.component('log', {
    props: {
        domainid: String,
    },
    template: `
                <div>
                    <p>20 latest check log entries.</p>
                    <table>
                        <thead>
                            <tr>
                            <th>Type</th>
                            <th>Url</th>
                            <th>Log entry</th>
                            <th>Succeeded</th>
                            <th>Result</th>
                            <th>Logged</th>
                            </tr>
                        </thead>
                        <tbody>
                            <tr v-for="log in logs">
                                <td>{{ log.log_type }}</td>
                                <td>{{ parsedLogs[$index].monitor && parsedLogs[$index].monitor.url}}</td>
                                <td>
                                    <button class="button small hollow secondary"
                                style="margin-bottom: 0" @click="showLog(parsedLogs[$index])">
                                    Log</button>
                                </td>
                                <td>{{ parsedLogs[$index].succeeded == 1 ? "Yes" : "No"}}</td>
                                <td v-if="parsedLogs[$index].check && isArray(parsedLogs[$index].check.result)">{{ parsedLogs[$index].check.result.result ? "Succeeded" : "Failed"}}</td>
                                <td v-else>{{ parsedLogs[$index].check && parsedLogs[$index].check.result ? "Succeeded" : "Failed"}}</td>
                                <td>{{ log.created_at }}</td>
                            </tr>
                        </tbody>
                    </table>
                    <div class="modal fade" tabindex="-1" role="dialog" id="codemodal">
                        <div class="modal-dialog">
                            <div class="modal-content">
                                <div class="modal-header">
                                    <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span
                                                aria-hidden="true">&times;</span></button>
                                    <h4 class="modal-title"></h4>
                                </div>
                                <div class="modal-body">
                                    <pre><code></code></pre>
                                </div>
                                <div class="modal-footer">
                                    <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                                </div>
                            </div><!-- /.modal-content -->
                        </div><!-- /.modal-dialog -->
                    </div><!-- /.modal -->
                </div>`,
    ready(){
        this.timelyUpdateLogs()
    },
    data(){
        return {
            logs: [],
            parsedLogs: [],
        }
    },
    methods: {
        showLog(log){
            let modal = $("#codemodal");
            modal.find('.modal-body code').text(JSON.stringify(log, null, 4));
            modal.modal('show');
        },
        isArray(arr){
            return Array.isArray(arr);
        },
        fetchLogs(){
            this.$http.get('/domains/' + this.domainid + '/logs').then(response => {
                this.logs = response.data.logs
                this.parsedLogs = this.logs.map(log => {
                    return JSON.parse(log.log)
                })
            })
        },
        timelyUpdateLogs(){
            this.fetchLogs();
            setTimeout(this.timelyUpdateLogs, 5000);
        }
    }
});