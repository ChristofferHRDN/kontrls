Vue.component('audit-info', {
    props: ['auditid', 'domainid', 'audit', 'status'],
    template: `
        <div class="callout small-12 columns">
            <slot name="header"></slot>
            <div class="row">
                <div class="small-6 columns">
                    <p><strong>Domain:</strong> <a href="/domains/{{audit.domain.hashid}}">{{audit.domain.name}}</a></p>
                    <p><strong>Finished:</strong> {{audit.finished ? "Yes" : "No"}}</p>
                    <p><strong>Running:</strong> {{audit.running ? "Yes" : "No"}}</p>
                    <p><strong>Started at:</strong> {{audit.started_audit}}</p>
                </div>
                <div class="small-6 columns">
                    <p><strong>File count:</strong> {{audit.file_count ? audit.file_count : 0}}</p>
                    <p><strong>Interesting file count (files that will be
                            scanned):</strong> {{audit.interesting_filecount ? audit.interesting_filecount : 0}}</p>
                    <p><strong>Directory count:</strong> {{audit.dir_count ? audit.dir_count :  0}}</p>
                    <p><strong>Stopped at:</strong> {{audit.stopped_audit ? audit.stopped_audit : "Haven't stopped"}}</p>
                </div>
            </div>
            <div class="row">
                <div class="columns">
                    <p>
                        <strong>Status:</strong> {{status}} <i v-show="fetchingStatus" class="fa fa-spinner fa-spin"></i> 
                    </p>
                    
                    <button type="submit" class="btn btn-primary">
                        <a href="/domains/{{audit.domain.hashid}}/audits/{{audit.hashid}}/files" style="color:white;">All files</a>
                    </button>
                    
                        <slot name="buttons"></slot>
                        <slot></slot>
                    </div>
                </div>
            </div>
        </div>`,

    ready() {
        this.updateStatus()
        this.updateAudit()
    },
    data(){
        return {
            fetchingStatus: false,
        }
    },
    methods: {
        updateStatus(){
            this.fetchingStatus = true
            this.$http.get('/domains/' + this.domainid + '/audits/' + this.auditid + '/status').then(response => {
                this.fetchingStatus = false
                this.status = response.data.status
                if(this.status !== 'Done' && this.status !== 'Stopped'){
                    setTimeout(this.updateStatus, 1000)
                }
            })
        },
        updateAudit(){
            this.$http.get('/domains/' + this.domainid + '/audits/' + this.auditid).then(response => {
                this.audit = response.data.audit
                if(this.status !== 'Done' && this.status !== 'Stopped'){
                    setTimeout(this.updateAudit, 2500)
                }
            })
        }
    }

});
