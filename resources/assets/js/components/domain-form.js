Vue.component('domain-form', {
    props: ['route'],
    template: `
<div class="alert alert-danger" v-show="form.errors.hasErrors()">
    <span class="help-block" v-show="form.errors.has('url')">
        <strong>{{ form.errors.get('url') }}</strong>
    </span>
</div>
<div class="input-group">
    <input type="url" @keyup.enter="submit" v-model="form.url" class="input-group-field" autofocus placeholder="Domain URL to the live site">
    <input type="text" v-model="form.name" class="input-group-field" placeholder="Domain name. Leave empty to get default name">
    <div class="input-group-button">
        <button @click="submit" class="btn btn-default" :disabled="form.busy" style="font-size: 14px;">
            <i class="fa fa-plus"></i> Add Domain
        </button>
    </div>
</div>`,

    ready() {

    },
    data(){
        return {
            form: new SparkForm({
                url: '',
                name: ''
            })
        }
    },
    methods: {
        submit() {
            Spark.post(this.route, this.form).then(response => {
                window.location = response.data.route;
            });
        }
    }

});
