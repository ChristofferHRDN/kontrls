Vue.component('all-domains', {
    props: ['user'],
    template: `
<div>
    <h2>All domains</h2>
    <table>
        <thead>
            <tr>
                <th>Name</th>
                <th>Verified</th>
                <th>User</th>
            </tr>
        </thead>
        <tbody>
            <tr v-for="domain in domains">
                <td><a href="/domains/{{domain.hashid}}">{{domain.name}}</a></td>
                <td>{{domain.verified ? 'Yes' : 'No'}}</td>
                <td><a href="/spark/kiosk#/users/{{domain.user.id}}">{{domain.user.name}}</a></td>
            </tr>
        </tbody>
    </table>
</div>`,

    ready() {
        this.$http.get('/domains/all').then(response => {
            this.domains = response.data.domains.data
            this.extra = response.data.domains
        })
    },
    data(){
        return {
            domains: [],
            extra: null
        }
    }

});
