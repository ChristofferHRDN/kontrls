Vue.component('domain', {
    props: ['domainid','is_queued', 'group'],

    ready() {
        this.$http.get('/domains/' + this.domainid).then(response => {
            console.log(response.data)
            this.domain = response.data.domain

            this.domain.monitors.forEach(monitor => {
                this.monitorForms.push(new SparkForm({
                    url: monitor.url.url,
                    check: monitor.check,
                    check_value: this.checkValue(monitor),
                    expected_value: this.expected(monitor)
                }))
            })
            this.groups = Object.keys(response.data.groups).map(key => {
                return {id: key, name: response.data.groups[key]}
            })
            this.data = response.data.data
            this.dates = response.data.dates
            if(this.groups[0])
            {
                this.group_id = this.groups[0].id
            }
            this.is_queued = response.data.queued
        }, error => {
            console.log(error, arguments)
        }).catch(function(){
            console.log(arguments)
        })
    },
    data(){
        return {
            domain: {
                url: '',
                name: '',
                monitors: []
            },
            groups: [],
            group_id: 0,
            editid: 0,
            data: [],
            dates: [],
            editname: "",
            monitorForms: [],
            form: new SparkForm({
                url: "",
                check: "",
                check_value: "",
                expected_value: "",
                should_fail: false,
                monitor: null
            })
        }
    },
    computed: {
        is_queued_text(){
            return this.is_queued === true ? "Remove from queue" : "Queue";
        },
        backgroundstyle(){
            let style = {backgroundColor: "red", borderRadius: "50%", width: "20px", height: "20px"}

            if (this.is_queued) {
                style.backgroundColor = "green"
            }
            return style
        }
    },
    methods: {
        stringify(data){
            console.log(JSON.stringify(data))
        },
        removeGroup(){
            console.log("Removegroup method");
            console.log('/domains/' + this.domainid + '/group');
            this.$http.delete('/domains/' + this.domainid + '/group').then(response => {
                this.group = null
            })
        },
        addGroup(){
            console.log(this)
            this.$http.post('/domains/' + this.domainid + '/group', {
                group_id: this.group_id
            }).then(response => {
                this.group = response.data.group
            })
        },
        queue(){
            if (this.is_queued) {
                this.$http.get('/domains/' + this.domainid + '/queue/remove').then(response => {
                    this.is_queued = response.data.is_queued
                })
            } else {
                this.$http.post('/domains/' + this.domainid + '/queue').then(response => {
                    this.is_queued = response.data.is_queued
                })
            }
        },
        editMonitor(monitor){
            this.editid = monitor.id
            this.editname = monitor.name
            this.form.url = monitor.url.url
            this.form.check = monitor.check_type_id
            this.form.check_value = this.checkValue(monitor)
            this.form.expected_value = this.expected(monitor)
            this.form.should_fail = this.shouldFail(monitor)
            this.form.monitor = monitor
        },
        cancelEdit(){
            this.editid = 0
            this.editname = ""
        },
        saveEditMonitor(id){
            Spark.post('/domains/' + this.domainid + '/monitors/' + id, this.form).then(response => {
                function findWithAttr(array, attr, value) {
                    for(var i = 0; i < array.length; i += 1) {
                        if(array[i][attr] === value) {
                            return i;
                        }
                    }
                }
                let monitor =this.domain.monitors[findWithAttr(this.domain.monitors, 'id', id)]
                monitor.url = response.monitor.url
                monitor.check = response.monitor.check
            })
        },
        deleteMonitor(id){
            this.$http.delete('/domains/' + this.domainid + '/monitors/' + id).then(response => {
                let targetIndex = undefined;
                this.domain.monitors.forEach((elm, index) => {
                    if(elm.id == id){
                        targetIndex = index;
                    }
                })
                if(targetIndex !== undefined){
                    this.domain.monitors.splice(targetIndex, 1);
                }
            })
        },
        checkValue(monitor){
            return JSON.parse(monitor.check).check_value;
        },
        expected(monitor){
            return JSON.parse(monitor.check).expected;
        },
        shouldFail(monitor){
            return JSON.parse(monitor.check).should_fail;
        },
        toggleMonitor(id){
            this.$http.post('/domains/' + this.domainid + '/monitors/' + id + '/toggle').then(response => {
                this.domain.monitors.filter(v=> {
                    return v.id === id
                })[0].enabled = response.data.enabled
            })
        },
        shouldShow(value, monitor){
            let displayedFields = {
                1: {check_value: false, expected_value: true, should_fail: true},
                2: {check_value: true, expected_value: true, should_fail: true},
                3: {check_value: true, expected_value: false, should_fail: true},
            }
            let fetchedValue = displayedFields[monitor.check_type_id][value]
            return fetchedValue !== undefined && fetchedValue !== false
        },
        getMonitorEditLabel(value, monitor){
            let displayedFields = {
                1: {expected_value: 'Allowed status codes', should_fail: 'Should this check fail?'},
                2: {
                    check_value: 'Regex string',
                    expected_value: 'Return (JSON format)',
                    should_fail: 'Should this check fail?'
                },
                3: {check_value: 'String to search for', should_fail: 'Should this check fail?'},
            }
            return displayedFields[monitor.check_type_id][value]
        }
    }
});
