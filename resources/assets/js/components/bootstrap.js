
/*
 |--------------------------------------------------------------------------
 | Laravel Spark Components
 |--------------------------------------------------------------------------
 |
 | Here we will load the Spark components which makes up the core client
 | application. This is also a convenient spot for you to load all of
 | your components that you write while building your applications.
 */

require('./../spark-components/bootstrap');

require('./home');

require('./domain');

require('./domains');

require('./domain-uptime-graph');

require('./log');

require('./all-domains');

require('./audit-status');

require('./audit-info');

require('./domain-form');