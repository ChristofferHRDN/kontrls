Vue.component('audit-status', {
    props: ['auditid', 'domainid'],
    template: `
<p>
    <strong>Status:</strong> {{status}}
</p>`,

    ready() {
        this.updateStatus()
    },
    data(){
        return {
            status: ''
        }
    },
    methods: {
        updateStatus(){
            this.$http.get('/domains/' + this.domainid + '/audits/' + this.auditid + '/status').then(response => {
                this.status = response.data.status
                setTimeout(this.updateStatus, 2500)
            })
        }
    }

});
