Vue.component('home', {
    props: ['user'],

    ready() {
        console.log("Ready");
        this.$http.get('/domains').then(response => {this.domains = response.data.domains})
        this.$http.get('/groups').then(response => {this.groups = response.data.groups})
    },
    data(){
        return {
            domains: [],
            groups: []
        }
    }

});
