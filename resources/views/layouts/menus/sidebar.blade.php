<div class="side-bar">
    <ul id="menu-showhide" class="topnav slicknav">
        <li>
            <a id="menu-select" class="tooltip-tip" href="{{ route('dashboard') }}" title="Dashboard">
                <i class="icon-monitor"></i>
                <span>Dashboard</span>
            </a>
        </li>
        <li>
            <a class="tooltip-tip" href="#">
                <i class=" icon-window"></i>
                <span>Domains</span>
            </a>
            <ul>
                <li>
                    <a href="{{ route('domains.list') }}">All domains</a>
                </li>
            </ul>
        </li>
        <li>
            <a href="#">
                <i class="icon-window"></i>
                <span>Groups</span>
            </a>
            <ul>
                <li>
                    <a href="{{ route('groups.list') }}">All groups</a>
                </li>
            </ul>
        </li>
    </ul>
</div>
<ul class="bottom-list-menu">
    <li><a href="#">Settings <span class="icon-gear"></span></a>
    </li>
    <li><a href="#">Help <span class="icon-phone"></span></a>
    </li>

</ul>