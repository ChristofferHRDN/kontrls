@extends('spark::layouts.app')

@section('content')
    <home :user="user" inline-template>
        <div class="container">
            <div class="row small-12 columns">
                <h1>Dashboard</h1>
            </div>
            <!-- Application Dashboard -->
            @include('domains.domain.list', ['domains' => Auth::user()->domains()->take(20)->get(), 'limited' => true])
            @include('groups.group.list', ['groups' => Auth::user()->groups()->take(20)->get(), 'limited' => true])
        </div>
    </home>
@endsection
