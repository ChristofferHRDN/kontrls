<div class="row">
    <div class="small-12 columns">
        <div class="callout">
            <div class="panel-heading"><h4 id="alerts">Alert methods</h4></div>
            <div class="panel-body">
                {!! Form::open(['method' => 'post', 'route' => ['alert_methods.add']])!!}
                <label>Alert method
                    {!! Form::select('method', App\AlertMethod::getValidMethods()) !!}
                </label>
                <label>Types that the alert should trigger on. Separate each type with a comma.
                    <select id="tokenize" multiple="multiple" class="tokenize" name="types[]">
                        @foreach(App\AlertMethod::getValidTypes() as $type => $display)
                            <option value="{{ $type }}" selected="selected">{{ $display }}</option>
                        @endforeach
                    </select>
                </label>
                <style>
                    .tokenize {
                        width: 100%;
                    }
                </style>
                <label>Only these domains will be used. Default is all domains
                    <?php $domains = Auth::user()->domains()->pluck('name', 'id'); ?>
                    <select name="alert_domains[]" multiple>
                        @foreach($domains as $id => $domain)
                            <option value="{{ $id }}">{{$domain}}</option>
                        @endforeach
                    </select>
                </label>
                <label>The way that alerts will be sent. If you choose the SMS method, this will be a telephone number.
                    If you choose Email, this will be an email address that alerts will be sent to.
                    <input type="text" name="alert_way">
                </label>
                <input type="submit" value="Save" class="button primary">
                {!! Form::close() !!}
                @if(count(Auth::user()->alert_methods))
                    <table>
                        <thead>
                        <tr>
                            <th>Method</th>
                            <th>Types</th>
                            <th>Enabled</th>
                            <th>Domains</th>
                            <th>Alert way</th>
                            <th></th>
                        </tr>
                        </thead>
                        <tbody>
                        @foreach(Auth::user()->alert_methods as $am)
                            <tr>
                                <td>{{ $am->method }}</td>
                                <td>{{ implode(', ', $am->types) }}</td>
                                <td>{{ $am->enabled === 1 ? "Yes" : "No"}}</td>
                                <td>
                                    @if(gettype($am->alert_domains) === "array")
                                        @foreach($am->alert_domains as $domain)
                                            <a href="{{route('domain.show', $domain)}}">{{ $domain->name }}</a>
                                        @endforeach
                                    @else
                                        {{ $am->alert_domains }}
                                    @endif
                                </td>
                                <td>{{$am->alert_way}}</td>
                                <td>
                                    {!! Form::open(['method' => 'delete', 'route' => ['alert_method.destroy', $am]]) !!}
                                    <button type="submit"><i class="fa fa-trash"></i></button>
                                    {!! Form::close() !!}
                                </td>
                            </tr>
                        @endforeach
                        </tbody>
                    </table>
                @endif
            </div>
        </div>
    </div>
</div>