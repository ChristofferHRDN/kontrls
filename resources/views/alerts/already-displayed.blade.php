@extends('spark::layouts.app')
@section('content')
    <div class="row">
        <div class="small-12 columns">
            <p>Please go to your <a href="/alerts">Alert list</a> instead. This link has already been used.</p>
            <p>You didn't read the alert? Then you might want to check who else has access to your units.</p>
        </div>
    </div>
@endsection