@extends('spark::layouts.app')

@section('content')
    @include('domains.domain.menu', ['domain' => $alert->domain])
    <div class="row">
        <div class="small-12 columns">
            <div class="columns callout">
                <h1>Alert</h1>
                <div>
                    <p>
                    <pre>{{$alert->message}}</pre>
                    </p>
                    <p><strong>Read:</strong> {{$alert->read ? 'Yes' : 'No'}}</p>
                    <p><strong>Alerted at:</strong> {{$alert->created_at}}</p>
                    <p><strong>Domain:</strong> <a
                                href="{{route('domain.show', $alert->domain)}}">{{$alert->domain->name}}</a></p>

                    {!! Form::open(['method' => 'delete', 'route' => ['domain.alert.destroy', $alert->domain, $alert]]) !!}
                    <button type="submit" class="btn btn-danger">
                        <i class="fa fa-trash-o"></i> Delete this alert
                    </button>
                    {!! Form::close() !!}
                </div>
            </div>
        </div>
    </div>
@endsection

