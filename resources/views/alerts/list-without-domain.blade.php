@extends('spark::layouts.app')

@section('content')
    <div class="row">
        <div class="small-12 columns">
            <div class="columns callout">
                <h1>All your alerts</h1>
                @if(count($alerts) > 0)
                    <div>
                        <p><a class="btn btn-danger" role="button" data-toggle="collapse" href="#collapseClear"
                              aria-expanded="false" aria-controls="collapseClear">
                                Clear all alerts
                            </a>
                            <a class="btn btn-warning" role="button" data-toggle="collapse"
                               href="#collapseMarkAllAsRead"
                               aria-expanded="false" aria-controls="collapseMarkAllAsRead">
                                Mark all as read
                            </a>
                        </p>
                        <div class="collapse" id="collapseClear">
                            <div class="well">
                                <p>Are you sure you want to clear all of your alerts? There is no going back from
                                    this.</p>
                                {!! Form::open(['method' => 'delete', 'route' => ['alerts.clear-all']]) !!}
                                <button type="submit" class="btn btn-danger">
                                    <i class="fa fa-trash-o"></i> I am sure, clear them all
                                </button>
                                {!! Form::close() !!}
                            </div>
                        </div>
                        <div class="collapse" id="collapseMarkAllAsRead">
                            <div class="well">
                                <p>Are you sure you want to mark all of your alerts as read? There is no going back from
                                    this.</p>
                                {!! Form::open(['post' => 'delete', 'route' => ['alerts.read-all']]) !!}
                                <button type="submit" class="btn btn-warning">
                                    <i class="fa fa-trash-o"></i> I am sure, mark them as read
                                </button>
                                {!! Form::close() !!}
                            </div>
                        </div>
                    </div>

                    <table>
                        <thead>
                        <tr>
                            <th>Message</th>
                            <th>Sent at</th>
                            <th>Method</th>
                            <th>Read</th>
                            <th></th>
                        </tr>
                        </thead>
                        <tbody>
                        @foreach($alerts as $alert)
                            <tr>
                                <td><a href="{{route('alert.show', $alert)}}">{{$alert->message}}</a></td>
                                <td>{{$alert->created_at}}</td>
                                <td>{{$alert->alert_method ?  $alert->alert_method->method : ''}}</td>
                                <td>{{$alert->read ? 'Yes' : 'No'}}</td>
                                <td>{!! Form::open(['method' => 'delete', 'route' => ['alert.destroy', $alert, 'page' => $page]]) !!}
                                    <button type="submit" class="btn btn-danger">
                                        <i class="fa fa-trash-o"></i> Delete
                                    </button>
                                    {!! Form::close() !!}</td>
                            </tr>
                        @endforeach

                        </tbody>
                    </table>
                    <div>
                    </div>

                    {!! $alerts->links() !!}
                @else
                    <p>There seems to be no alerts to be displayed at this moment. Have you configured your <a href="/settings#/alerts">alert methods</a>?</p>
                @endif
            </div>
        </div>

    </div>
@endsection