@extends('spark::layouts.app')

@section('content')
    @if(session('status'))
        <div class="row">
            <div class="alert alert-success">
                <strong>Success!</strong> {{session('status')}}
            </div>
        </div>
    @endif
    <div class="row">
        <div class="callout small-12 columns">
            <p>The MD5's are domain agnostic, meaning that they do not know or care about any domains. As such
                whitelisting
                a MD5 will mean that it will be allowed in all audits on all domains.</p>
            {!! Form::open(['method' => 'post', 'route' => ['whitelist.store']]) !!}
            <div class="input-group">
                <input class="input-group-field" type="text" maxlength="32" name="md5" placeholder="MD5 hash">
                <div class="input-group-button">
                    <input type="submit" class="button" value="Save md5">
                </div>
            </div>
            {!! Form::close() !!}
        </div>
    </div>
    @if(count($md5s))
        <div class="row">
            <div class="callout small-12 columns">
                <table>
                    <thead>
                    <tr>
                        <td>MD5</td>
                        <td>Added at</td>
                        <td></td>
                    </tr>
                    </thead>
                    <tbody>
                    @foreach($md5s as $md5)
                        <tr>
                            <td>{{$md5->md5}}</td>
                            <td>{{$md5->created_at}}</td>
                            <td>
                                {!! Form::open(['method' => 'delete', 'route' => ['whitelist.delete', $md5]]) !!}
                                <input type="submit" class="button alert" value="Delete">
                                {!! Form::close() !!}
                            </td>
                        </tr>
                    @endforeach
                    </tbody>
                </table>

            </div>
        </div>
    @endif
@endsection