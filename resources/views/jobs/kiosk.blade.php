@php
    $jobs = \App\Repositories\JobRepository::getJobs();
@endphp

<div class="row">
    <div class="column">
        <h1>Jobs</h1>
        <p>Rendered at: {{Carbon\Carbon::now()->toDateTimeString()}}</p>
        <table class="table">
            <thead>
            <tr>
                <th>ID</th>
                <th>Queue</th>
                <th>Attemps</th>
                <th>Reserved</th>
                <th>Reserved at</th>
                <th>Available at</th>
                <th>Created at</th>
            </tr>
            </thead>
            <tbody>
            @foreach($jobs as $job)
                <tr<?php if(\Carbon\Carbon::createFromTimestamp($job->available_at)->diffInSeconds(Carbon\Carbon::now(), false) > 0) { echo " style='background-color: rgba(255, 0, 0, 0.2);'";} else { echo " style='background-color: rgba(0, 255, 0, 0.2);";}?>>
                    <td>{{$job->id}}</td>
                    <td>{{$job->queue}}</td>
                    <td>{{$job->attempts}}</td>
                    <td>{{$job->reserved}}</td>
                    <td>{{$job->reserved_at ? Carbon\Carbon::createFromTimestamp($job->reserved_at)->toDateTimeString() : '-'}}</td>
                    <td>{{Carbon\Carbon::createFromTimestamp($job->available_at)->toDateTimeString()}}</td>
                    <td>{{Carbon\Carbon::createFromTimestamp($job->created_at)->toDateTimeString()}}</td>
                </tr>
            @endforeach
            </tbody>
        </table>
    </div>
</div>