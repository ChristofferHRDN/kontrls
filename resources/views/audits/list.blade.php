@extends('spark::layouts.app')

@section('content')
    @include('domains.domain.menu')
    <div class="row">
        <div class="columns">
            <div class="callout">
                <h1>Audits</h1>
                @if (session('message'))
                    <div class="alert alert-success" data-dismiss="alert">
                        <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                            <span aria-hidden="true">&times;</span>
                        </button>

                        {{ session('message') }}
                    </div>
                @endif
                @if (session('error'))
                    <div class="alert alert-danger">
                        <strong>Oh, no! There was an error.</strong>
                        <p>{{ session('error') }}</p>
                    </div>
                @endif
                @if($domain->hasAuditRunning())
                    <p>You already have an audit running.</p>
                @else
                    @if($domain->audit_url)
                        @if($domain->verified)
                        <p>You do not seem to have any audit running. Click the button below to start one.</p>
                        <p>
                            <a href="{{route('domain.audit.run', [$domain])}}" class="button">Start an audit</a>
                        </p>
                        @else
                            <div class="alert alert-warning">
                                <strong>Verify your domain first.</strong>
                                <p>You must verify your domain before you can audit it. You do that in the <a
                                        href="{{route('domain.settings', $domain)}}">settings</a>.</p>
                            </div>
                        @endif
                    @else
                        <p>
                        <span>You haven't added an audit url yet. You can't audit this domain before you do that. <a
                                    href="{{route('domain.settings', $domain)}}">Go to domain settings</a></span>
                        </p><p>After you have added the audit url, you need to verify the domain, which you can also do from the domain settings page. Then you can begin
                            auditing.
                        </p>
                    @endif
                @endif
                @if(count($audits))
                    <div>
                        <p>Below are the available audits listed. They are sorted in reverse chronological ordering,
                            with the first audit last, and the last audit first.</p>
                        <table>
                            <thead>
                            <tr>
                                <td></td>
                                <td>Started</td>
                                <td>Finished</td>
                                <td>Status</td>
                                <td>Initiated by</td>
                                <td></td>
                            </tr>
                            </thead>
                            <tbody>
                            @foreach($audits->reverse() as $audit)
                                <tr>
                                    <td><a href="{{route('domain.audit.show', [$audit->domain, $audit])}}">Audit</a>
                                    </td>
                                    <td>{{$audit->started_audit}}</td>
                                    <td>{{$audit->stopped_audit}}</td>
                                    <td>{{ $audit->running ? "Running": ($audit->finished? "Finished" : "Not finished for some reason")}}</td>
                                    <td>{{ucwords($audit->initiated_by)}}</td>
                                    <td>
                                        <form action="{{ route('domain.audit.delete', [$audit->domain, $audit]) }}"
                                              method="POST">
                                            {!! csrf_field() !!}
                                            {!! method_field('DELETE') !!}

                                            <button type="submit" class="btn btn-danger">
                                                <i class="fa fa-trash-o"></i> Delete
                                            </button>
                                        </form>
                                    </td>
                                </tr>
                            @endforeach
                            </tbody>
                        </table>

                    </div>
                @endif
            </div>
        </div>
    </div>
@endsection