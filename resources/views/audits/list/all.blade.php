@extends('spark::layouts.app')

@section('content')
    <div class="row">
        <div class="small-12 columns callout">
            <h1>All audits</h1>
            @if(count($audits))
                <table>
                    <thead>
                    <tr>
                        <td></td>
                        <td>Started</td>
                        <td>Finished</td>
                        <td>Status</td>
                    </tr>
                    </thead>
                    <tbody>
                    @foreach($audits as $audit)
                        <tr>
                            <td><a href="{{route('domain.audit.show', [$audit->domain, $audit])}}">Audit of {{$audit->domain->name}}</a></td>
                            <td>{{$audit->started_audit}}</td>
                            <td>{{$audit->stopped_audit}}</td>
                            <td>{{ $audit->running ? "Running": ($audit->finished? "Finished" : "Not finished for some reason")}}</td>
                        </tr>

                    @endforeach
                    </tbody>
                </table>
            @endif
        </div>
    </div>
@endsection