@extends('spark::layouts.app')

@section('content')
    <div class="row">
        <div class="small-12 columns callout">
            <h1>Audit information</h1>
            {{$message}}
            <a href="{{route('domain.audit.show', [$audit->domain, $audit])}}">Audit</a>
        </div>
    </div>
@endsection