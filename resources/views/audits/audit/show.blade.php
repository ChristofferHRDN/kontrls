@extends('spark::layouts.app')
@section('scripts')
    <script src="/js/prism.js"></script>
    <link rel="stylesheet" href="/css/prism.css">
@endsection
@section('content')
    @include('domains.domain.menu', ['domain' => $audit->domain])
    @if(session('status'))
        <div class="row">
            <div class="alert alert-success">
                <strong>Success!</strong> {{session('status')}}
            </div>
        </div>
    @endif
    <div class="row">
        <audit-info :auditid="'{{$audit->hashid}}'" :domainid="'{{$audit->domain->hashid}}'"
                    :audit="{{json_encode($audit)}}" :done="'{{$audit->getStatus()}}'">
            <div slot="header">
                <div><h1>Audit</h1></div>
                @if (session('message'))
                    <div class="alert alert-success">
                        {{ session('message') }}
                    </div>
                @endif
            </div>
            <div slot="buttons" style="display: inline-block;">
                <button type="button" class="btn btn-danger" role="button" data-toggle="collapse" href="#collapseClear"
                        aria-expanded="false" aria-controls="collapseClear">
                    Delete audit
                </button>
                @if($audit->running && !$audit->stopped)
                    <button type="button" class="btn btn-warning" role="button" data-toggle="collapse"
                            href="#collapseStop"
                            aria-expanded="false" aria-controls="collapseStop">
                        Stop audit
                    </button>
                @endif
            </div>
            <div>
                <div class="collapse" id="collapseClear">
                    <div class="well">
                        <p>Are you sure you want to remove this audit? The audit will be permanently deleted.</p>
                        {!! Form::open(['method' => 'delete', 'route' => ['domain.audit.delete', $audit->domain, $audit]]) !!}
                        <button type="submit" class="btn btn-danger">
                            <i class="fa fa-trash-o"></i> Delete
                        </button>
                        {!! Form::close() !!}
                    </div>
                </div>
                @if($audit->running && !$audit->stopped)
                    <div class="collapse" id="collapseStop">
                        <div class="well">
                            <p>Are you sure you want to stop this audit? When you have stopped an audit, there is no way
                                to resume it.</p>
                            {!! Form::open(['method' => 'post', 'route' => ['domain.audit.stop', $audit->domain, $audit]]) !!}
                            <button type="submit" class="btn btn-danger">
                                <i class="fa fa-trash-o"></i> Stop
                            </button>
                            {!! Form::close() !!}
                        </div>
                    </div>
                @endif
            </div>
        </audit-info>
    </div>
    <div class="row">
        @if(count($audit->files))
            <div id="files" class="columns callout">
                <h3>Scanned files</h3>
                <div>
                    @if($audit->running)
                        <p>Scanned {{$audit->files->count()}} files of {{$audit->interesting_filecount}} so far.</p>
                        <p>To update the scanned files list, you need to reload the page.</p>
                    @else
                        <p>Scanned {{$audit->files->count()}} files.</p>
                    @endif
                </div>
                <fieldset class="fieldset">
                    <legend style="width: auto;">Filters</legend>
                    <a class="btn btn-primary<?php if ($filefilter !== 'w'):echo ' active';endif;?>" href="?f=a"
                       style="border-radius: 0;">All files ({{$audit->files->count()}})</a>
                    <a class="btn btn-primary<?php if ($filefilter === 'w'):echo ' active';endif;?>" href="?f=w"
                       style="border-radius: 0;">Only detected files
                        ({{$audit->files()->where('weight', '>', 0)->where('caught', 1)->count()}})</a>
                </fieldset>
                {!! $files->links() !!}
                <ul class="accordion" data-accordion data-allow-all-closed="true" data-accordion
                    data-multi-expand="true">
                    @foreach($files as $index => $file)
                        <?php $problems = json_decode($file->lines); ?>
                        <?php $explanations = json_decode($file->explanations); ?>
                        <?php $problemCount = count($problems); ?>
                        <?php if (count($explanations) > $problemCount) {
                            $problemCount = count($explanations);
                        }?>
                        <?php $averageWeightPerProblem = round($file->weight / max(1, $problemCount)); ?>
                        <li class="accordion-item" data-accordion-item>
                            <a href="#" class="accordion-title">{{$file->filename}} <span
                                        style="position: absolute; right: 3em;">{{$file->weight}}</span></a>
                            <div class="accordion-content" data-tab-content>
                                <div class="row">
                                    <div class="small-6 columns">
                                        <p><strong>Filename:</strong> {{$file->filename}}</p>
                                        <p>
                                            <a class="button secondary"
                                               href="{{route('domain.file.get-content', [$audit->domain, $audit, 'f' => $file->filename])}}">See
                                                content</a></p>
                                    </div>
                                    @if($file->weight)
                                        <div class="small-6 columns">
                                            <p><strong>Weight:</strong> {{$file->weight}}</p>
                                            <p><strong>Problems:</strong> {{$problemCount}}</p>
                                            <p><strong>Average weight:</strong> {{$averageWeightPerProblem}}</p>
                                            <p><strong>MD5:</strong> {{$file->md5}}</p>
                                            <div class="row">
                                                <div class="columns"><strong>Whitelisted:</strong></div>
                                                @if(!in_array($file->md5, $audit->domain->user->allowedHashes()))
                                                    <div class="small-10 columns">
                                                        <form method="POST" action="{{route('whitelist.store')}}">
                                                            {{csrf_field()}}
                                                            <input type="submit" value="Add to whitelist">
                                                            <input type="hidden" value="{{$file->md5}}" name="md5">
                                                            <input type="hidden"
                                                                   value="{{route('domain.audit.show', [$audit->domain, $audit])}}"
                                                                   name="next">
                                                        </form>
                                                    </div>
                                                @else
                                                    <div class="small-10 columns">
                                                    Whitelisted
                                                    </div>
                                                @endif
                                            </div>
                                        </div>
                                    @endif
                                </div>
                                <ul class="accordion" data-accordion data-allow-all-closed="true">
                                    <li class="accordion-item" data-accordion-item>
                                        <a href="#" class="accordion-title">File stats</a>
                                        <div class="accordion-content" data-tab-content>
                                            <pre><code class="language-json">{{json_encode(json_decode($file->stats), JSON_PRETTY_PRINT)}}</code></pre>
                                        </div>
                                    </li>
                                </ul>
                                @if(count($explanations))
                                    <h4>Explanations</h4>
                                    <ul>
                                        @foreach($explanations as $explanation)
                                            <li>{{$explanation}}</li>
                                        @endforeach
                                    </ul>
                                @endif
                                @if(count($problems))
                                    <h4>Problems: {{count($problems)}}</h4>
                                    <div>
                                        @foreach($problems as $line)
                                            <div>Line: {{$line->lineno}}. <span>Message: {{$line->message}}
                                                    .  Weight of {{$line->weight}}</span></div>
                                            <div></div>
                                            <pre><code class="language-php">{{$line->line_content or ''}}</code></pre>
                                        @endforeach
                                    </div>
                                @endif
                            </div>
                        </li>
                    @endforeach
                </ul>
                {!! $files->links() !!}
            </div>
        @endif
    </div>
@endsection
@section('lastscripts')
    <script>
        jQuery(function () {
            jQuery('[data-toggle="tooltip"]').tooltip()
        });
    </script>
@endsection