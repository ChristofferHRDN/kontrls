@extends('spark::layouts.app')
@section('scripts')
    <script src="/js/prism.js"></script>
    <link rel="stylesheet" href="/css/prism.css">
@endsection
@section('content')
    @include('domains.domain.menu', ['domain' => $audit->domain])
    <div class="row file-content">
        <div class="columns callout">
            <div class="columns">
                <h2 class="filename">File content of {{$filename}}</h2>
                <p><a href="{{route('domain.audit.show', [$audit->domain, $audit])}}">Back to audit</a></p>
            </div>
            @if(empty($result['error']))
                <div class="small-12 columns">
                    <pre class="line-numbers"><code class="language-php">{{$file}}</code></pre>
                </div>
            @else
                <div class="small-12 columns">
                    There was an error fetching the file: <strong>{{$result['error']}}</strong>
                </div>
            @endif
        </div>
    </div>
@endsection