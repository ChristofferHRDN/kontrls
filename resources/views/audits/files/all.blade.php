@extends('spark::layouts.app')
@section('content')
    <div class="row">
        <div class="small-12 columns callout">
            <h1>All files</h1>
            <p><a href="{{route('domain.audit.show', [$audit->domain, $audit])}}">Back to audit</a></p>
            <p>These files are not cached on the server, but are instead fetched dynamically every time you visit the
                page.</p>
            @if(count($files))
                <ul class="accordion" data-accordion data-allow-all-closed="true">
                    @foreach($files->sortBy('filename', SORT_NATURAL|SORT_FLAG_CASE) as $structure)
                        <li class="accordion-item" data-accordion-item>
                            <a href="#" class="accordion-title">{{$structure->filename}}</a>
                            <div class="accordion-content" data-tab-content>
                                <a href="{{route('domain.file.get-content', [$audit->domain, $audit, 'f' => $structure->filename])}}">See
                                    content</a>
                            </div>
                        </li>
                    @endforeach
                </ul>
                {!! $files->links() !!}
            @else
                <p>There are no files in this audit yet.</p>
            @endif
        </div>
@endsection