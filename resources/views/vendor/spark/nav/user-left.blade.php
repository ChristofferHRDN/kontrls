<!-- Left Side Of Navbar -->
<li>
    <a href="{{ route('domains.list') }}">Domains</a>
</li>
<li>
    <a href="{{ route('groups.list') }}">Groups</a>
</li>
<li>
    <a href="{{ route('alerts.list') }}">Alerts</a>
</li>
<li>
    <a href="{{ route('whitelist.index') }}">Whitelist</a>
</li>