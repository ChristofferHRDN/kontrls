@extends('spark::layouts.app')

@section('content')
	@include('domains.domain.menu')
	@if(session('status'))
		<div class="row">
			<div class="alert alert-success">
				{{session('status')}}
			</div>
		</div>
	@endif
	<div class="row">
		<div class="small-12 columns">
			<div class="callout">
				<table>
					@foreach($backups as $backup)
						<tr>
							<td>{{$backup->initiated_by}}</td>
							<td>{{$backup->stopped_at}}</td>
						</tr>
					@endforeach
				</table>
				@if(count($logs))
					<table>
						@foreach($logs as $log)
							<tr>{{json_decode($log->log)->message}}</tr>
						@endforeach
					</table>
				@endif
				@if($domain->canStartBackup())
					{!! Form::open(['method' => 'post', 'route' => ['domain.backup.start', $domain]]) !!}
					<button type="submit" class="button">
						Backup this domain
					</button>
					{!! Form::close() !!}
				@else
					@if($domain->hasBackupRunning())
						<p>You already have a backup running. Wait until it is finished and then start a new one.</p>
					@elseif($domain->has_backup_queued)
						<p>You have a backup queued already. Wait until it finishes.</p>
					@endif
					@if(!$domain->getSetting('backup_secret') || !$domain->getSetting('is_joomla_site'))
						<p>You haven't configured this domain correctly yet. You need to add the backup secret and check
							the "Is Joomla site" box before
							you can start a backup. </p>
						<p>Go to the <a href="{{route('domain.settings', $domain)}}">domain
								settings</a> and change it to be able to run a backup.</p>
					@elseif(!$domain->verified_backup)
						<p>You haven't verified the backup domain yet. You must do that before you can start doing
							backups.</p>
						{!! Form::open(['method' => 'post', 'route' => ['domain.backup.verify', $domain]]) !!}
						<button type="submit" class="button">
							Verify
						</button>
						{!! Form::close() !!}

					@endif
				@endif
			</div>
		</div>
	</div>
@endsection