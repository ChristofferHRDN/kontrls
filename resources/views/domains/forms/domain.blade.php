{{--<form action="{{ route('domain.store') }}" method="POST">
    {!! csrf_field() !!}
    <div class="input-group">
        <input type="url" name="url" id="url" class="input-group-field" autofocus placeholder="Domain URL to the live site">
        <input type="text" name="name" id="name" class="input-group-field" placeholder="Domain name. Leave empty to get default name">
        <div class="input-group-button">
            <button type="submit" class="btn btn-default" style="font-size:14px;">
                <i class="fa fa-plus"></i> Add Domain
            </button>
        </div>
    </div>
</form>
--}}

<domain-form :route="'{{route('domain.store')}}'"></domain-form>