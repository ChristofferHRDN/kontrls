@extends('spark::layouts.app')
@section('scripts')
    <script src="https://code.jquery.com/jquery-2.2.3.min.js"></script>
@endsection
@section('content')
    <domain :domainid="'{{$domain->hashid}}'" :is_queued="{{$domain->isQueued()}}" :group="{{$domain->group}}"
            inline-template>
        @include('domains.domain.menu')
        <div class="row">
            <div class="small-12 columns">
                <div class="callout">
                    <div class="panel-heading bg-transparent"><h4>Domain</h4></div>
                    <div class="panel-body">
                        <h1><a href="{{$domain->url}}">{{ $domain->name }}</a></h1>
                        <div><strong>Url:</strong> <span>{{$domain->url}}</span></div>
                        @if(count($domain->urls))<p><strong>Queued for monitoring:</strong> @{{ is_queued ? 'Yes' : 'No' }} <a @click="queue"
                            >@{{ is_queued_text }}</a>
                        </p>@endif
                        <div>
                            <div v-if="group" class="row align-middle">
                                <div class="column">Belongs to <a
                                            href="/groups/@{{ group.hashid }}">@{{ group.name }}</a></div>
                                <div class="column">
                                    <button @click.prevent="removeGroup" class="btn btn-alert small"
                                            value="Remove from group">
                                        Remove group
                                    </button>
                                </div>
                            </div>
                            <div v-else v-if="groups.length">
                                {!! Form::open(['method' => 'post', 'route' => ['domain.group.add', $domain]]) !!}
                                <div class="input-group">
                                    <select name="group_id" v-model="group_id" class="input-group-field">
                                        <option v-for="group in groups" :value="group.id">
                                            @{{ group.name }}
                                        </option>
                                    </select>
                                    <div class="input-group-button">
                                        <button @click.prevent="addGroup" class="button success">Add to group
                                        </button>
                                    </div>
                                </div>

                                {!! Form::close() !!}
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div class="row">
            <div class="small-12 columns">
                <div class="callout">
                    <div class="panel-heading"><h4 id="urls">Urls</h4></div>
                    <div class="panel-body">
                        <p>Here you add the urls that are related to your domain. The urls are used in the
                            monitors to check for uptime or other metrics.</p>
                        @if(count($domain->urls) == 0)<p>After you've added your first URL you can add monitors and
                            more.
                        </p>@endif
                        {!! Form::open(['method' => 'post', 'route' => ['url.store', $domain]]) !!}
                        <div class="input-group">
                            {!! Form::text('url', '', ['placeholder' => 'Url', 'class' => 'input-group-field']) !!}
                            <div class="input-group-button">
                                {!! Form::submit('Add url', ['class' => 'button success']) !!}
                            </div>
                        </div>
                        <p class="help-text">A url has some connection to your site.</p>
                        {!! Form::close() !!}

                        @if(count($domain->urls))
                            <hr>
                            <h4>Current urls</h4>
                            <table>
                                <tbody>
                                @foreach($domain->urls as $url)
                                    <tr>
                                        <td>
                                            {!! Form::open(['method' => 'post', 'route' => ['url.update', $domain, $url]]) !!}
                                            <div class="row" style="margin-top: 15px;">
                                                <div class="small-8 columns">
                                                    <input type="text" value="{{$url->url}}" name="url">
                                                </div>
                                                <div class="small-4 columns">
                                                    <button type="submit" value="Save" class="btn small">Save</button>
                                                </div>
                                            </div>
                                            {!! Form::close() !!}

                                        </td>
                                        <td>{!! Form::open(['method' => 'delete', 'route' => ['url.destroy', $domain, $url]]) !!}
                                            <button type="submit" class="btn small">Remove url</button>
                                            {!! Form::close() !!}</td>
                                    </tr>
                                @endforeach
                                </tbody>
                            </table>
                        @endif
                    </div>
                </div>
            </div>
        </div>
        @if(count($domain->urls))
            <div class="row">
                <div class="small-12 columns">
                    <div class="callout">
                        <div class="panel-heading"><h4 id="monitors">Monitors</h4></div>
                        <div class="panel-body">
                            @if (count($errors) > 0)
                                <div class="alert alert-danger">
                                    <ul>
                                        @foreach ($errors->all() as $error)
                                            <li>{{ $error }}</li>
                                        @endforeach
                                    </ul>
                                </div>
                            @endif
                            <div v-show="editid == 0">
                                {!! Form::open(['method' => 'post', 'route' => ['domain.monitors.store', $domain]]) !!}

                                {!! Form::label('url', 'Url') !!}
                                {!! Form::select('url', $urls) !!}

                                {!! Form::label('check', 'Check type') !!}
                                {!! Form::select('check', $check_types) !!}

                                {!! Form::label('check_value', 'Checking conditions') !!}
                                {!! Form::text('check_value', old('check_value'), ['placeholder' => 'Checking conditions']) !!}

                                {!! Form::label('expected_value', 'Expected value') !!}
                                {!! Form::text('expected_value', old('expected_value'), ['placeholder' => 'Expected value']) !!}
                                <div>
                                    {!! Form::labelCheckbox('is_negative_check', 'false', 'Is this check a negative match?') !!}
                                </div>

                                {!! Form::submit('Add monitor', ['class' => 'button success']) !!}

                                {!! Form::close() !!}
                            </div>
                            <div v-for="monitor in domain.monitors" v-show="editid == monitor.id">
                                <label>Url
                                    <input type="text" v-model="form.url" name="url"
                                           value="@{{ monitor.url.url }}">
                                </label>

                                {{--{!! Form::text('url', old('url', $monitor->url), ['placeholder' => 'URL']) !!}--}}
                                <label>Check type
                                    <select name="check" v-model="form.check">
                                        @foreach($check_types as $key => $val)
                                            <option value="{{$key}}"
                                                    :selected="monitor.check_type_id == {{$key}}">{{$val}}</option>
                                        @endforeach
                                    </select>
                                </label>
                                <label>@{{ getMonitorEditLabel('check_value', monitor) }}
                                    <input v-show="shouldShow('check_value', monitor)" type="text"
                                           name="check_value" v-model="form.check_value"
                                           value="@{{ checkValue(monitor) }}" placeholder="Checking conditions">
                                </label>
                                <label>@{{ getMonitorEditLabel('expected_value', monitor) }}
                                    <input v-show="shouldShow('expected_value', monitor)" type="text"
                                           name="expected_value" v-model="form.expected_value"
                                           value="@{{ expected(monitor) }}" placeholder="Expected value">
                                </label>

                                <div>
                                    <label>
                                        <input type="checkbox" name="is_negative_check" value="on"
                                               v-model="form.should_fail" :checked="shouldFail(monitor)">
                                        @{{ getMonitorEditLabel('should_fail', monitor) }}</label>

                                </div>
                                <button type="submit" class="button success"
                                        @click.prevent="saveEditMonitor(monitor.id)">Edit
                                </button>
                                <button class="button alert" @click.prevent="cancelEdit">Cancel</button>
                            </div>
                            <template v-if="domain.monitors.length">
                                <hr>

                                <h4>Current monitors</h4>
                                <table>
                                    <thead>
                                    <tr>
                                        <th>Url</th>
                                        <th>Check type</th>
                                        <th>Check value</th>
                                        <th>Expected value</th>
                                        <th>Enabled</th>
                                        <th></th>
                                        <th></th>
                                    </tr>
                                    </thead>
                                    <tbody>
                                    <tr v-for="monitor in domain.monitors">
                                        <td>@{{monitor.url.url}}</td>
                                        <td>@{{monitor.check_type ? monitor.check_type.name : "No name for the given check type"}}</td>
                                        <td>@{{checkValue(monitor) ? checkValue(monitor) : "-"}}</td>
                                        <td>@{{expected(monitor) ? expected(monitor) : "-"}}</td>
                                        <td @click="toggleMonitor(monitor.id)"
                                        >@{{ monitor.enabled == 1 ? "Yes" : "No" }}</td>
                                        <td>
                                            <button @click="deleteMonitor(monitor.id)">
                                            <i class="fa fa-trash-o"></i>
                                            </button>
                                        </td>
                                        <td>
                                            <button @click="editMonitor(monitor)">Edit</button></td>
                                        </td>
                                    </tr>
                                    </tbody>
                                </table>
                            </template>
                        </div>
                    </div>
                </div>
            </div>
        @endif
    </domain>
@endsection