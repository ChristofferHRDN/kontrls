<?php $routes = [
		[
				'route' => 'domain.show',
				'icon' => 'fa fa-home',
				'name' => 'Domain',
				'weight' => 10,
		],
		[
				'route' => 'domain.show',
				'icon' => 'fa fa-underline',
				'name' => 'Urls',
				'extra' => 'urls',
				'weight' => 20,
		],
		[
				'route' => 'domain.settings',
				'icon' => 'fa fa-cog',
				'name' => 'Settings',
				'weight' => 30,
		],
		[
				'route' => 'domain.notes',
				'icon' => 'fa fa-sticky-note-o',
				'name' => 'Notes',
				'weight' => 40,
		],
		[
				'route' => 'domain.logs',
				'icon' => 'fa fa-area-chart',
				'name' => 'Logs',
				'weight' => 50,
		],
		[
				'route' => 'domain.uptime',
				'icon' => 'fa fa-area-chart',
				'name' => 'Uptime',
				'weight' => 60,
		],
		[
				'route' => 'domain.alerts.list',
				'icon' => 'fa fa-bell',
				'name' => 'Alerts',
				'weight' => 70,
		],
		[
				'route' => 'domain.audits.list',
				'icon' => 'fa fa-search',
				'name' => 'Audits',
				'weight' => 80,
		],
		[
				'route' => 'domain.backup.list',
				'icon' => 'fa fa-search',
				'name' => 'Backups',
				'weight' => 90,
		],
];?>
<?php if (count($domain->urls) > 0) {
	$routes[] = ['route' => 'domain.show',
			'icon' => 'fa fa-television',
			'name' => 'Monitors',
			'extra' => 'monitors',
			'weight' => 25,];
}?>
<div class="row">
	<div class="small-12 columns">
		<div class="columns callout">
			<h1>{{$domain->name}}</h1>
			<ul class="menu icon-top vertical large-horizontal menu">
				@foreach(collect($routes)->sortBy('weight') as $route)
					<li<?php if (Route::currentRouteName() === $route['route']):echo ' class="active"'; endif;?>><a
								href="{{route($route['route'], $domain)}}<?php if (array_get($route, 'extra')):echo '#' .
										$route['extra']; endif;?>"><i class="{{$route['icon']}}"></i> {{$route['name']}}
						</a>
					</li>
				@endforeach
			</ul>
		</div>
	</div>
</div>