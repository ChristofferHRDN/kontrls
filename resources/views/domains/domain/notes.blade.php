@extends('spark::layouts.app')

@section('content')
    @include('domains.domain.menu')
    <div class="row">
        <div class="small-12 columns">
            <div class="callout">
                <div class="panel-heading"><h4>Notes</h4></div>
                <div class="panel-body">
                    @if($note->id)
                        <p>You are currently editing a note. Save the note before you try to create a new one.</p>
                        <div style="position: absolute; top: 0; right: 0;">
                            {!! Form::open(['method' => 'delete', 'route' => ['domain.notes.destroy', $domain, $note]]) !!}
                            {!! Form::submit('Destroy note', ['class' => 'button alert small']) !!}
                            {!! Form::close() !!}
                        </div>
                    @endif
                    {!! Form::open(['method' => 'post', 'route' => ['domain.notes.store', $domain]]) !!}
                    <input type="hidden" name="noteid" value="{{$note->id}}">
                    <textarea name="note" rows="10">{{$note->note}}</textarea>
                    <input type="submit" class="button success"
                           value="{{ $note->id == "" ? "Add note" : "Save note" }}">
                    @if($note->id)
                        <button type="button" class="button secondary">
                            <a href="./" style="color: white;">Cancel</a>
                        </button>
                    @endif
                    {!! Form::close() !!}

                </div>
            </div>
            <?php $count = count($domain->notes);
            $count = $note->id ? $count - 1 : $count;?>
            @if($count)
                <?php $outerNote = $note; ?>
                @foreach($domain->notes as $note)
                    <?php if ($outerNote->id === $note->id): continue; endif;?>
                    <div class="callout">

                        <div>{!! Markdown::parse($note->note) !!}</div>
                        <div>
                            <small>{{ $note->created_at }}</small>
                            <a href="{{route('domain.notes.edit', [$domain, $note])}}">Edit note</a>
                        </div>
                    </div>
                @endforeach
            @endif
        </div>
    </div>
@endsection