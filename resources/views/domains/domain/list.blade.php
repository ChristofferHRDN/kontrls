<div class="row">
    <div class="small-12 columns">
        <div class="callout">
            <div class="box-header">
                <!-- tools box -->
                <div class="pull-right box-tools">
                    <span class="box-btn" data-widget="collapse"><i class="icon-minus"></i>
                    </span>
                </div>
                <h3 class="box-title"><i class="icon-graph-pie"></i>
                    <span>Domains</span>
                </h3>

            </div>
            <div class="box-body">
                <!-- Bootstrap Boilerplate... -->
                @if(count($domains) <= Auth::user()->sparkPlan()->attribute('max_sites'))
                <div class="panel-body">
                    <!-- Display Validation Errors -->
                @include('common.errors')

                <!-- New Domain Form -->
                @include('domains.forms.domain')
                </div>
                @endif
                <div class="domains">
                    @if(count($domains) > 0)
                        <div class="domains-wrapper">
                            <div class="panel-heading">
                                @if(!isset($limited) || !$limited)
                                Number of domains: {{ count($domains) }}
                                @else
                                    A selection of domains. To seem them all go to the <a href="{{route('domains.list')}}">full listing</a>.
                                @endif
                            </div>
                            <table class="small-12 columns" style="padding: 0;">
                                <thead>
                                <tr>
                                    <th>Name</th>
                                    <th>URL</th>
                                    <th>Group</th>
                                    <th>Notes</th>
                                    <th>Queued</th>
                                    <th>Audits</th>
                                    <th></th>
                                </tr>
                                </thead>
                                <tbody>
                                @foreach($domains as $domain)
                                    <tr class="hide-child-until-hover">
                                        <td>
                                            <a href="{{ route('domain.show', $domain->hashid) }}">{{ $domain->name ? $domain->name : $domain->url }}</a>
                                        </td>
                                        <td>{{ $domain->url }}</td>
                                        <td>
                                            @if($domain->group)
                                                <a href="{{ route('group.show', $domain->group->id) }}">{{$domain->group->name}}</a>
                                            @else
                                                No group assigned
                                            @endif
                                        </td>
                                        <td>{{ count($domain->notes) }}</td>
                                        <td>{{ $domain->isQueued() == true ? "Queued" : "Not queued"}}</td>
                                        <td>{{ $domain->audits()->count() }}</td>
                                        <td>{!! Form::open(['method' => 'delete', 'route' => ['domain.destroy', $domain], 'class' => 'child']) !!}
                                            <button type="submit" class="btn btn-danger"><i class="fa fa-trash-o"></i></button>
                                            {!! Form::close() !!}
                                        </td>
                                    </tr>
                                @endforeach
                                </tbody>
                            </table>
                        </div>
                    @endif
                </div>
            </div>
        </div>
    </div>
</div>