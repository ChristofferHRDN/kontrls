@extends('spark::layouts.app')

@section('content')
	@include('domains.domain.menu')
	<div class="row">
		<div class="small-12 columns">
			@if(session('verifymessage'))
			<div class="alert alert-success">
					{{session('verifymessage')}}
				</div>
			@endif
				@if(session('error'))
					<div class="alert alert-danger">
						<strong>Oh no, there was an error!</strong>
						<p>{{session('error')}}</p>
					</div>
				@endif
			<div class="callout collapsed-box">
				<div class="panel-heading bg-transparent" data-widget="collapse">
					<!-- tools box -->
					<div class="pull-right box-tools">
                                    <span class="box-btn"><i class="icon-minus"></i>
                                    </span>
					</div>
					<h4 class="box-title"><i class="fa fa-pie-chart"></i>
						<span>Change domain</span>
					</h4>
				</div>
				<!-- /.box-header -->
				<div class="panel panel-body" id="domaintarget">
					<div style="margin-bottom: 20px;">
						<h4>Domain</h4>
						@include('common.errors')
						<form class="form-horizontal" method="POST"
							  action="{{ route('domain.update', $domain) }}">
							{!! csrf_field() !!}
							<div class="row">
								<div class="small-12 columns">
									<label>Name
										<input type="text" id="name" name="name"
											   value="{{ $domain->name }}">
									</label>
								</div>
							</div>
							<div class="row">
								<div class="small-12 columns">
									<label>URL
										<input type="url" id="url" name="url"
											   value="{{ $domain->url }}">
									</label>
								</div>
							</div>
							<div class="row">
								<div class="small-12 columns">
									<label>
										Audit url
										<input type="text" id="audit_url" name="audit_url"
											   value="{{$domain->audit_url}}">
										<p class="help-text">This must point to the actual file. If you visit this
											domain, you should get the message "That is not a command", and nothing
											else.</p>
									</label>
								</div>
							</div>
							<input type="submit" value="Save" class="button success">
						</form>
					</div>

					<hr>
					@if(!$domain->isVerified())
						<form action="{{route('domain.verify', $domain)}}" method="POST">
							{{csrf_field()}}
							@endif
							<p>Remember to change the "Audit url" setting above so that it points to the correct domain,
								when you have saved the file on your server.</p>
							<a href="{{route('domain.sync-file', $domain)}}?download=1" class="button">Download the sync
								file</a>
							@if(!$domain->isVerified())
								@if($domain->audit_url)
									<button type="submit" class="button">
										Verify domain
									</button>
								@endif
							@endif
						</form>
						<hr>
						<div>
							<h4>Settings</h4>
							<form class="form-horizontal" method="POST"
								  action="{{ route('domain.settings.update', $domain) }}">
								{!! csrf_field() !!}
								<div class="row">
									<div class="small-12 columns">
										<label>Poll time (Every n-th minute)
											<input type="number" min="0" id="polltime" name="polltime"
												   value="{{  $domain->getSetting("polltime")}}">
										</label>
										@if($domain->group)
											<p>This domains group
												polltime: {{$domain->group->getSetting("polltime")}}</p>
											<p>The lowest value will take priority.</p>
										@endif
									</div>
									<div class="small-12 columns">
										<label>User-agent. Leave blank to use the default user-agent string
											<input type="text" name="useragent"
												   value="{{trim($domain->getSetting("useragent"))}}"
												   placeholder="Defaults to: 'Kntrl.io Ping Job'">
										</label>
									</div>
									<div class="small-12 columns">
										<label>Time in minutes between each alert sent
											<input type="number" min="1" name="time_between_alerts"
												   value="{{$domain->getSetting('time_between_alerts')}}">
										</label>
									</div>
									<div class="small-12 columns">
										<label>Minimum weight that is required before an alert is send for a file
											<input type="number" min="1" name="minimum_audit_alert_weight"
												   placeholder="10000"
												   value="{{$domain->getSetting('minimum_audit_alert_weight')}}">
										</label>
									</div>
									<div class="small-12 columns">
										<?php
										if ($domain->scheduledAudit) {
											if (!$domain->scheduledAudit->enabled) {
												$selected = -1;
											} else {
												$selected = $domain->scheduledAudit->wait_time;
											}
										} else {
											$selected = -1;
										}
										?>
										<label>Automatic audits
											<select name="automatic_audit_wait_time">
												<option value="-1" @if($selected === -1)selected="selected"@endif>
													Never
												</option>
												<option value="3600" @if($selected === 3600)selected="selected"@endif>
													Hourly
												</option>
												<option value="86400" @if($selected === 86400)selected="selected"@endif>
													Daily
												</option>
												<option value="604800"
														@if($selected === 604800)selected="selected"@endif>Weekly
												</option>
												<option value="2592000"
														@if($selected === 2592000)selected="selected"@endif>Monthly
												</option>
											</select>
										</label>

										<hr>
									</div>
									<h4 class="small-12 columns">Backups</h4>
									<p class="small-12 columns">To use backups, you will first need to check the box Is this a Joomla
											site
											and
											save. Then add the Akeeba secret and save, and then configure the backup
											interval.</p>
									<div class="small-12 columns">
										<label>Is this a Joomla site?
											<input type="checkbox" name="isjoomlasite"
												   @if($domain->getSetting('isjoomlasite'))checked="checked" @endif
												   value="yes">
										</label>
									</div>
									@if($domain->getSetting('isjoomlasite'))
										<div class="small-12 columns">
											<label>Akeeba Backup secret
												<input type="text" name="backup_secret"
													   value="{{trim($domain->getSetting('backup_secret'))}}"
													   placeholder="">
											</label>
											<p class="help-text">The secret that is needed to communicate to Akeeba.
												You
												also need to allow for front-end backups in the Akeeba settings.</p>
										</div>
										@if($domain->getSetting('backup_secret'))
											<div class="small-12 columns">
												<?php
												if ($domain->scheduledBackup) {
													if (!$domain->scheduledBackup->enabled) {
														$selected = -1;
													} else {
														$selected = $domain->scheduledBackup->wait_time;
													}
												} else {
													$selected = -1;
												}
												?>
												<label>Automatic backups
													<select name="automatic_backup_wait_time">
														<option value="-1"
																@if($selected === -1)selected="selected"@endif>
															Never
														</option>
														<option value="3600"
																@if($selected === 3600)selected="selected"@endif>
															Hourly
														</option>
														<option value="86400"
																@if($selected === 86400)selected="selected"@endif>
															Daily
														</option>
														<option value="604800"
																@if($selected === 604800)selected="selected"@endif>
															Weekly
														</option>
														<option value="2592000"
																@if($selected === 2592000)selected="selected"@endif>
															Monthly
														</option>
													</select>
												</label>
												<p class="help-text">Backups will be taken periodically if you
													select a
													value here. After finishing a backup, it will wait the given
													time
													before starting another one.</p>
											</div>
										@endif
									@endif
								</div>

								<input type="submit" value="Save" class="button success">

							</form>
						</div>
				</div>
				<!-- end .timeline -->
			</div>
			<!-- box -->
		</div>
	</div>
@endsection