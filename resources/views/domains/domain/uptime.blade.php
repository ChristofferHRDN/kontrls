@extends('spark::layouts.app')

@section('scripts')
	<script src="https://cdnjs.cloudflare.com/ajax/libs/Chart.js/2.1.5/Chart.bundle.min.js"></script>
@endsection

@section('content')
	@include('domains.domain.menu')
	<div class="row" id="check-logs">
		<div class="small-12 columns">
			<div class="callout">
				<div class="">
					<h4 class="box-title">
						<span>Uptime</span>
					</h4>
				</div>
				<div>Downtime {{ceil($downtime * 100)}}%
					<small>(Downtime is calculated over the whole time logging has been enabled. The graph shows up to
						100 entries
						from today only.)
					</small>
				</div>
				<!-- /.box-header -->
				<div class="box-body " style="display: block;">
					<domain-uptime-graph :domainid="'{{$domain->hashid}}'"></domain-uptime-graph>
				</div>
				<!-- end .timeline -->
			</div>
			<!-- box -->
		</div>
	</div>
@endsection