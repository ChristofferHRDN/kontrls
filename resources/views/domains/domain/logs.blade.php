@extends('spark::layouts.app')

@section('scripts')
    <script src="https://cdnjs.cloudflare.com/ajax/libs/Chart.js/2.1.5/Chart.bundle.min.js"></script>
@endsection

@section('content')
    @include('domains.domain.menu')
    <div class="row">
        <div class="small-12 columns">
            <div class="callout">
                <div>
                    <h4 class="box-title">Logs</h4>
                </div>
                <div class="box-body" style="display: block">
                    <log :domainid="'{{$domain->hashid}}'"></log>
                </div>
            </div>
        </div>
    </div>
@endsection