An alert was sent to you by Kntrl.io (http://kntrl.io)
The alert was created {{$alert->created_at}}. This mail was generated at {{\Carbon\Carbon::now()}}
Message: {{$data ?: "Huh, no message? That's odd."}}
{{route('alert.show-by-id', $alert->read_id)}} This link can only be opened once.
Below you will find the alert in it's full.

{{json_encode($alert, JSON_PRETTY_PRINT)}}