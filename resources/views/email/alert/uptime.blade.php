<!doctype html>
<html lang="en">
<head>
    <meta charset="UTF-8">
<body>
<h1>An alert was sent to you by <a href="http://kntrl.io">Kntrl.io</a></h1>
<p>The alert was created <time>{{$alert->created_at}}</time>. This mail was generated at {{\Carbon\Carbon::now()}}</p>
<p><strong>Message</strong>: {{$data ?: "Huh, no message? That's odd."}}</p>
<p><a href="{{route('alert.show-by-id', $alert->read_id)}}">Link to the alert.</a> This link can only be opened once.
</p>
<p>Below you will find the alert in it's full.</p>
<pre><code>{{json_encode($alert, JSON_PRETTY_PRINT)}}</code></pre>
</body>
</html>