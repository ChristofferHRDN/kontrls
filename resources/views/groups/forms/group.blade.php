<form action="{{ route('group.store') }}" method="POST">
    {!! csrf_field() !!}
    <div class="input-group">
        <input type="text" name="name" id="group-name" class="input-group-field" autofocus placeholder="Group name">
        <div class="input-group-button">
            <button type="submit" class="btn btn-default" style="font-size:14px;">
                <i class="fa fa-plus"></i> Add group
            </button>
        </div>
    </div>
</form>