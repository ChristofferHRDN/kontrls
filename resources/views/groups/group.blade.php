@extends('spark::layouts.app')

@section('content')
    <div class="row">
        <div class="small-12 columns">
            <div class="callout">
                <div class="box-header bg-transparent"></div>
                <div class="box-body">
                    <h2>{{ $group->name }}</h2>
                    <p>Domains: {{count($group->domains)}}</p>
                    @if(count($avail_domains))
                        {!! Form::open(['method'=>'post']) !!}
                        {!! Form::select('domain_id', $avail_domains) !!}
                        <input type="submit" value="Add domain">
                        {!! Form::close() !!}
                    @endif
                    <div>
                        @if(count($group->domains) > 0)
                            <div class="domains-wrapper">
                                <table class="small-12 columns" style="padding: 0;">
                                    <thead>
                                    <tr>
                                        <th>Name</th>
                                        <th>URL</th>
                                        <th>Group</th>
                                        <th width="10%"></th>
                                    </tr>
                                    </thead>
                                    <tbody>
                                    @foreach($group->domains as $domain)
                                        <tr>
                                            <td>
                                                <a href="{{ route('domain.show', $domain) }}">{{ $domain->name ? $domain->name : $domain->url }}</a>
                                            </td>
                                            <td>{{ $domain->url }}</td>
                                            <td>
                                                @if($domain->group)
                                                    <a href="{{ route('group.show', $domain->group) }}">{{$domain->group->name}}</a>
                                                @else
                                                    No group assigned
                                                @endif
                                            </td>
                                            <td>
                                                {!! Form::open(['method' => 'delete', 'route' => ['group.domains.remove', $group, $domain]]) !!}
                                                <button type="submit" class="btn btn-warning">
                                                    <i class="fa fa-trash-o"></i> Remove from group
                                                </button>
                                                {!! Form::close() !!}
                                            </td>
                                        </tr>
                                    @endforeach
                                    </tbody>
                                </table>
                            </div>
                        @endif
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div class="row">
        <div class="large-12 columns">
            <div class="callout collapsed-box" data-widget="collapse">
                <div class="box-header bg-transparent" data-widget="collapse">
                    <a data-toggle="collapse" data-target="#groupsettings">
                        <!-- tools box -->
                        <div class="pull-right box-tools">
                                    <span class="box-btn"><i class="icon-minus"></i>
                                    </span>
                        </div>
                        <h3 class="box-title" style="cursor: pointer"><i class="icon-graph-pie"></i>
                            <span>Change group settings</span>
                        </h3>
                    </a>
                </div>
                <!-- /.box-header -->
                <div class="panel-collapse collapse panel-body" id="groupsettings">
                    <div>
                        Settings
                        <form method="POST" action="{{ route('group.settings.update', $group) }}">
                            {!! csrf_field() !!}
                            <label for="poll_time">Poll time (Every n-th minute)
                                <input type="number" min="1" id="polltime" name="polltime"
                                       value="{{ $group->settings->getValue("polltime")}}">
                            </label>
                            <label>Time between alerts
                                <input type="number" min="1" name="time_between_alerts"
                                       value="{{$group->getSetting('time_between_alerts')}}">
                            </label>

                            <div class="small-12 columns">
                                <?php $selected = $group->getSetting('automatic_audit_wait_time', -1); ?>
                                <label>Automatic audits on domains
                                    <select name="automatic_audit_wait_time">
                                        <option value="-1" @if($selected === -1)selected="selected"@endif>Never</option>
                                        <option value="3600" @if($selected === 3600)selected="selected"@endif>Hourly</option>
                                        <option value="86400" @if($selected === 86400)selected="selected"@endif>Daily</option>
                                        <option value="604800" @if($selected === 604800)selected="selected"@endif>Weekly</option>
                                        <option value="2592000" @if($selected === 2592000)selected="selected"@endif>Monthly</option>
                                    </select>
                                </label>
                            </div>

                            <input type="submit" value="Save" class="btn btn-info">
                        </form>
                    </div>
                </div>
                <!-- end .timeline -->
            </div>
            <!-- box -->
        </div>
    </div>
@endsection