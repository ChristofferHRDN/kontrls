<div class="row">
    <div class="small-12 columns">
        <div class="callout">
            <div class="box-header">
                <!-- tools box -->
                <div class="pull-right box-tools">
                    <span class="box-btn" data-widget="collapse"><i class="icon-minus"></i>
                    </span>
                </div>
                <h3 class="box-title"><i class="icon-graph-pie"></i>
                    <span>Groups</span>
                </h3>

            </div>
            <div class="box-body">
                <!-- Bootstrap Boilerplate... -->
                <div class="panel-body">
                    <!-- Display Validation Errors -->
                @include('common.errors')

                <!-- New Domain Form -->
                    @include('groups.forms.group')
                </div>
                <div class="groups">
                    @if(count($groups) > 0)
                        <div class="groups-wrapper">
                            <div class="panel-heading">
                                Current groups
                            </div>
                            <table class="small-12 columns" style="padding: 0">
                                <thead>
                                <tr>
                                    <th>#</th>
                                    <th>Name</th>
                                    <th># Domains</th>
                                    <th># Notes</th>
                                    <th width="10%"></th>
                                </tr>
                                </thead>
                                <tbody>
                                <?php $i = 1;?>
                                @foreach($groups as $group)
                                    <tr class="hide-child-until-hover">
                                        <td>{{$i++}}</td>
                                        <td><a href="{{ route('group.show', $group->hashid) }}">{{ $group->name }}</a></td>
                                        <td>{{ count($group->domains) }}</td>
                                        <td>{{ count($group->notes) }}</td>
                                        <td><form action="{{ route('group.destroy', $group->hashid) }}" method="POST" class="child">
                                                {!! csrf_field() !!}
                                                {!! method_field('DELETE') !!}

                                                <button type="submit" class="btn btn-danger">
                                                    <i class="fa fa-trash-o"></i>
                                                </button>
                                            </form></td>
                                    </tr>
                                @endforeach
                                </tbody>
                            </table>
                        </div>
                    @endif
                </div>
            </div>
        </div>
    </div>
</div>