<?php

Form::macro('labelCheckbox', function($name, $value, $label)
{
	$html = "<label>
                <input name='$name' type='checkbox' value='$value' /> $label
            </label>";

	return $html;
});